#ifndef _FA_READER_H_
#define _FA_READER_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct {
  FILE *fa_file;
  char line[MAX_LINE_LEN];
  char *line_iter;
  char seq_name[MAX_LINE_LEN];
} FAReader;

void fa_close(FAReader *reader) {
  fclose(reader->fa_file);
}


FAReader *init_fa(const char *file_name) {
  FAReader *reader = (FAReader*) malloc(sizeof(FAReader));
  if ((reader->fa_file = fopen(file_name,"r")) == NULL) {
    fprintf(stderr, "Cannot open file %s\n", file_name);
    abort();
  }
  reader->line[0] = 0;
  return reader;
}

void destroy_fa(FAReader *reader) {
  fa_close(reader);
  free(reader);
}


void fa_init(FAReader *reader, const char *file_name) {
  if ((reader->fa_file = fopen(file_name,"r")) == NULL) {
    fprintf(stderr, "Cannot open file %s\n", file_name);
    abort();
  }
  
  reader->line[0] = 0;
}

int fa_next_seq(FAReader *reader) {
  while (1) {
    if (reader->line[0] =='>') {
      if (strlen(reader->line) == MAX_LINE_LEN - 1) {
	fprintf(stderr, "Line too long\n");
	abort();
      }
      /* reader->line[strlen(reader->line) - 1] = '\0'; */ /* remove '\n' */
      /* strcpy(reader->seq_name, reader->line+1); */
      char *pch = strtok(reader->line, "\t \n");
      strcpy(reader->seq_name, pch+1);
      if (fgets(reader->line, MAX_LINE_LEN, reader->fa_file) != NULL) {
	reader->line_iter=reader->line;
	return 1;
      }
    }

    if (fgets(reader->line, MAX_LINE_LEN, reader->fa_file) == NULL)
      return 0;
  }
}

char fa_getbase(FAReader *reader) {
  
  if (*reader->line_iter == '\n') {
    if (fgets(reader->line, MAX_LINE_LEN, reader->fa_file) == NULL || reader->line[0] == '>')
      return '\0';
    else {
      reader->line_iter=reader->line;
    }
  }
  
  return *reader->line_iter++;
  
}

void fa_getseq(FAReader *reader, char *seq, int seq_len) {
  char base;
  int i;
  for (i=0; i<seq_len; i++) {
    if ((base = fa_getbase(reader)) == '\0') {
      fprintf(stderr, "sequence not long enough.\n");
      abort();
    }

    seq[i] = base;
    
  }
}
#endif /* _FA_READER_H_ */
