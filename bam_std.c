#include "bam_std.h"

/* samtools' rmdup */
void bam_rmdup_core(samfile_t *in, samfile_t *out);

/* bam sort (as if in-place) */
void bam_sort_core_ext(int is_by_qname, const char *fn, const char *prefix, size_t _max_mem, int is_stdout, int n_threads, int level, int full_path);

void bam_sort_ip(char *bam_fn) {
  fprintf(stderr, "[%s] sort bam.\n", __func__);
  fflush(stderr);
  char *tmp_fn = (char*) calloc(strlen(bam_fn)+5, 1);
  sprintf(tmp_fn, "%s.tmp", bam_fn);
  bam_sort_core_ext(0, bam_fn, tmp_fn, 768<<20, 0, 0, -1, 1);
  rename(tmp_fn, bam_fn);
  free(tmp_fn);
}

/* bam mark duplicate (as if in-place) */
int mark_dup_nosort(char *bam_in_fn, char *bam_out_fn);
void bam_mkdup(char *bam_fn) {
  char *tmp_fn = (char*) calloc(strlen(bam_fn)+5, 1);
  sprintf(tmp_fn, "%s.tmp", bam_fn);
  mark_dup_nosort(bam_fn, tmp_fn);
  rename(tmp_fn, bam_fn);
  free(tmp_fn);
  bam_sort_ip(bam_fn);
  bam_index_build(bam_fn);
}

/* bam realign (as if in-place) */
void target_realn_nosort(char *in_fn, char *out_fn, KCnts *kc, uint8_t *siv);
void bam_realn(char *bam_fn, KCnts *kc, uint8_t *siv) {
  char *tmp_fn = (char*) calloc(strlen(bam_fn)+5, 1);
  sprintf(tmp_fn, "%s.tmp", bam_fn);
  target_realn_nosort(bam_fn, tmp_fn, kc, siv);
  rename(tmp_fn, bam_fn);
  free(tmp_fn);
  bam_sort_ip(bam_fn);
  bam_index_build(bam_fn);
}

