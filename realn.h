#ifndef _WZ_REALN_H_
#define _WZ_REALN_H_

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "samtools/bam.h"
#include "samtools/sam.h"
#include "samtools/khash.h"
#include "samtools/klist.h"
#include "pileup.h"
#include "wstr.h"
#include "ssw.h"

typedef struct {
  uint32_t window_width;
  uint32_t min_mapQ;
  uint32_t min_baseQ;
  uint32_t max_indel;
  uint32_t min_reads;
  uint32_t min_exempt_length;
  double min_cov;
  uint32_t flen;
  uint8_t sort;
  /* uint8_t target_only; */
} RealnConf;


/* Allele Window Sequence with a list of mutations */
typedef struct {
  char *seq;
  uint32_t len;	    /* length of the allele window sequence (mutated) */
  int8_t *seq_sw;
  AllelePList *alts;		/* keep sorted */
  IntList *poses;   /* position of mutation on the alternative allele */
} AWSeq;

DEFINE_VECTOR(AWSeqList, AWSeq)

DEFINE_VECTOR(CigarList, uint32_t)

typedef struct {
  Allele *beg;			/* first allele */
  Allele *end;			/* last allele (NOT the one past last) */
  uint32_t rsbeg;		/* ref sequence begin */
  uint32_t rslen;		/* ref sequence length */
  AWSeq *ref;			/* reference seq, pointing to seqs */
  AWSeqList *seqs;
  int up;			/* whether seqs is updated */
} AlleleWindow;

static inline int
allele_list_overlap(AllelePList *alts, Allele *t) {
  uint32_t i;
  for (i=0; i<alts->size; ++i) {
    Allele *a = get_AllelePList(alts, i);
    if (a->rpos + a->len > t->rpos && 
	t->rpos + t->len > a->rpos) {
      return 1;
    }
  }
  return 0;
}

/* initiate reference sequences,
   one with the mutation, the other without */
static inline void 
init_AWSeq(AlleleWindow *win, RefSeq *rs, uint32_t window_width) {

  clear_AWSeqList(win->seqs);
  AWSeq *as  = next_ref_AWSeqList(win->seqs);
  win->ref   = as;
  win->rsbeg = win->beg->rpos - window_width;
  win->rslen = win->end->rpos + window_width - win->rsbeg + 1;
  as->seq    = fetch_RefSeq(rs, win->beg->tid, win->rsbeg, win->rslen);
  as->seq_sw = nt256char_encode_nt256int8(as->seq, win->rslen);
  as->alts   = init_AllelePList(2);
  as->poses  = init_IntList(2);
  as->len    = win->rslen;

}

static inline void
AlleleWindow_free(AlleleWindow *win) {
  uint32_t i;
  for (i=0; i<win->seqs->size; ++i) {
    AWSeq *as = ref_AWSeqList(win->seqs, i);
    free_AllelePList(as->alts);
    free_IntList(as->poses);
    if (as->seq) free(as->seq);
    if (as->seq_sw) free(as->seq_sw);
  }
  free_AWSeqList(win->seqs);
}

Allele* allele_find(AlleleList *alts, Allele *t);

int target_realn(char *in_fn, char *out_fn, KCnts *kc, uint8_t *siv, RealnConf *conf);

void bam_realn_alts(AlleleList *alts, char *in_fn, char *out_fn, RefSeq *rs, RealnConf *conf);

void spawn_AWSeqList(AWSeq *as_dst, AWSeq *as_src, Allele *a, AlleleWindow *win);

void read_realn_alts(bam1_t *b, AlleleWindow *win);

int aln_recover_cigar(bam1_t *b, s_align *aln, AWSeq *as, AlleleWindow *win);

void target_realn_default(char *in_fn, char *out_fn, KCnts *kc, uint8_t *siv);

#endif /* _WZ_REALN_H_ */
