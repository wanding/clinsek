#include <stdio.h>
#include <string.h>

#define CLINSEK_VERSION "0.1"

int main_homoscan(int argc, char *argv[]);
int main_tcall(int argc, char *argv[]);
int main_scall(int argc, char *argv[]);
int main_mkdup(int argc, char *argv[]);
int main_tpileup(int argc, char *argv[]);
int main_spileup(int argc, char *argv[]);
int main_realn(int argc, char *argv[]);
int main_bcall(int argc, char *argv[]);

static int usage() {
  fprintf(stderr, "\n");
  fprintf(stderr, "Program: clinsek (Tools for targeted alignments and variant calling)\n");
  fprintf(stderr, "Version: %s\n\n", CLINSEK_VERSION);
  fprintf(stderr, "Usage:   clinsek <command> [options]\n\n");
  fprintf(stderr, "Command: homoscan    scan homologous sites\n");
  fprintf(stderr, "         tcall       targeted variant call\n");
  fprintf(stderr, "         scall       targeted somatic call\n");
  fprintf(stderr, "         bcall       break point call\n");
  fprintf(stderr, "         tpileup     targeted pileup\n");
  fprintf(stderr, "         spileup     targeted somatic pileup\n");
  fprintf(stderr, "         realn       targeted indel re-alignment\n");
  fprintf(stderr, "         mkdup       mark duplicate reads\n");

  fprintf(stderr, "\n");

  return 1;
}

int main(int argc, char *argv[]) {

  if (argc < 2) return usage();
  else if (strcmp(argv[1], "tcall") == 0) return main_tcall(argc-1, argv+1);
  else if (strcmp(argv[1], "scall") == 0) return main_scall(argc-1, argv+1);
  else if (strcmp(argv[1], "bcall") == 0) return main_bcall(argc-1, argv+1);
  else if (strcmp(argv[1], "homoscan") == 0) return main_homoscan(argc-1, argv+1);
  else if (strcmp(argv[1], "mkdup") == 0) return main_mkdup(argc-1, argv+1);
  else if (strcmp(argv[1], "tpileup") == 0) return main_tpileup(argc-1, argv+1);
  else if (strcmp(argv[1], "spileup") == 0) return main_spileup(argc-1, argv+1);
  else if (strcmp(argv[1], "realn") == 0) return main_realn(argc-1, argv+1);
  else if (strcmp(argv[1], "-h") == 0) return usage();
  else {
    fprintf(stderr, "[main] unrecognized command '%s'\n", argv[1]);
    return 1;
  }

  return 0;
}
