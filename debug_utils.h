#include "site.h"
#include "refseq.h"
#include "samtools/kstring.h"

char *format_site(Site *st, RefSeq *rs) {
  kstring_t s;
  s.m = s.l = 0; s.s = 0;
  kputs(tid2chrm(rs, st->chrm), &s);
  ksprintf(&s, ":%d", st->pos);
  return s.s;
}
