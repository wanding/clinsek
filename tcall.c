#include <unistd.h>
#include <time.h>
#include "tally.h"
#include "tgt_aln.h"
#include "samtools/bam.h"
#include "tpileup.h"
#include "bam_std.h"
#include "wstr.h"

static int usage() {

  fprintf(stderr, "\n");
  fprintf(stderr, "Usage: clinsek tcall [options] -s sitelist -r ref.fa -1 in1.fq [in1.fq [...]] -2 in2.fq [in2.fq [...]]\n");
  fprintf(stderr, "Input options:\n");
  fprintf(stderr, "     -s FILE   site list [required]\n");
  fprintf(stderr, "     -r FILE   reference in fasta format [required]\n");
  fprintf(stderr, "     -1 FILES  fastq file(s) (1st in pair), files may be gzipped [required]\n");
  fprintf(stderr, "     -2 FILES  fastq file(s) (2nd in pair), files may be gzipped [required]\n");
  fprintf(stderr, "     -o STR    output file prefix [test]\n");
  fprintf(stderr, "     -l FILE   optional site filter [no output]\n");
  fprintf(stderr, "     -c FLOAT  estimate for contamination rate [0.01]\n");
  fprintf(stderr, "     -e FLOAT  estimate for sequencing error rate [1e-3]\n");
  fprintf(stderr, "     -m FLOAT  estimate for mutation rate [1e-3]\n");
  fprintf(stderr, "     -q INT    minimum mapping quality in variant calling [10]\n");
  fprintf(stderr, "     -b INT    minimum base quality in SNP calling [10]\n");
  fprintf(stderr, "     -d INT    base quality threshold in preliminary call [20]\n");
  fprintf(stderr, "     -v INT    minimum number of variants for preliminary call [3]\n");
  fprintf(stderr, "     -u FLOAT  minimum variant fraction for preliminary call [0.005]\n");
  fprintf(stderr, "     -a        toggle to turn ON indel realignment\n");
  fprintf(stderr, "     -k        toggle to turn OFF duplicate marking\n");
  fprintf(stderr, "     -n        toggle to turn OFF preliminary variant filtering\n");
  fprintf(stderr, "     -V INT    verbose level [0]\n");
  fprintf(stderr, "     -h        this help\n");
  fprintf(stderr, "\n");

  return 1;
}


int main_tcall(int argc, char *argv[]) {

  CallConf conf;
  conf_init_default(&conf);
  clock_t t = clock();

  if (argc < 2) return usage();

  FList *fl1 = init_FList(2);
  FList *fl2 = init_FList(2);
  char *sfn=0, *rfn=0, *prefix=0, *filter_fn=0;
  int c;
  while ((c = getopt(argc, argv, "s:r:1:2:o:l:c:e:m:q:b:d:u:v:V:kanh")) >= 0) {
    switch (c) {
    case 's': sfn = optarg; break;
    case 'r': rfn = optarg; break;
    case '1': {
      int index;
      for (index = optind-1;index < argc;++index) {
      	char *next = argv[index];
      	if (next[0] == '-') break;
      	push_FList(fl1, strdup(next));
      }
      optind = index-1;
      break;
    }
    case '2': {
      int index;
      for (index = optind-1;index < argc;++index) {
      	char *next = argv[index];
      	if (next[0] == '-') break;
      	push_FList(fl2, strdup(next));
      }
      optind = index-1;
    } break;
    case 'o': prefix = optarg; break;
    case 'l': filter_fn = optarg; break;
    case 'c': conf.contam = atof(optarg); break;
    case 'e': conf.error = atof(optarg); break;
    case 'm': conf.mu = atof(optarg); break;
    case 'q': conf.min_mapQ  = atoi(optarg); break;
    case 'b': conf.min_baseQ = atoi(optarg); break;
    case 'd': conf.tbaseQ = atoi(optarg); break;
    case 'v': conf.min_var = atoi(optarg); break;
    case 'u': conf.min_var_p = atof(optarg); break;
    case 'a': conf.realn = 1; break;
    case 'k': conf.mkdup = 0; break;
    case 'n': conf.filter = 0; break;
    case 'V': conf.verbose = atoi(optarg); break;
    case 'h': return usage();
    default:
      fprintf(stderr, "[%s] Unrecognized command: %c\n", __func__, c);
      exit(1);
    }
  }

  push_FList(fl1, NULL);
  push_FList(fl2, NULL);
  if (!prefix) prefix = "test";

  KCnts *kc = load_sites(sfn, rfn);
  kc->conf = &conf;

  char *vb_fn = wasprintf("%s.vardp", prefix);
  VarSIBuffer *vb = init_VarSIBuffer(vb_fn);
  free(vb_fn);

  char *rb_fn = wasprintf("%s.refdp", prefix);
  RefSIBuffer *rb = init_RefSIBuffer(rb_fn);
  free(rb_fn);

  tally(kc, fl1->buffer, fl2->buffer, vb, rb);

  SiteFilter *fter = conf.filter ? prelim_call(kc, vb, rb) : init_SiteFilter_all(kc->sl->size);
  if (filter_fn) dump_filter(filter_fn, fter);

#ifdef DEBUG

  printf("Sites aligned:\n");
  uint32_t _i;
  for (_i=0; _i<kc->sl->size; ++_i) {
    if (get_SiteFilter(fter, _i)) {
      Site *s = get_SiteList(kc->sl, _i);
      printf("%s:%zu\n", tid2chrm(kc->rs, s->chrm), s->pos);
    }
  }

#endif // DEBUG

  char *bam_fn = wasprintf("%s.bam", prefix);
  tgt_aln(kc, fl1->buffer, fl2->buffer, bam_fn, vb, rb, fter->buffer);
  bam_sort_ip(bam_fn);
  bam_index_build(bam_fn);

  if (conf.realn) bam_realn(bam_fn, kc, fter->buffer);
  if (conf.mkdup) bam_mkdup(bam_fn);

  destroy_VarSIBuffer(vb);
  destroy_RefSIBuffer(rb);

  char *bcf_fn = wasprintf("%s.bcf", prefix);
  tpileup(bcf_fn, bam_fn, kc, fter->buffer);

  free_SiteFilter(fter);
  free(bam_fn);
  free(bcf_fn);

  destroy_FList(fl1);
  destroy_FList(fl2);
  free_KCnts(kc);

  fprintf(stderr, "[%s] All done. Time: %.3f min\n", __func__, (float) (clock() - t) / CLOCKS_PER_SEC / 60.);

  return 0;
}
