#include "samtools/kstring.h"
#include "wvec.h"

DEFINE_VECTOR(FieldList, char*)

typedef struct {
  char *fn;
  FILE *fp;
  FieldList *fields;
} TSVParser;

TSVParser* tsv_open(char *fn) {
  TSVParser *tsv = calloc(1,sizeof(TSVParser));
  tsv->fn = fn;
  tsv->fp = fopen(fn, "r");
  tsv->fields = init_FieldList(2);

  return tsv;
}

void clean_FieldList(FieldList *fields) {
  uint32_t i;
  for (i=0; i<fields->size; ++i) {
    free(get_FieldList(fields, i));
  }
  clear_FieldList(fields);
}

void tsv_readline(TSVParser *tsv) {
  clean_FieldList(tsv->fields);
  kstring_t s;
  s.s = 0; s.m = s.l = 0;
  while (1) {
    int c = fgetc(tsv->fp);
    if (c == '\n' || c == EOF) {
      if (s.s) push_FieldList(tsv->fields, s.s);
      return;
    } else if (c == '\t') {
      push_FieldList(tsv->fields, s.s);
      s.s = 0; s.m = s.l = 0;
    } else {
      kputc(c, &s);
    }
  }
}

char *tsv_field(TSVParser *tsv, uint32_t i) {
  return get_FieldList(tsv->fields, i);
}

char* tsv_formatline(TSVParser *tsv) {
  uint32_t i;
  kstring_t s;
  s.s = 0; s.m = s.l = 0;
  for (i=0; i<tsv->fields->size; ++i) {
    if (i!=0) kputc('\t', &s);
    kputs(get_FieldList(tsv->fields, i), &s);
  }
  return s.s;
}

void tsv_close(TSVParser *tsv) {
  if (tsv->fp) fclose(tsv->fp);
}

void free_tsv(TSVParser *tsv) {
  clean_FieldList(tsv->fields);
  free_FieldList(tsv->fields);
  free(tsv);
}
