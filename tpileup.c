#include "tpileup.h"
#include "wstr.h"
#include "debug_utils.h"
#include <getopt.h>

static char info_str[] = \
  "##INFO=<ID=AN,Number=1,Type=Integer,Description=\"Total Allele Count\">\n"
  "##INFO=<ID=AC,Number=A,Type=Integer,Description=\"Alternate Allele Count\">\n"
  "##INFO=<ID=AF,Number=A,Type=Float,Description=\"Allele Frequency\">\n"
  "##INFO=<ID=SVTYPE,Number=1,Type=String,Description=\"Insertion or Deletion\">\n";

static char fmt_str[] = \
  "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Raw read depth (without quality filtering)\">\n"
  "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n"
  "##FORMAT=<ID=GL,Number=G,Type=Float,Description=\"Genotype Likelihoods (ln-transformed)\">\n"
  "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype quality (Phred-scaled from genotype probability)\">\n";


PlpData *init_PlpData(char *bam_fn, char *bcf_fn, char *rfn) {
  PlpData *pd = malloc(sizeof(PlpData));
  prepare_pileup_bam(&pd->bam, bam_fn);
  prepare_pileup_allele(&pd->ad);
  prepare_pileup_read(&pd->rd);
  prepare_pileup_bcf(bcf_fn, rfn, &pd->bam, &pd->bcf,
		     info_str, fmt_str);
  pd->refseq.seq = NULL;
  return pd;
}

void free_PlpData(PlpData *pd) {
  free(pd->refseq.seq);
  clean_pileup_read(&pd->rd);
  clean_pileup_bam(&pd->bam);
  clean_pileup_bcf(&pd->bcf);
  clean_pileup_allele(&pd->ad);
  free(pd);
}

/* dp is the read coverage
   an is the insert coverage */
static void
write_bcf(PlpBCFData *bcf, BCFRec *rec, uint32_t dp, uint32_t an, CallConf *conf) {

  /* order alternative alleles by insert counts */
  qsort(rec->alts->buffer, rec->alts->size,
	sizeof(Allele*), cmp_allelep_by_ic);

  /* AllelePList *majors = ad->majors; */
  /* Allele *ref = &ad->ref; */

  /* if (majors->size==0) return; */

  /* qsort(majors->buffer, majors->size, sizeof(Allele*), cmp_allele); */

  /* uint32_t sumrc = ref->rc; */
  /* uint32_t i; */
  /* for (i=0; i<majors->size; ++i) { */
  /*   Allele *a = get_AllelePList(majors, i); */
  /*   sumrc += a->rc; */
  /* } */

  /* /\* bcf output the major alternative allele *\/ */
  /* Allele *a = get_AllelePList(majors, 0); */

  bcf1_t *b = calloc(1, sizeof(bcf1_t));
  b->n_smpl = bcf->bh->n_smpl;
  Allele *a = get_AllelePList(rec->alts, 0);
  b->tid = a->tid;
  b->pos = a->rpos-1;		/* BCF position is zero-based */
  b->qual = max_qual(rec->quals);
  /* uint32_t varrc = sumrc - ref->rc; */
  /* b->qual = pval2qual(varcall_pval(ref->rc, varrc, conf->error, conf->mu, conf->contam)); */
  
  kstring_t s;
  s.s = 0; s.m = s.l = 0;
  /* s.s = b->str; s.m = b->m_str; s.l = 0; */

  /* ID */
  kputc('\0', &s);

  uint32_t i;
  if (a->cigar == BAM_CDIFF) {	/* substitution */

    /* REF */
    Allele *_a = get_AllelePList(rec->alts, 0);
    kputsn(rec->ref->seq-rec->ref->rpos+_a->rpos, _a->len, &s);
    kputc('\0', &s);

    /* ALT */
    for (i=0; i<rec->alts->size; ++i) {
      _a = get_AllelePList(rec->alts, i);
      if (i) kputc(',', &s);
      kputsn(_a->seq, _a->len, &s);
    }
    kputc('\0', &s);

  } else if (a->cigar == BAM_CDEL) { /* deletion */

    /* REF */
    kputsn(a->seq, a->len, &s);
    kputc('\0', &s);

    /* ALT */
    kputc(a->seq[0], &s);
    kputc('\0', &s);

  } else if (a->cigar == BAM_CINS) { /* insertion */

    /* REF */
    kputc(rec->ref->seq[0], &s);
    kputc('\0', &s);

    /* ALT */
    kputc(rec->ref->seq[0], &s);
    kputsn(a->seq, a->len, &s);
    kputc('\0', &s);

  } else {                      /* shouldn't see BAM_CREF_SKIP */
    fprintf(stderr, "[%s] Unknown cigar.\n", __func__);
    exit(1);
  }

  /* FILTER */
  if (b->qual>5) kputs("PASS", &s);
  else kputs("LOWQUAL", &s);
  kputc('\0', &s);

  /* INFO */
  ksprintf(&s, "AN=%d;DP=%d", an, dp);
  kstring_t ac, af;
  ac.m = ac.l = 0; ac.s = 0;
  af.m = af.l = 0; af.s = 0;
  for (i=0; i<rec->alts->size; ++i) {
    if (i) {
      kputc(',', &ac);
      kputc(',', &af);
    }
    Allele *alt = get_AllelePList(rec->alts,i);
    /* uint32_t altcnt = get_IntList(rec->altcnts, i); */
    ksprintf(&ac, "%d", alt->ic);
    ksprintf(&af, "%.4g", (float)alt->ic/an);
  }
  ksprintf(&s, ";AC=%s", ac.s);
  ksprintf(&s, ";AF=%s", af.s);
  free(ac.s); free(af.s);

  if (a->cigar == BAM_CDEL) kputs(";SVTYPE=DEL", &s);
  if (a->cigar == BAM_CINS) kputs(";SVTYPE=INS", &s);

  /* kputw(a->rc, &s); */
  /* I16, QS, VDB, RPB ... */
  kputc('\0', &s);

  /* FMT */
  /* PL, DP, DV, SP etc. */
  kputs("DP:GT:GL:GQ", &s);
  kputc('\0', &s);

  b->m_str = s.m; b->str = s.s; b->l_str = s.l;
  bcf_sync(b);

  bcf_ginfo_t *g = b->gi;

  /* DP: uint16 */
  ((uint16_t*) g->data)[0] = dp; ++g;

  /* GT: uint8
   * GT 9 (001001) for "1/1"
   * GT 1 (000001) for "0/1"
   * GT 0 (000000) for "0/0" */
  uint32_t max_alt = max_alt_ic(rec->alts);
  float gl0 = log(conf->prior0) + genotype_lnlik(HOMOREF, rec->ref->ic, max_alt, conf->error, conf->contam);
  float gl1 = log(conf->prior1) + genotype_lnlik(HET, rec->ref->ic, max_alt, conf->error, conf->contam);
  float gl2 = log(conf->prior2) + genotype_lnlik(HOMOVAR, rec->ref->ic, max_alt, conf->error, conf->contam);
  int gq;
  if (gl0>gl1) {
    if (gl0>gl2) {
      ((uint8_t*) g->data)[0] = 0;
      gq = pval2qual(1 - exp(gl0 - ln_sum3(gl0, gl1, gl2)));
    } else {
      ((uint8_t*) g->data)[0] = 9;
      gq = pval2qual(1 - exp(gl2 - ln_sum3(gl0, gl1, gl2)));
    }
  } else if (gl1>gl2) {
    ((uint8_t*) g->data)[0] = 1;
    gq = pval2qual(1 - exp(gl1 - ln_sum3(gl0, gl1, gl2)));
  } else {
    ((uint8_t*) g->data)[0] = 9;
    gq = pval2qual(1 - exp(gl2 - ln_sum3(gl0, gl1, gl2)));
  }
  ++g;
  
  /* GL: float */
  ((float*)g->data)[0] = gl0;
  ((float*)g->data)[1] = gl1;
  ((float*)g->data)[2] = gl2;
  ++g;

  /* GQ: uint8 */
  ((uint8_t*) g->data)[0] = gq;

  bcf_write(bcf->bp, bcf->bh, b);
  bcf_destroy(b);
}


/* ac2bcf does not handle cases where indels and snps are mixed */
static void 
ac2bcf(PlpAlleleData *ad, PlpBCFData *bcf, CallConf *conf) {

  uint32_t i;

  AllelePList *al = ad->al;
  AllelePList *al_rec = init_AllelePList(2); /* alleles to be proecessed in one record */
  BCFRec *rec = init_BCFRec();
  while (check_remain(al)) {

    /* this is one record */
    clear_AllelePList(al_rec);
    for (i=0; i<al->size; ++i) {
      Allele *a = get_AllelePList(al, i);
      if (!a->recorded) {
	if (al_rec->size>0) {
	  if (same_category(get_AllelePList(al_rec, 0), a)) { /* two alleles are in the same category if they are at the same position and they are both SNPs */
	    push_AllelePList(al_rec, a);
	    a->recorded = 1;
	  }
	} else {
	  push_AllelePList(al_rec, a);
	  a->recorded = 1;
	}
      }
    }

    clear_BCFRec(rec);
    rec->ref = &ad->ref;

    /* check quality */
    for (i=0; i<al_rec->size; ++i) {
      Allele *a = get_AllelePList(al_rec, i);
      uint32_t altcnt = a->ic;
      if (altcnt > 0) {
	/* rec->ref->rc ==> ad->an - altcnt */
	double pval = varcall_pval(ad->an - altcnt, altcnt,
				   conf->error, conf->mu, conf->contam);
	double qual = pval2qual(pval);
	push_FloatList(rec->quals, qual);
	push_AllelePList(rec->alts, a);
      }
    }
    if (rec->alts->size > 0) {
      write_bcf(bcf, rec, ad->dp, ad->an, conf);
    }
  }
  free_AllelePList(al_rec);
  free_BCFRec(rec);
}

void tpileup(char *bcf_fn, char *bam_fn, KCnts *kc, uint8_t *siv) {

  PlpData *pd = init_PlpData(bam_fn, bcf_fn, kc->rs->rfn);
  uint32_t i, c=0;
  for (i=0; i<kc->sl->size; ++i) {
    if (siv[i]) {

      fprintf(stderr, "\r[%s] pile up %u mutations", __func__, c);
      ++c;

      Site *st = get_SiteList(kc->sl, i);

      /* fprintf(stdout, "\t%s:%zu\n", tid2chrm(kc->rs, st->chrm), st->pos); */
      bind_site_reference(&pd->refseq, st, kc->rs);
      init_pileup_allele(&pd->ad, &pd->refseq, st);

      InsertList *insl=site_fetch_inserts(st, &pd->bam, &pd->rd, kc->conf->min_mapQ);
      plp_inserts(&pd->ad, insl, &pd->refseq, st, kc->conf->min_baseQ);
      ac2bcf(&pd->ad, &pd->bcf, kc->conf);

      destroy_InsertList(insl, &pd->rd);
    }
  }
  fprintf(stderr, "\r[%s] pile up %u mutations\n", __func__, c);
  free_PlpData(pd);

}

static int usage() {
  fprintf(stderr, "\n");
  fprintf(stderr, "Usage: clinsek tpileup [options] -s sitelist -r ref.fa -b aln.bam -o out.bcf\n");
  fprintf(stderr, "Input options:\n");
  fprintf(stderr, "     -s FILE   site list [required]\n");
  fprintf(stderr, "     -r FILE   reference in fasta format [required]\n");
  fprintf(stderr, "     -i FILE   indexed bam file [required]\n");
  fprintf(stderr, "     -o STR    file name for bcf output [input.bcf].\n");
  fprintf(stderr, "     -c FLOAT  estimate for contamination rate [0.01]\n");
  fprintf(stderr, "     -e FLOAT  estimate for sequencing error rate [1e-3]\n");
  fprintf(stderr, "     -m FLOAT  estimate for mutation rate [1e-3]\n");
  fprintf(stderr, "     -q INT    minimum mapping quality in variant calling [10]\n");
  fprintf(stderr, "     -b INT    minimum base quality in SNP calling [10]\n");
  fprintf(stderr, "     -d INT    base quality threshold in preliminary call [20]\n");
  fprintf(stderr, "     -h        this help\n");
  fprintf(stderr, "\n");
  return 1;
}

int main_tpileup(int argc, char *argv[]) {

  if (argc<2) return usage();

  CallConf conf;
  conf_init_default(&conf);

  char *sfn=0, *rfn=0;
  char *bam_fn=0, *bcf_fn=0;
  char *filter_fn = 0;

  static struct option long_options[] = {
    {"site", required_argument, 0, 's'},
    {"sitestr", required_argument, 0, 'S'},
    {"reference", required_argument, 0, 'r'},
    {"in", required_argument, 0, 'i'},
    {"out", required_argument, 0, 'o'},
    {"filter", required_argument, 0, 'l'},
    {"contam", required_argument, 0, 'c'},
    {"error", required_argument, 0, 'e'},
    {"mu", required_argument, 0, 'm'},
    {"minmapq", required_argument, 0, 'q'},
    {"minbaseq", required_argument, 0, 'b'},
    {"tbaseq", required_argument, 0, 'd'},
    /* homozygous reference prior is 1-P-Q */
    {"prior1", required_argument, 0, 'P'}, /* heterozygous prior */
    {"prior2", required_argument, 0, 'Q'}, /* homozygous variant prior */
    {0, 0, 0, 0}
  };

  kstring_t sitestr; sitestr.s = 0; sitestr.m = sitestr.l = 0;
  int long_index = 0, opt = 0;
  while ((opt = getopt_long(argc, argv,
			    "s:S:r:i:o:l:c:e:m:q:b:d:h",
			    long_options, &long_index)) != -1) {
    switch(opt) {
    case 's': sfn	      = optarg; break;
    case 'S': kputs(optarg, &sitestr); break;
    case 'r': rfn	      = optarg; break;
    case 'i': bam_fn	      = optarg; break;
    case 'o': bcf_fn	      = strdup(optarg); break;
    case 'l': filter_fn       = optarg; break;
    case 'c': conf.contam     = atof(optarg); break;
    case 'e': conf.error      = atof(optarg); break;
    case 'm': conf.mu	      = atof(optarg); break;
    case 'q': conf.min_mapQ   = atoi(optarg); break;
    case 'b': conf.min_baseQ  = atoi(optarg); break;
    case 'd': conf.tbaseQ     = atoi(optarg); break;
    case 'P': conf.prior1     = atof(optarg); break;
    case 'Q': conf.prior2     = atof(optarg); break;
    case 'h': return usage();
    default:
      fprintf(stderr, "[%s] Unrecognized command: %c.\n", __func__, opt);
      exit(1);
    }
  }

  conf.prior0 = 1 - conf.prior1 - conf.prior2;
  if ((conf.prior0 < 0 || conf.prior0 > 1.0) ||
      (conf.prior1 < 0 || conf.prior1 > 1.0) || 
      (conf.prior2 < 0 || conf.prior2 > 1.0)) {
    fprintf(stderr, "[%s] Invalid prior setting\n Make sure prior1 > 0 && prior2 > 0 && prior1 + prior2 > 0", __func__);
    exit(1);
  }

  /*   while ((c=getopt(argc, argv, ""))>=0) { */
  /*     switch (c) { */
  /*     case 's': sfn	      = optarg; break; */
  /*     case 'S': sitestr         = optarg; break; */
  /*     case 'r': rfn	      = optarg; break; */
  /*     case 'i': bam_fn	      = optarg; break; */
  /*     case 'o': bcf_fn	      = strdup(optarg); break; */
  /*     case 'l': filter_fn       = optarg; break; */
  /*     case 'c': conf.contam     = atof(optarg); break; */
  /*     case 'e': conf.error      = atof(optarg); break; */
  /*     case 'm': conf.mu	   = atof(optarg); break; */
  /*     case 'q': conf.min_mapQ   = atoi(optarg); break; */
  /*     case 'b': conf.min_baseQ  = atoi(optarg); break; */
  /*     case 'd': conf.tbaseQ     = atoi(optarg); break; */
  /*     case 'h': return usage(); */
  /*     default: */
  /*       fprintf(stderr, "[%s] Unrecognized command: %c.\n", __func__, c); */
  /*       exit(1); */
  /*     } */
  /*   } */

  KCnts *kc = load_sites_core(sfn, rfn, sitestr.s?&sitestr:0);
  kc->conf = &conf;
/*   parse_target(kc->sl, sitestr, kc->rs, 100); */
/*   fprintf(stderr, "sitestr: %s\n", sitestr.s); */
  if (sitestr.s) free(sitestr.s);

  SiteFilter *fter = load_filter(filter_fn, kc->sl->size);

  if (!bam_fn) {
    fprintf(stderr, "[%s:%d] Error, missing input bam.\n", __func__, __LINE__);
    exit(1);
  }
  if (!bcf_fn) bcf_fn = wasprintf("%s.bcf", bam_fn);

  tpileup(bcf_fn, bam_fn, kc, fter->buffer);

  free_KCnts(kc);

  free(bcf_fn);

  return 0;
}
