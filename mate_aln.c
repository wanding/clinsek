#include "tgt_aln.h"

void copy_best_aln(bam_aln_t *ba, AlignList *alist, uint32_t st_pos) {

  /* best alignment should have highest score
   * and closest position to the target site */
  Align *best_aln=ref_AlignList(alist, 0);
  uint32_t i;
  for (i=1; i<alist->size; ++i) {
    Align *qa = ref_AlignList(alist, i);
    if (qa->score > best_aln->score) best_aln = qa;
    if (qa->score == best_aln->score && abs((qa->begin+qa->end)/2-1000) < abs((qa->begin+qa->end)/2-1000)) best_aln = qa;
  }

  ba->str = best_aln->str;
  ba->pos = best_aln->begin+st_pos;
  ba->n_cigar = best_aln->n_cigar;
  ba->cigar = (uint32_t*) malloc(ba->n_cigar*sizeof(uint32_t));
  memcpy(ba->cigar, best_aln->cigar, ba->n_cigar*sizeof(uint32_t));

}


void
fix_mate(SeqHash *sh, Site *st, bam_aln_t *ba,
	 RKGen *rk, SSPool *ssp, CallConf *conf) {

  AlignList *alist = init_AlignList(20);
  s_align *a, *ar;

  uint32_t i;
  while (rk_generate(rk) > 0) {

    Suffix *sf = get_SeqHash(sh, rk->p, rk->d);
    if (sf == NULL) continue;

    size_t si;
    SeqList *sl = (SeqList*) sf->data;
    for (si = 0; si < sl->size; ++si) {
      Seq *s = ref_SeqList(sl, si);
      Kmer *k = s->k;
      if (k->site == st) {

	/* if the k-mer is covered by a previous alignment
	 * then skip the k-mer */
	uint8_t p_aligned = 0;
	for (i=0; i<alist->size; ++i) {
	  Align *al=ref_AlignList(alist,i);
	  if (al->begin<(unsigned) k->spos &&
	      al->end>(unsigned) k->spos+KLEN) {
	    p_aligned = 1;
	    break;
	  }
	}

	if (p_aligned) continue;
	SiteSeq *ss = get_SSPool(ssp, st);
	uint8_t cs1, cs2, m;
	if (s->str) {

	  if (seed_extend_neg(ss->seq, rk, k, &cs1, &cs2, &m, 0)) {
	    Align *al = next_ref_AlignList(alist);
	    al->str = 1;
	    al->score = rk->len*2 - m*4;
	    al->begin = k->spos - rk->len + rk->ks + rk->klen + cs1;
	    al->end = k->spos + rk->klen + rk->ks - 1 - cs2;
	    seedext_set_cigar(al, rk->len, cs1, cs2);
	    break;
	  }

	} else {

	  if (seed_extend_pos(ss->seq, rk, k, &cs1, &cs2, &m, 0)) {
	    Align *al = next_ref_AlignList(alist);
	    al->str = 0;
	    al->score = rk->len*2 - m*4;
	    al->begin = k->spos - rk->ks + cs1;
	    al->end = k->spos + rk->len - rk->ks - 1 - cs2;
	    seedext_set_cigar(al, rk->len, cs1, cs2);
	    break;
	  }
	}

	/** local Smith-Waterman **/
	rk_encode_nt256int8(rk);
	ss_encode_nt256int8(ss);
	uint32_t seq_begin = k->spos - rk->len * 3/2;
	a=_ssw_align(rk->seq_sw, rk->len,
		     ss->swseq + seq_begin, 3 * rk->len);
	ar=_ssw_align(rk->seq_sw_r, rk->len,
		      ss->swseq + seq_begin, 3 * rk->len);

	if (a->score1 > ar->score1) {
	  if (check_aln(a, ss->seq + seq_begin, rk, 0, conf)) {
	    Align *al = next_ref_AlignList(alist);
	    al->str = 0;
	    al->begin = seq_begin+a->ref_begin1;
	    al->end = seq_begin+a->ref_end1;
	    swaln_cpy_cigar2(al, a, rk->len);
	  }
	} else {
	  if (check_aln(ar, ss->seq + seq_begin, rk, 1, conf)) {
	    Align *al = next_ref_AlignList(alist);
	    al->str = 1;
	    al->begin = seq_begin+ar->ref_begin1;
	    al->end = seq_begin+ar->ref_end1;
	    swaln_cpy_cigar2(al, ar, rk->len);
	  }
	}
	free_align(a);
	free_align(ar);
      }
    }
  }

  /* compare all the alignments of the mate read to fix */
  if (alist->size) {
    copy_best_aln(ba, alist, st->pos-st->rpos);
  } else {
    /** global Smiths-Waterman **/
    rk_encode_nt256int8(rk);
    SiteSeq *ss = get_SSPool(ssp, st);
    ss_encode_nt256int8(ss);
    s_align *a = _ssw_align(rk->seq_sw, rk->len,
			    ss->swseq, ss->len);
    s_align *ar = _ssw_align(rk->seq_sw_r, rk->len,
			     ss->swseq, ss->len);

    if (a->score1 > ar->score1) {
      if (check_aln(a, ss->seq, rk, 0, conf)) {
	ba->str = 0;
	ba->pos = st->pos - st->rpos + a->ref_begin1;
	swaln_cpy_cigar(ba, a, rk->len);
      }
    } else {
      if (check_aln(ar, ss->seq, rk, 1, conf)) {
	ba->str = 1;
	ba->pos = st->pos - st->rpos + ar->ref_begin1;
	swaln_cpy_cigar(ba, ar, rk->len);
      }
    }
    free_align(a);
    free_align(ar);
  }

  free_AlignList(alist);
}
