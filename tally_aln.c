

#include "tally.h"

uint8_t
type_SIVar(Site *st, int8_t *ref_seq, CallConf *conf,
	   int8_t *rseq_sw, s_align *a, QIter *qter) {

  /* type target site */
  int8_t *t=ref_seq + st->rpos;
  int8_t *r=ref_seq + a->ref_begin1;
  int8_t *q=rseq_sw + a->read_begin1;
  uint32_t *cigar=a->cigar;
  if (st->rpos > (unsigned) a->ref_begin1
      && st->rpos<(unsigned) a->ref_end1) {
    uint32_t len = bam_cigar_oplen(*cigar);
    uint32_t op = bam_cigar_op(*cigar);
    while (r+len<t || op==BAM_CINS) {
      switch (op) {
      case BAM_CMATCH:
	r += len;
	q += len;
	break;
      case BAM_CINS:
	q += len;
	break;
      case BAM_CDEL:
	r += len;
	break;
      default:
	fprintf(stderr, "[%s] Error, unknown cigar, %u\n", __func__, op);
	exit(1);
      }
      cigar++;
      len = bam_cigar_oplen(*cigar);
      op = bam_cigar_op(*cigar);
    }

    if (r + len - 1 == t && 
	cigar < a->cigar + a->cigarLen - 1 &&
	(bam_cigar_op(cigar[1]) == BAM_CDEL ||
	 bam_cigar_op(cigar[1]) == BAM_CINS)) {
      return 1;
    }

    /* a reference if match or mis-match but base
     * quality is less than 10 */
    if (op==BAM_CMATCH) {

      if (q[t-r]==*t) {
	return 0;
      } else {
	qter_adv(qter, a->read_begin1+t-r);
	if (*qter->p > 33 + conf->tbaseQ) {
	  return 1;
	} else return 0;
      }
    } else return 1;
  } else return 0;
  return 0;
}

void
append_soft_clipping(VarSI_H *tvh, s_align* a, int8_t *rseq_sw, int8_t *ref_seq, uint32_t rlen) {

  if (a->cigarLen<5) {

    /* post-process cigar string, add soft-clipping */
    size_t qlen = a->read_begin1; size_t k;
    tvh->n_cigar = a->cigarLen;
    for (k=0; k<tvh->n_cigar; ++k) {
      if (bam_cigar_type(bam_cigar_op(a->cigar[k]))&1) {
	qlen += bam_cigar_oplen(a->cigar[k]);
      }
    }

    tvh->css = a->read_begin1;
    tvh->cse = rlen-qlen;

    /* fix the 2S issue: if there is a 2S and one of the
     * soft-clipping is actually a match, then extend
     * remove the soft-clipping.
     * The issue arises from the ambiguity of the score
     * equivally optimal in the scenario where mis-match
     * occurs to the 2nd to first/last position.
     * In such case, the ssw library favors the shorter
     * alignment while the seed extension favors the longer
     * alignment. */
    if (tvh->css == 2 && rseq_sw[0] == ref_seq[a->ref_begin1-2]) {
      tvh->css = 0; tvh->pos -= 2;
      a->cigar[0] += 2<<BAM_CIGAR_SHIFT;
    }
    if (tvh->cse == 2 && rseq_sw[rlen-1] == ref_seq[a->ref_end1+2]) {
      tvh->cse = 0;
      a->cigar[a->cigarLen-1] += 2<<BAM_CIGAR_SHIFT;
    }

    for (k=0; k<tvh->n_cigar; ++k) tvh->cigar[k] = a->cigar[k];

  } else {
    tvh->n_cigar = 0; /* need to realign, for saving storage */
  }
}
