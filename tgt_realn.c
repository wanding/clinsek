#include "realn.h"
#include "bam_std.h"
#include "biproc.h"

/* return the sequence on a read */
static char *bam_charseq(bam1_t *b, uint32_t beg, uint32_t end) {
  char *seq = malloc(end-beg);
  uint32_t i;
  for (i=0; beg+i<end; ++i) {
    seq[i] = bscall(bam1_seq(b), beg+i);
  }
  return seq;
}

static uint32_t
qual_sum(bam1_t *b, uint32_t beg, uint32_t end) {
  size_t i; uint32_t sum = 0;
  for (i = beg; i < end; i++) sum += bam1_qual(b)[i];
  return sum;
}


/* search indel around the target site
   only indels overlap with the target site is considered */
static void
read_type_indel(bam1_t *b, int tid, uint32_t t, AlleleList *alts, uint32_t flen, uint32_t min_baseQ, uint32_t min_exempt_length) {
  bam1_core_t *c = &b->core;
  uint32_t *cigar = bam1_cigar(b);

  uint32_t q=0, r=c->pos, op=0;
  uint32_t i, l;
  for (i=0; i<c->n_cigar; ++i) {
    op = bam_cigar_op(cigar[i]);
    l = bam_cigar_oplen(cigar[i]);

    Allele *a = 0;
    switch (op) {
    case BAM_CMATCH:
      r += l;
      q += l;
      break;
    case BAM_CINS:
      if ((unsigned) abs(r-t) < flen) {
	if ((qual_sum(b, q, q+l) / l) > min_baseQ || l>min_exempt_length) {
	  a = try_next_AlleleList(alts);
	  memset(a, 0, sizeof(Allele));
	  a->cigar = BAM_CINS;
	  a->tid = tid;
	  a->rpos = r+1;		/* 1-based */
	  a->len = l;
	  a->seq = bam_charseq(b, q, q+l);
	  break;
	}
      }
      q += l;
      break;
    case BAM_CSOFT_CLIP:
      q += l;
      break;
    case BAM_CDEL:
      if (r<=t+flen && r+l>=t-flen) {
	if (qual_sum(b, q-1, q+1) / 2.0 > min_baseQ || l>min_exempt_length) {
	  a = try_next_AlleleList(alts);
	  memset(a, 0, sizeof(Allele));
	  a->cigar = BAM_CDEL;
	  a->tid = tid;
	  a->rpos = r+1;		/* 1-based */
	  a->len = l;
	  a->seq = 0;
	  break;
	}
      }
      r += l;
      break;
    case BAM_CREF_SKIP:
      r += l;
      break;
    default:
      fprintf(stderr, "Unknown cigar, %u\n", op);
      abort();
    }
    if (r>t) break;

    if (a) {
      Allele *a2 = allele_find(alts, a);
      if (a2) {
	a2->rc++;
	if (a->seq) free(a->seq);
      } else {
	a->rc = 1;
	commit_next_AlleleList(alts);
      }
    }
  }
}

/* loop over reads mapped around the target site
   and return coverage at the target site */
static uint32_t
site_build_alts(AlleleList *al, Site *st, bamFile in, bam_index_t *idx, RealnConf *conf) {
  uint8_t tid = st->chrm;
  uint32_t pos = st->pos;
  bam1_t *b = bam_init1();
  bam_iter_t iter = bam_iter_query(idx, tid, pos-5, pos+5);
  uint32_t cov = 0;

  while (bam_iter_read(in, iter, b)>=0) {
    if (b->core.qual >= conf->min_mapQ && !(b->core.flag & BAM_FDUP)) {
      read_type_indel(b, st->chrm, st->pos, al, conf->flen, conf->min_baseQ, conf->min_exempt_length);
      if (b->core.pos > 0 &&
	  (unsigned) b->core.pos <= pos &&
	  bam_calend(&b->core, bam1_cigar(b)) >= pos) {
	cov++;
      }
    }
  }
  bam_destroy1(b);
  bam_iter_destroy(iter);
  return cov;
}


/* re-alignment takes place in 2-passes.
   1) find alternative alleles, sort the alternative alleles
   2) traverse the bam file and re-align reads if read covers alternative alleles, otherwise simply output the read into the new bam file.
*/

int
target_realn(char *in_fn, char *out_fn,
	     KCnts *kc, uint8_t *siv, RealnConf *conf) {

  fprintf(stderr, "[%s] Perform indel realignment.\n", __func__);
  bamFile in = bam_open(in_fn, "r");
  bam_header_t *header = bam_header_read(in);
  bam_header_destroy(header);
  bam_index_t *idx = bam_index_load(in_fn);
  AlleleList *alts = init_AlleleList(2);
  AlleleList *alts0 = init_AlleleList(2);
  uint32_t i, j; uint32_t cov;

  for (i=0; i<kc->sl->size; ++i) {
    if (siv[i]) {
      if ((i & 0xFFFU)==0) {
	fprintf(stderr, "\r[%s] processed %u sites\n", __func__, i);
	fflush(stderr);
      }

      Site *st = get_SiteList(kc->sl, i);
      clean_AlleleList(alts0);
      cov = site_build_alts(alts0, st, in, idx, conf);
      for (j=0; j<alts0->size; ++j) {
	Allele *a = ref_AlleleList(alts0, j);
	Allele *existing = allele_find(alts, a);
	if (existing) {
	  if (existing->rc < a->rc) existing->rc = a->rc;
	} else if (a->rc > conf->min_reads &&
		   (double) a->rc / (double) cov > conf->min_cov) {
	  copy_allele(next_ref_AlleleList(alts), a);
	}
      }
    }
  }
  bam_close(in);
  bam_index_destroy(idx);

/* #ifdef DEBUGREALN */

  /* for (i=0; i<alts->size; ++i) { */
  /*   Allele *a = ref_AlleleList(alts, i); */
  /*   fprintf(stdout, "indel alt %u: pos: %d:%u, cigar: %u, len: %d, rc: %u\n", i, a->tid, a->rpos, a->cigar, a->len, a->rc); */
  /* } */

/* #endif // DEBUGREALN */

  fprintf(stderr, "[%s] Identified %zu indels for realignment.\n",
	  __func__, alts->size);

  /* re-align */
  qsort(alts->buffer, alts->size, sizeof(Allele), cmp_allele_sort);
  bam_realn_alts(alts, in_fn, out_fn, kc->rs, conf);
  destroy_AlleleList(alts0);
  destroy_AlleleList(alts);
  return 0;
}


static void
realn_conf_init_default(RealnConf *conf) {
  conf->min_mapQ = 20;
  conf->flen = 30;
  conf->window_width = 200;
  conf->max_indel = 2;
  conf->min_reads = 1;
  conf->min_cov = 0.01;
  conf->sort = 1;
  conf->min_baseQ = 10;
  conf->min_exempt_length = 3;
  /* conf->target_only = 0; */
}

int
target_realn_nosort(char *in_fn, char *out_fn, KCnts *kc, uint8_t *siv) {

  RealnConf conf;
  realn_conf_init_default(&conf);

  return target_realn(in_fn, out_fn, kc, siv, &conf);
}

Allele* allele_find(AlleleList *alts, Allele *t) {
  uint32_t i;
  for (i=0; i<alts->size; ++i) {
    Allele *a = ref_AlleleList(alts, i);
    if (a->cigar == t->cigar &&
	a->len == t->len &&
	a->rpos == t->rpos) {
      if ((!t->seq && !a->seq) ||
	  (t->seq && a->seq && 
	   memcmp(a->seq, t->seq, t->len) == 0)) {
	return a;
      }
    }
  }
  return NULL;
}



/* catesian build alternative seqs */
void reconstruct_seqs(AlleleWindow *win, RefSeq *rs, RealnConf *conf) {
  Allele *a;
  AWSeq *as, *new_as;
  uint32_t i;
  init_AWSeq(win, rs, conf->window_width);
  for (a=win->beg; a<=win->end; ++a) {

    /* add allele into existing allele list if
       the new allele is not in overlap with any
       of the existing alleles
    */

    uint32_t n_seqs = win->seqs->size;
    for (i=0; i<n_seqs; ++i) {
      as = ref_AWSeqList(win->seqs, i);
      if (as->alts->size <= conf->max_indel &&
	  !allele_list_overlap(as->alts, a)) {
	new_as = next_ref_AWSeqList(win->seqs);
	as = ref_AWSeqList(win->seqs, i); /* re-point as since next_ref may change the address of win->seqs */
	spawn_AWSeqList(new_as, as, a, win);
      }
    }
  }
  win->up = 1;
}


/* a: new allele to be pushed to as_dst
   as_dst = as_src + [a]
 */
void spawn_AWSeqList(AWSeq *as_dst, AWSeq *as_src, Allele *a, AlleleWindow *win) {

  as_dst->alts  = dup_AllelePList(as_src->alts);
  as_dst->poses = dup_IntList(as_src->poses);
  push_AllelePList(as_dst->alts, a);

  kstring_t s;
  s.s = 0; s.m = s.l = 0;

  /* sequence before new allele */
  uint32_t new_al_pos = 0; 
  if (as_src->alts->size) {
    Allele *last = last_AllelePList(as_src->alts);
    uint32_t last_al_pos = last_IntList(as_src->poses);
    if (last->cigar == BAM_CINS) {
      /* RefAllele ====     ========
	 AltAllele =================
	 Mutation                *
       */
      new_al_pos = last_al_pos + last->len + a->rpos - last->rpos;
      kputsn(as_src->seq, new_al_pos, &s);
    } else if (last->cigar == BAM_CDEL) {
      /* RefAllele =================
	 AltAllele =====     =======
	 Mutation                *
      */
      new_al_pos = last_al_pos + a->rpos - last->rpos;
      kputsn(as_src->seq, new_al_pos, &s);
    } else {
      new_al_pos = a->rpos - win->rsbeg;
      kputsn(as_src->seq, new_al_pos, &s);
    }
  } else {
    new_al_pos = a->rpos - win->rsbeg;
    kputsn(as_src->seq, new_al_pos, &s);
  }

  push_IntList(as_dst->poses, new_al_pos);

  /* sequence after new allele */
  if (a->cigar == BAM_CINS) {
    kputsn(a->seq, a->len, &s);
    kputsn(as_src->seq + new_al_pos, as_src->len - new_al_pos, &s);
  } else if (a->cigar == BAM_CDEL) {
    kputsn(as_src->seq + new_al_pos + a->len,
	   as_src->len - a->len - new_al_pos, &s);
  }

  as_dst->len = s.l;
  as_dst->seq = s.s;
  as_dst->seq_sw = nt256char_encode_nt256int8(as_dst->seq, as_dst->len);

}

Allele *find_chromosome_first(AlleleList *alts, int32_t tid) {
  uint32_t i;
  for (i=0; i<alts->size; ++i) {
    Allele *a = ref_AlleleList(alts, i);
    if (a->tid == tid) {
      return a;
    }
  }
  return NULL;
}

static void
window_init(AlleleWindow *win, AlleleList *alts, uint32_t tid) {
  win->beg = find_chromosome_first(alts, tid);
  win->end = win->beg;
  win->up  = 0;
}

static void
window_update(AlleleWindow *win, bam1_t *b, Allele *last) {

  if (!win->beg) return; /* no alleles left in the current chromosome */

  bam1_core_t *c = &b->core;
  while (win->beg->rpos <= (unsigned) c->pos - 20) {
    win->beg++;
    win->up = 0;
    if (win->beg > last || win->beg->tid != c->tid) {
      win->beg = 0;
      return;
    }
  }
  /* if (win->beg > last || win->beg->tid != c->tid) { */
  /*   win->beg = 0;		/\* end of this chromosome *\/ */
  /*   win->up = 0; */
  /*   return; */
  /* } */

  uint32_t read_end = bam_calend(c, bam1_cigar(b));
  while (win->end < last &&
	 win->end[1].tid == c->tid &&
	 win->end[1].rpos <= read_end + 20) {
    win->end++;
    win->up = 0;
  }
}

/* keep alternative alleles sorted
   since bam reads are also sorted
   we look for alternative alleles that affect the read

   the "alternative window" is the collection of
   alternative mutations defined by the "beg" and "end"
   pointer to the sorted allele list.

 */
void
bam_realn_alts(AlleleList *alts,
	       char *in_fn, char *out_fn,
	       RefSeq *rs, RealnConf *conf) {

  AlleleWindow win = { .seqs = init_AWSeqList(2) };
  Allele *last = last_ref_AlleleList(alts);

  samfile_t *in = samopen(in_fn, "rb", 0);
  samfile_t *out = samopen(out_fn, "wb", in->header);

  BIProc *bip = init_BIProc(in, out);
  int curr_tid = -1;
  bam1_t *b, *b1, *b2;
  bam1_core_t *c, *c1, *c2;
  while ((b = bip_read1(bip)) != NULL) {
    c = &b->core;
    if (c->tid < 0) {
      bip_write1r(bip, b);
      continue;
    } /* output unmapped reads */

    bip_put_read(bip, b);

    if (c->tid != curr_tid) {	/* change of chromosome */
      fprintf(stderr, "\r[%s] Processing chromosome %s\033[K", __func__, tid2chrm(rs, c->tid));
      fflush(stderr);
      window_init(&win, alts, c->tid);
      curr_tid = c->tid;
    }

    window_update(&win, b, last);
    uint32_t read_end = bam_calend(c, bam1_cigar(b));
    if (win.beg && win.beg->rpos <= read_end) {
      if (!win.up) reconstruct_seqs(&win, rs, conf);
      read_realn_alts(b, &win);

      /* re-edit insert size of matched inserts */
      if (bip->ins->b1 && bip->ins->b2) {
	b1 = bip->ins->b1; b2 = bip->ins->b2;
	c1 = &b1->core; c2 = &b2->core;
	c1->mpos = c2->pos; c2->mpos = c1->pos;
	if (c1->flag & BAM_FREVERSE) c1->isize = c2->pos - c1->pos - bam_cigar2qlen(c1, bam1_cigar(b1));
	else c1->isize = c2->pos - c1->pos + bam_cigar2qlen(c2, bam1_cigar(b2));
	c2->isize = -c1->isize;
      }

    }
    bip_write1i(bip);
  }
  /* khint_t k; */
  /* for (k=kh_begin(bip->im); k<kh_end(bip->im); ++k) { */
  /*   if (kh_exist(bip->im, k)) { */
  /*     insert1_t *ins = kh_val(bip->im, k); */
  /*     if (ins->b1) fprintf(stderr, "insert left (1): %s\n", ins->b1->data); */
  /*     if (ins->b2) fprintf(stderr, "insert left (2): %s\n", ins->b2->data); */
  /*   } */
  /* } */
  /* fprintf(stderr, "imp insert size: %zu\t%zu\n", bip->imp->n, bip->imp->cnt); */
  /* fprintf(stderr, "kmp read size: %zu\t%zu\n", bip->rmp->n, bip->rmp->cnt); */
  fprintf(stderr, "\n");
  AlleleWindow_free(&win);
  samclose(in);
  samclose(out);
  free_BIProc(bip);
}

void
read_realn_alts(bam1_t *b, AlleleWindow *win) {
  int8_t *read_seq = nt16_decode_nt256int8(bam1_seq(b), b->core.l_qseq);
  uint32_t i;
  s_align *best_aln = 0;
  AWSeq *best_as = 0;
  for (i=0; i<win->seqs->size; ++i) {
    AWSeq *as = ref_AWSeqList(win->seqs, i);
    s_align *aln = _ssw_align(read_seq, b->core.l_qseq,
			      as->seq_sw, as->len);
    if (i == 0) {
      best_aln = aln;
      best_as = as;
    } else if (aln->score1 > best_aln->score1 + S_GAPOPEN-S_MISMATCH) {
      free_align(best_aln);
      best_aln = aln;
      best_as = as;
    } else {
      free_align(aln);
    }
  }
  aln_recover_cigar(b, best_aln, best_as, win);
  free_align(best_aln);
  free(read_seq);
}

int leading_cigar(CigarList *cgr) {
  uint32_t i;
  for (i=0; i<cgr->size; ++i) {
    uint32_t op = bam_cigar_op(get_CigarList(cgr, i));
    if (op == BAM_CMATCH || op == BAM_CDIFF) return 0;
  }
  return 1;
}

#define cigar_append(cgr, op, l) push_CigarList((cgr), bam_cigar_gen((l), (op)))

/* merge contiguous insertion and deletion */
CigarList *cigar_merge_indel(CigarList *cgr) {

  CigarList *cgr2 = init_CigarList(2);
  uint32_t i;
  for (i=0; i<cgr->size; ++i) {
    uint32_t c1 = get_CigarList(cgr, i);
    uint32_t o1 = bam_cigar_op(c1);
    uint32_t l1 = bam_cigar_oplen(c1);
    if (i+2<cgr->size) {
      uint32_t c2 = get_CigarList(cgr, i+1);
      uint32_t c3 = get_CigarList(cgr, i+2);
      uint32_t o2 = bam_cigar_op(c2);
      uint32_t o3 = bam_cigar_op(c3);
      uint32_t l2 = bam_cigar_oplen(c2);
      uint32_t l3 = bam_cigar_oplen(c3);
      int32_t nl, ml;
      if (o1 == BAM_CMATCH) {
	if (o2 == BAM_CINS && o3 == BAM_CDEL) {
	  nl = l2 - l3;
	  ml = min(l2, l3);
	  cigar_append(cgr2, BAM_CMATCH, l1+ml);
	  if (nl > 0) cigar_append(cgr2, BAM_CINS, nl);
	  else if (nl < 0) cigar_append(cgr2, BAM_CDEL, -nl);
	  i += 2;
	} else if (o2 == BAM_CDEL && o3 == BAM_CINS) {
	  nl = l3 - l2;
	  ml = min(l2, l3);
	  cigar_append(cgr2, BAM_CMATCH, l1+ml);
	  if (nl > 0) cigar_append(cgr2, BAM_CINS, nl);
	  else if (nl < 0) cigar_append(cgr2, BAM_CDEL, -nl);
	  i += 2;
	} else {
	  cigar_append(cgr2, o1, l1);
	}
      } else {
	cigar_append(cgr2, o1, l1);
      }
    } else {
      cigar_append(cgr2, o1, l1);
    }
  }
  free_CigarList(cgr);
  return cgr2;
}

/* void */
/* cigar_append(CigarList *cgr, uint32_t op, uint32_t l) { */
/*   if (l<=0) return; */
/*   if (op == BAM_CSOFT_CLIP && cgr->size > 0 && */
/*       bam_cigar_op(get_CigarList(cgr, cgr->size-1)) == BAM_CSOFT_CLIP) { */
/*     uint32_t *c = ref_CigarList(cgr, cgr->size-1); */
/*     *c += l<<BAM_CIGAR_SHIFT; */
/*   } else { */

/*     if (op == BAM_CDEL && leading_cigar(cgr)) { */
/*       if (cgr->size > 0 && bam_cigar_op(get_CigarList(cgr, cgr->size-1)) == BAM_CSOFT_CLIP) { */
/* 	uint32_t *c = ref_CigarList(cgr, cgr->size-1); */
/* 	*c += l<<BAM_CIGAR_SHIFT; */
/*       } */
/*     } */

/*     push_CigarList(cgr, bam_cigar_gen(l, op)); */
/*   } */
/* } */

static void
set_bam_cigar(bam1_t *b, uint32_t c_pos, CigarList *cgr) {
  bam1_core_t *c = &b->core;
  c->pos = c_pos;
  int tmp_len = b->data_len+(cgr->size - c->n_cigar)*4;
  uint8_t *tmp = malloc(tmp_len);
  /* copy name */
  memcpy(tmp, bam1_qname(b), c->l_qname);
  /* copy cigar */
  memcpy(tmp + c->l_qname, cgr->buffer, cgr->size*4);
  /* copy rest */
  memcpy(tmp + c->l_qname + cgr->size*4,
	 b->data + c->n_cigar*4 + c->l_qname,
	 b->data_len - c->n_cigar*4 - c->l_qname);
  c->n_cigar = cgr->size;
  /* copy the whole thing */
  if (tmp_len > b->m_data) b->data = realloc(b->data, tmp_len);
  b->data_len = tmp_len;
  memcpy(b->data, tmp, tmp_len);
  free(tmp);
}

/* given alignment and alternative allele, restore the cigar
   on the original reference sequence 

   return 1 if successful, 0 if failure
*/

int aln_recover_cigar(bam1_t *b, s_align *aln, AWSeq *as,
		      AlleleWindow *win) {
  /* if there is no alternative,
     just leave the original alignment */
  if (as->alts->size == 0) return 1;

  /* mapping from alternative sequence to reference */
  int32_t *as2ref = calloc(as->len, sizeof(uint32_t));
  uint32_t i, j, k, rpos;
  rpos = win->rsbeg; j=0;
  for (i=0; i<as->alts->size; ++i) {
    Allele *a = get_AllelePList(as->alts, i);
    for (; rpos < a->rpos; ++rpos, ++j) {
      as2ref[j] = rpos;
    }
    if (a->cigar == BAM_CINS) {
      for (k=a->len; k; --k, ++j) as2ref[j] = -1;
    } else if (a->cigar == BAM_CDEL) {
      rpos += a->len;
    }
  }
  for (; j<as->len; ++j, ++rpos) as2ref[j] = rpos;


  /* mapping from read to reference via alternative sequence */
  bam1_core_t *c = &b->core;
  int32_t *read2ref = calloc(c->l_qseq, sizeof(uint32_t));
  uint32_t apos = aln->ref_begin1;
  uint32_t qpos = aln->read_begin1;
  uint32_t op, l;
  int ai;
  for (j=0; j<qpos; ++j) read2ref[j] = -1;
  for (ai=0; ai<aln->cigarLen; ++ai) {
    op = bam_cigar_op(aln->cigar[ai]);
    l = bam_cigar_oplen(aln->cigar[ai]);
    switch (op) {
    case BAM_CMATCH:
      for (k=l; k; --k, ++qpos, ++apos) {
	read2ref[qpos] = as2ref[apos];
      }
      break;
    case BAM_CDEL:
      apos += l;
      break;
    case BAM_CINS:
      for (k=l; k; --k, ++qpos) read2ref[qpos] = -1;
      break;
    case BAM_CREF_SKIP:
      apos += l;
      break;
    default:
      fprintf(stderr, "unknown cigar %u\n", op);
      exit(1);
    }
  }
  for (; qpos < (unsigned) c->l_qseq; ++qpos) read2ref[qpos] = -1;


/*   if (strcmp(b->data, "HWI-ST959:193:C3556ACXX:3:2305:14726:93081") == 0) { */
/*     fprintf(stderr, "start: %u\t%u\n", win->rsbeg, b->core.flag); */
/*     for (i=0; i<as->len; ++i) */
/*       fprintf(stderr, "%d(%c),", as2ref[i], as->seq[i]); */
/*     fprintf(stderr, "\n"); */
/*     for (i=0; i<c->l_qseq; ++i) */
/*       fprintf(stderr, "%d(%c),", read2ref[i], bscall(bam1_seq(b), i)); */
/*     fprintf(stderr, "\n"); */
/*   } */

  /* reassign the read cigar */
  CigarList *cgr = init_CigarList(2);
  uint32_t pos = 0; i=0;
  while (i< (unsigned) c->l_qseq) {
    if (!pos) {
      while (read2ref[i] < 0) ++i;
      if (i) cigar_append(cgr, BAM_CSOFT_CLIP, i);
      pos = read2ref[i];
      j=i;
    } else if (read2ref[i] < 0) {
      j=i;
      do ++i; while (read2ref[i] < 0);
      if (i==(unsigned) c->l_qseq) cigar_append(cgr, BAM_CSOFT_CLIP, i-j); 
      else cigar_append(cgr, BAM_CINS, i-j);
    } else {
      j=i;
      do ++i; while (i<(unsigned) c->l_qseq && read2ref[i] == read2ref[i-1] + 1);
      cigar_append(cgr, BAM_CMATCH, i-j);
      j=i;
    }

    if (j && j<(unsigned) c->l_qseq && 
	read2ref[j-1] > 0 && read2ref[i] > read2ref[i-1] + 1) {
      cigar_append(cgr, BAM_CDEL, read2ref[i] - read2ref[j-1] - 1);
    }
  }
  
  cgr = cigar_merge_indel(cgr);
  free(read2ref); free(as2ref);
  set_bam_cigar(b, pos-1, cgr);
  free_CigarList(cgr);
  
  return 1;
}

/* /\* given alignment and alternative allele, restore the cigar */
/*    on the original reference sequence  */

/*    return 1 if successful, 0 if failure */
/* *\/ */
/* int aln_recover_cigar(bam1_t *b, s_align *aln, */
/* 		      AWSeq *as, AlleleWindow *win) { */

/*   /\* if there is no alternative, */
/*      just leave the original alignment *\/ */
/*   if (as->alts->size == 0) return 1; */

/*   bam1_core_t *c = &b->core; */
/*   uint32_t *cigar = aln->cigar; */

/*   /\* find first position *\/ */

/*   uint32_t rpos = aln->ref_begin1; /\* on alternative allele *\/ */
/*   uint32_t qpos = aln->read_begin1; */

/*   int32_t ci = 0; */
/*   uint32_t op = bam_cigar_op(cigar[ci]); */
/*   uint32_t l = bam_cigar_oplen(cigar[ci]); */

/*   uint32_t ai = 0; */
/*   uint32_t al_pos = get_IntList(as->poses, ai); */
/*   Allele *al = get_AllelePList(as->alts, ai); */

/*   CigarList *cgr = init_CigarList(2); */
/*   if (qpos > 0) { */
/*     cigar_append(cgr, BAM_CSOFT_CLIP, qpos); */
/*   } */

/*   uint32_t c_pos = win->rsbeg + rpos - 1; /\* on refernece allele, -1 for 0-base *\/ */
/*   while (al_pos < rpos) { */
/*     if (al->cigar == BAM_CINS) { */
/*       if (al_pos+al->len < rpos) { */
	
/* 	/\* RefAllele ========          ========= */
/* 	   AltAllele ========----------========= */
/* 	   Read                   +++++++++++ */
/* 	 *\/ */

/* 	cigar_append(cgr, BAM_CSOFT_CLIP, al_pos+al->len-rpos); */
/* 	c_pos = al_pos + al->len; */
/* 	qpos += al_pos + al->len - rpos; */
/*       } else { */

/* 	/\* RefAllele ====          =============== */
/* 	   AltAllele ====----------=============== */
/* 	   Read                      +++++++++++ */
/* 	 *\/ */

/* 	c_pos -= al->len; */
/*       } */
/*     } else if (al->cigar == BAM_CDEL) { */

/*       /\* RefAllele ====----------=============== */
/* 	 AltAllele ====          =============== */
/* 	 Read                      +++++++++++ */
/*       *\/ */

/*       c_pos += al->len; */
/*     } */
/*     if (++ai >= as->alts->size) return 1; */
/*     al_pos = get_IntList(as->poses, ai); */
/*     al = get_AllelePList(as->alts, ai); */
/*   } */

/*   for (; ci<aln->cigarLen; ++ci) { */
/*     op = bam_cigar_op(cigar[ci]); */
/*     l = bam_cigar_oplen(cigar[ci]); */
/*     switch (op) { */
/*     case BAM_CMATCH: */
/*       if (rpos <= al_pos && rpos + l > al_pos) { */
/* 	if (al->cigar == BAM_CDEL) { */

/* 	  /\* RefAllele ====----------=============== */
/* 	     AltAllele ====          =============== */
/* 	     Read       +++----------+++++ */
/* 	  *\/ */

/* 	  if (rpos < al_pos) cigar_append(cgr, BAM_CMATCH, al_pos-rpos); */
/* 	  cigar_append(cgr, BAM_CDEL, al->len); */
/* 	  cigar_append(cgr, BAM_CMATCH, l-(al_pos-rpos)); */
/* 	} else if (al->cigar == BAM_CINS) { */
/* 	  if (al_pos - rpos + al->len >= l) { */
/* 	    if (ci == aln->cigarLen-1) { /\* last cigar *\/ */

/* 	      /\* RefAllele ====          =============== */
/* 		 AltAllele ====----------=============== */
/* 		 Read        ++++++++++ */
/* 	      *\/ */

/* 	      cigar_append(cgr, BAM_CMATCH, al_pos-rpos); */
/* 	      cigar_append(cgr, BAM_CSOFT_CLIP, l-(al_pos-rpos)); */
/* 	    } else return 1;	/\* fall-back mode, use original cigar *\/ */
/* 	  } else { */

/* 	    /\* RefAllele ====          =============== */
/* 	       AltAllele ====----------=============== */
/* 	       Read        ++++++++++++++ */
/* 	    *\/ */

/* 	    cigar_append(cgr, BAM_CMATCH, al_pos-rpos); */
/* 	    cigar_append(cgr, BAM_CINS, al->len); */
/* 	    cigar_append(cgr, BAM_CMATCH, l-(al_pos-rpos)-al->len); */
/* 	  } */
/* 	} */
/* 	if (++ai < as->alts->size) { */
/* 	  al_pos = get_IntList(as->poses, ai); */
/* 	  al = get_AllelePList(as->alts, ai); */
/* 	} */
/*       } else { */
/* 	cigar_append(cgr, BAM_CMATCH, l); */
/*       } */
/*       rpos += l; */
/*       qpos += l; */
/*       break; */
/*     case BAM_CINS: */
/*       cigar_append(cgr, BAM_CINS, l); */
/*       qpos += l; */
/*       break; */
/*     case BAM_CDEL: */
/*       if (rpos <= al_pos && rpos + l > al_pos) { */
/* 	/\* RefAllele =====-------==========  */
/* 	   AltAllele =====       ========== */
/* 	   Read       ++++       ++++++++ */
/* 	 *\/ */
/* 	cigar_append(cgr, BAM_CDEL, al->len+l); */
/* 	if (++ai < as->alts->size) { */
/* 	  al_pos = get_IntList(as->poses, ai); */
/* 	  al = get_AllelePList(as->alts, ai); */
/* 	} */
/* 	/\* fprintf(stderr, "[%s:%d] Warning, complex SV in deletion, skip read\n%s\n", __func__, __LINE__, bam1_qname(b)); *\/ */
/* 	/\* return 0; *\/ */
/*       } */
/*       cigar_append(cgr, BAM_CDEL, l); */
/*       rpos += l; */
/*       break; */
/*     } */
/*   } */

/*   uint32_t qlen = bam_cigar2qlen(c, bam1_cigar(b)); */
/*   if (qpos < qlen) { */
/*     cigar_append(cgr, BAM_CSOFT_CLIP, qlen-qpos); */
/*   } */
/*   set_bam_cigar(b, c_pos, cgr); */
/*   free_CigarList(cgr); */

/*   return 1; */
/* } */


static int usage() {
  fprintf(stderr, "\n");
  fprintf(stderr, "Usage: clinsek realn [options] -i in.bam -o out.bam -s sitelist -r hg19.fa\n");
  fprintf(stderr, "Input options:\n");
  fprintf(stderr, "     -i FILE   input bam [required]\n");
  fprintf(stderr, "     -s FILE   site list [required]\n");
  fprintf(stderr, "     -r FILE   reference file [required]\n");
  fprintf(stderr, "     -o FILE   output bam [input+.realn]\n");
  fprintf(stderr, "     -q INT    mininum mapping quality in suggesting indel [20]\n");
  fprintf(stderr, "     -w INT    width of flanking region extended from the indels in forming allele window [200]\n");
  fprintf(stderr, "     -m INT    maximum number of indels to allow in the alternative allele [2]\n");
  fprintf(stderr, "     -n INT    minimum reads support for candidate indel [2]\n");
  fprintf(stderr, "     -p FLOAT  minimum coverage ratio for candidate indel [0.01]\n");
  fprintf(stderr, "     -b INT    minimum base quality in suggesting indel [10]\n");
  fprintf(stderr, "     -t        toggle to turn OFF sorting and indexing after re-alignment\n");
  fprintf(stderr, "     -f INT    span such that indels within the span of any target site is considered [30]\n");
  fprintf(stderr, "     -e        minimum length for indel to exempt from base quality filtering [4]\n");
  fprintf(stderr, "     -l        site filter\n");
  fprintf(stderr, "     -h        this help\n");
  fprintf(stderr, "\n");
  return 1;
}

int main_realn(int argc, char *argv[]) {

  RealnConf conf;
  realn_conf_init_default(&conf);

  char *in_fn=0, *out_fn=0;
  char *sfn=0, *rfn=0; char *filter_fn=0;
  int c;
  if (argc < 2) return usage();
  while ((c = getopt(argc, argv, "i:o:s:r:l:q:f:b:w:m:t:n:e:p:g:h")) >= 0) {
    switch (c) {
    case 'i': in_fn = optarg; break;
    case 'o': out_fn = strdup(optarg); break;
    case 's': sfn = optarg; break;
    case 'r': rfn = optarg; break;
    case 'l': filter_fn = optarg; break;
    case 'q': conf.min_mapQ = atoi(optarg); break;
    case 'f': conf.flen = atoi(optarg); break;
    case 'w': conf.window_width = atoi(optarg); break;
    case 'm': conf.max_indel = atoi(optarg); break;
    case 't': conf.sort = 0; break;
    case 'n': conf.min_reads = atoi(optarg); break;
    case 'p': conf.min_cov = atof(optarg); break;
    case 'b': conf.min_baseQ = atoi(optarg); break;
    case 'e': conf.min_exempt_length = atoi(optarg); break;
    /* case 'g': conf.target_only = 1; break; */
    case 'h': return usage();
    default:
      fprintf(stderr, "[%s] Unrecognized command: %c.\n", __func__, c);
      exit(1);
    }
  }

  if (!in_fn) return usage();

  if (!out_fn) out_fn = wasprintf("%s.realn", in_fn);

  KCnts *kc = load_sites(sfn, rfn);

  SiteFilter *fter = load_filter(filter_fn, kc->sl->size);
  target_realn(in_fn, out_fn, kc, fter->buffer, &conf);

  if (conf.sort) {
    bam_sort_ip(out_fn);
    bam_index_build(out_fn);
  }

  free_SiteFilter(fter);
  free_KCnts(kc);
  free(out_fn);

  return 0;
}

