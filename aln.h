#ifndef _WZ_ALN_H_
#define _WZ_ALN_H_

#include "site.h"
#include "kseq.h"
#include "ssw.h"
#include "ref.h"
#include "kcnts.h"
#include "rkgen.h"
#include "samtools/sam.h"

WZ_KSEQ_DECLARE(gzFile)

typedef struct {
  char *p;
  uint8_t d;			/* direction */
} QIter;


static inline void
qter_adv(QIter *qter, int32_t inc) {
  if (qter->d) qter->p -= inc;
  else qter->p += inc;
}

/* utility functions */
/* int seed_extend_pos(char *rs, char *ds, Kmer *k, uint32_t ks, uint32_t rlen, uint8_t *cs1, uint8_t *cs2, uint8_t *m, char *t); */
int seed_extend_pos(char *rs, RKGen *rk, Kmer *k, uint8_t *cs1, uint8_t *cs2, uint8_t *m, char *t);

/* int seed_extend_neg(char *rs, char *ds, Kmer *k, uint32_t ks, uint32_t rlen, uint8_t *cs1, uint8_t *cs2, uint8_t *m, char *t); */
int seed_extend_neg(char *rs, RKGen *rk, Kmer *k, uint8_t *cs1, uint8_t *cs2, uint8_t *m, char *t);

int check_aln(s_align *aln, char *refseq, RKGen *rk, int str, CallConf *conf);

#endif /* _WZ_ALN_H_ */
