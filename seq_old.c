#include "seq.h"

#define is_entity_null(flags, idx)    ((flags)[(idx)>>4]>>(((idx)&0x0f)<<1)&0x01)
#define is_entity_del(flags, idx)     ((flags)[(idx)>>4]>>(((idx)&0x0f)<<1)&0x02)
#define exists_entity(flags, idx)     (!((flags)[(idx)>>4]>>(((idx)&0x0f)<<1)&0x03))
#define set_entity_null(flags, idx)   ((flags)[(idx)>>4] |= (0x01u<<(((idx)&0x0f)<<1)))
#define set_entity_del(flags, idx)    ((flags)[(idx)>>4] |= (0x02u<<(((idx)&0x0f)<<1)))
#define clear_entity_null(flags, idx) ((flags)[(idx)>>4] &= ~(0x01u<<(((idx)&0x0f)<<1)))
#define clear_entity_del(flags, idx)  ((flags)[(idx)>>4] &= ~(0x02u<<(((idx)&0x0f)<<1)))

#define ffwrite(ptr, e_size, size, file) (e_size * fwrite(ptr, e_size, size, file))
#define ffread(ptr, e_size, size, file) (e_size * fread(ptr, e_size, size, file))

#define seq_equals(k1, k2, len) (memcmp((k1)->s, (k2)->s, len) == 0)

#undef get16bits
#if (defined(__GNUC__) && defined(__i386__)) || defined(__WATCOMC__)	\
  || defined(_MSC_VER) || defined (__BORLANDC__) || defined (__TURBOC__)
#define get16bits(d) (*((const uint16_t *) (d)))
#endif

#if !defined (get16bits)
#define get16bits(d) ((((uint32_t)(((const uint8_t *)(d))[1])) << 8)	\
		      +(uint32_t)(((const uint8_t *)(d))[0]) )
#endif


static inline uint32_t SuperFastHash (const char * data, int len) {

  uint32_t hash = len, tmp;
  int rem;

  if (len <= 0 || data == NULL) return 0;

  rem = len & 3;
  len >>= 2;

  /* Main loop */
  for (;len > 0; len--) {
    hash  += get16bits (data);
    tmp    = (get16bits (data+2) << 11) ^ hash;
    hash   = (hash << 16) ^ tmp;
    data  += 2*sizeof (uint16_t);
    hash  += hash >> 11;
  }

  /* Handle end cases */
  switch (rem) {
  case 3: hash += get16bits (data);
    hash ^= hash << 16;
    hash ^= ((signed char)data[sizeof (uint16_t)]) << 18;
    hash += hash >> 11;
    break;
  case 2: hash += get16bits (data);
    hash ^= hash << 11;
    hash += hash >> 17;
    break;
  case 1: hash += (signed char)*data;
    hash ^= hash << 10;
    hash += hash >> 1;
  }

  /* Force "avalanching" of final 127 bits */
  hash ^= hash << 3;
  hash += hash >> 5;
  hash ^= hash << 4;
  hash += hash >> 17;
  hash ^= hash << 25;
  hash += hash >> 6;

  return hash;
}

static inline uint32_t seq_hash_code(const Seq *s, size_t len){
  /* const unsigned char *s = s.seq; */
  /* uint32_t h = *s; */
  /* if (h) for (++s ; len--; ++s) h = (h << 5) - h + *s; */
  /* return h; */
  return SuperFastHash((char *) s->s, len);
}

static inline int SeqHash_is_prime(size_t num){
  size_t i, max;
  if(num < 4) return 1;
  if(num % 2 == 0) return 0;
  max = (size_t)sqrt((float)num);
  for(i=3;i<max;i+=2){ if(num % i == 0) return 0; }
  return 1;
}

static inline size_t SeqHash_find_next_prime(size_t num){
  if(num % 2 == 0) num ++;
  while(1){ if(SeqHash_is_prime(num)) return num; num += 2; }
}

static inline SeqHash* init2_SeqHash(uint32_t size, float factor){
  SeqHash *set;
  set		   = (SeqHash*)malloc(sizeof(SeqHash));
  set->e_size	   = sizeof(Seq*);
  set->size	   = size;
  set->count	   = 0;
  set->ocp	   = 0;
  set->load_factor = factor;
  set->max	   = set->size * set->load_factor;
  set->iter_ptr    = 0;
  set->array       = calloc(set->size, set->e_size);
  set->flags       = malloc((set->size + 15)/16 * 4);
  memset(set->flags, 0x55, (set->size + 15) / 16 * 4);
  return set;
}

SeqHash* init_SeqHash(uint32_t size){
  return init2_SeqHash(size, 0.67f);
}

Seq** get_SeqHash(SeqHash *set, Seq *key, size_t len){
  Seq **e;
  size_t hc;
  hc = seq_hash_code(key, len) % set->size;
  while(1){
    if(is_entity_null(set->flags, hc)){
      return NULL;
    } else if(is_entity_del(set->flags, hc)){
    } else {
      e = ((Seq**)set->array) + hc;
      if(seq_equals(*e, key, len)) return e;
    }
    hc ++;
    hc %= set->size;
  }
  return NULL;
}

static inline void encap_SeqHash(SeqHash *set, size_t num, size_t len);

Seq** prepare_SeqHash(SeqHash *set, Seq *key, int *exists, size_t len){
  Seq **e;
  size_t hc, d;
  encap_SeqHash(set, 1, len);
  hc = seq_hash_code(key, len) % set->size;
  d = set->size;
  while(1){
    if(is_entity_null(set->flags, hc)){
      if(d == set->size){
	clear_entity_null(set->flags, hc);
	set->ocp ++;
      } else {
	hc = d;
	clear_entity_del(set->flags, hc);
      }
      *exists = 0;
      set->count ++;
      e = ((Seq**)set->array) + hc;
      return e;
    } else if(is_entity_del(set->flags, hc)){
      if(d == set->size) d = hc;
    } else {
      e = ((Seq**)set->array) + hc;
      if(seq_equals(*e, key, len)){
	*exists = 1;
	return e;
      }
    }
    hc ++;
    hc %= set->size;
  }
  return NULL;
}

static inline int exists_SeqHash(SeqHash *set, Seq *key, size_t len){
  Seq **e;
  size_t hc;
  hc = seq_hash_code(key, len) % set->size;
  while(1){
    if(is_entity_null(set->flags, hc)){
      return 0;
    } else if(is_entity_del(set->flags, hc)){
    } else {
      e = ((Seq**)set->array) + hc;
      if(seq_equals(*e, key, len)) return 1;
    }
    hc ++;
    hc %= set->size;
  }
  return 0;
}

static inline Seq** add_SeqHash(SeqHash *set, Seq *key, size_t len){
  Seq **e;
  size_t d, hc;
  hc = seq_hash_code(key, len) % set->size;
  d  = set->size;
  do{
    if(is_entity_null(set->flags, hc)){
      if(d == set->size){
	d = hc;
	clear_entity_null(set->flags, d);
	set->ocp ++;
      } else {
	clear_entity_del(set->flags, d);
      }
      e = ((Seq**)set->array) + d;
      *e = key;
      set->count ++;
      return e;
    } else if(is_entity_del(set->flags, hc)){
      if(d == set->size) d = hc;
    } else {
      e = ((Seq**)set->array) + hc;
      if(seq_equals(*e, key, len)){
	return e;
      }
    }
    if(hc + 1 == set->size) hc = 0;
    else hc = hc + 1;
  } while(1);
  return NULL;
}

static inline Seq** put_SeqHash(SeqHash *set, Seq *key, size_t len){
  encap_SeqHash(set, 1, len);
  return add_SeqHash(set, key, len);
}

static inline void delete_SeqHash(SeqHash *set, Seq **key){
  set_entity_del(set->flags, key - set->array); set->count --; }

static inline int remove_SeqHash(SeqHash *set, Seq *key, size_t len){
  Seq **e ;
  size_t hc;
  hc = seq_hash_code(key, len) % set->size;
  while(1){
    if(is_entity_null(set->flags, hc)){
      return 0;
    } else if(is_entity_del(set->flags, hc)){
    } else {
      e = ((Seq**)set->array) + hc;
      if(seq_equals(*e, key, len)){
	set->count --;
	set_entity_del(set->flags, hc);
	return 1;
      }
    }
    hc ++;
    hc %= set->size;
  }
  return 0;
}

static inline void reset_iter_SeqHash(SeqHash *set){ set->iter_ptr = 0; }

static inline int iter_SeqHash(SeqHash *set, Seq **ret){
  if(set->iter_ptr >= set->size) return 0;
  while(set->iter_ptr < set->size){
    if(exists_entity(set->flags, set->iter_ptr)){
      *ret = *(((Seq**)set->array) + set->iter_ptr);
      set->iter_ptr ++;
      return 1;
    }
    set->iter_ptr ++;
  }
  return 0;
}

static inline Seq** ref_iter_SeqHash(SeqHash *set){
  if(set->iter_ptr >= set->size) return NULL;
  while(set->iter_ptr < set->size){
    if(exists_entity(set->flags, set->iter_ptr)){
      return (((Seq**)set->array) + set->iter_ptr++);
    }
    set->iter_ptr ++;
  }
  return NULL;
}

int64_t count_SeqHash(SeqHash *set){
  return set->count;
}

static inline void clear_SeqHash(SeqHash *set){
  memset(set->flags, 0x55, (set->size + 15) / 16 * 4);
  set->count = 0;
  set->ocp   = 0;
  set->iter_ptr = 0;
}

static inline size_t sizeof_SeqHash(SeqHash *set){
  return sizeof(size_t) * 3 + sizeof(float) + set->e_size * set->size
    + sizeof(uint32_t) * ((set->size + 15) / 16);
}

static inline size_t dump_SeqHash(SeqHash *set, FILE *out){
  size_t len, i, n;
  n =  ffwrite(&set->e_size, sizeof(size_t), 1, out);
  n += ffwrite(&set->size, sizeof(size_t), 1, out);
  n += ffwrite(&set->count, sizeof(size_t), 1, out);
  n += ffwrite(&set->load_factor, sizeof(float), 1, out);
  for(i=0;i<set->size;i+=1024){
    len = set->size - i;
    if(len > 1024) len = 1024;
    n += ffwrite(set->array + i * set->e_size, set->e_size, len, out);
  }
  for(i=0;i<(set->size+15)/16;i+=1024){
    len = (set->size + 15) / 16 - i;
    if(len > 1024) len = 1024;
    n += ffwrite(set->flags + i, sizeof(uint32_t), len, out);
  }
  fflush(out);
  return n;
}

static inline SeqHash* load_SeqHash(FILE *in){
  SeqHash *set;
  size_t n;
  set = (SeqHash*)malloc(sizeof(SeqHash));
  n =  ffread(&set->e_size, sizeof(size_t), 1, in);
  n += ffread(&set->size, sizeof(size_t), 1, in);
  n += ffread(&set->count, sizeof(size_t), 1, in);
  n += ffread(&set->load_factor, sizeof(float), 1, in);
  set->max   = set->size * set->load_factor;
  set->array = malloc(set->size * set->e_size);
  n += ffread(set->array, set->e_size, set->size, in);
  set->flags = (uint32_t*)malloc((set->size + 15) / 16 * 4);
  n += ffread(set->flags, sizeof(uint32_t), (set->size + 15) / 16, in);
  return set;
}

void free_SeqHash(SeqHash *set){
  free(set->array);
  free(set->flags);
  free(set);
}

static inline void encap_SeqHash(SeqHash *set, size_t num, size_t len){
  uint32_t *flags, *f;
  uint64_t i, n, size, hc;
  Seq *key;
  Seq *tmp;
  if(set->ocp + num <= set->max) return;
  n = set->size;
  do{ n = SeqHash_find_next_prime(n * 2); } while(n * set->load_factor < set->count + num);
  set->array = realloc(set->array, n * set->e_size);
  if(set->array == NULL){
    fprintf(stderr, "-- Out of memory --\n");
    abort();
  }
  flags = malloc((n+15)/16 * 4);
  memset(flags, 0x55, (n+15)/16 * 4);
  size = set->size;
  set->size = n;
  set->ocp  = set->count;
  set->max = n * set->load_factor;
  f = set->flags;
  set->flags = flags;
  flags = f;
  for(i=0;i<size;i++){
    if(!exists_entity(flags, i)) continue;
    key = ((Seq**)set->array)[i];
    set_entity_del(flags, i);
    while(1){
      hc = seq_hash_code(key, len) % set->size;
      while(!is_entity_null(set->flags, hc)){ hc = (hc + 1) % set->size; }
      clear_entity_null(set->flags, hc);
      if(hc < size && exists_entity(flags, hc)){
	tmp = key;
	key = ((Seq**)set->array)[hc];
	((Seq**)set->array)[hc] = tmp;
	set_entity_del(flags, hc);
      } else {
	((Seq**)set->array)[hc] = key;
	break;
      }
    }
  }
  free(flags);
}
