#include "samtools/sam.h"
#include "site.h"
#include "tgt_aln.h"

void insert_aln_hsite(SeqHash *sh, HomoSite *hst, bam_aln_t *ba, RKGen *rk, CallConf *conf) {

  AlignList *alist = init_AlignList(20);
  s_align *a, *ar;

  rk_rewind(rk);
  while (rk_generate(rk) > 0) {

    Suffix *sf = get_SeqHash(sh, rk->p, rk->d);
    if (sf==NULL) continue;

    size_t si;
    SeqList *sl = (SeqList*)sf->data;
    for (si=0; si<sl->size; ++si) {
      Seq *s = ref_SeqList(sl, si);
      Kmer *k = s->k;
      if (k->site == hst) {

	/* if the k-mer is covered by a previous alignment
	 * then skip the k-mer */
	uint8_t p_aligned = 0; uint32_t i;
	for (i=0; i<alist->size; ++i) {
	  Align *al=ref_AlignList(alist,i);
	  if (al->begin<(unsigned) k->spos && 
	      al->end>(unsigned) k->spos + rk->klen) {
	    p_aligned = 1;
	    break;
	  }
	}

	if (p_aligned) continue;
	uint8_t cs1, cs2, m;
	if (s->str) {
	  if (seed_extend_neg(hst->seq, rk, k, &cs1, &cs2, &m, 0)) {
	    Align *al = next_ref_AlignList(alist);
	    al->str = 1;
	    al->score = rk->len * 2 - m * 4;
	    al->begin = k->spos-rk->len + rk->ks + rk->klen + cs1;
	    al->end = k->spos + rk->klen + rk->ks - 1 - cs2;
	    seedext_set_cigar(al, rk->len, cs1, cs2);
	    break;
	  }

	} else {

	  if (seed_extend_pos(hst->seq, rk, k, &cs1, &cs2, &m, 0)) {
	    Align *al = next_ref_AlignList(alist);
	    al->str = 0;
	    al->score = rk->len * 2 - m * 4;
	    al->begin = k->spos - rk->ks + cs1;
	    al->end = k->spos + rk->len - rk->ks - 1 - cs2;
	    seedext_set_cigar(al, rk->len, cs1, cs2);
	    break;
	  }
	}

	/** local Smith-Waterman **/
	rk_encode_nt256int8(rk);
	size_t seq_begin = k->spos - rk->len * 3/2;
	a=_ssw_align(rk->seq_sw, rk->len,
		     hst->swseq + seq_begin, 3 * rk->len);
	ar=_ssw_align(rk->seq_sw_r, rk->len,
		      hst->swseq + seq_begin, 3 * rk->len);

	if (a->score1 > ar->score1) {
	  if (check_aln(a, hst->seq + seq_begin, rk, 0, conf)) {
	    Align *al = next_ref_AlignList(alist);
	    al->str = 0;
	    al->begin = seq_begin + a->ref_begin1;
	    al->end = seq_begin + a->ref_end1;
	    swaln_cpy_cigar2(al, a, rk->len);
	  }

	} else {
	  if (check_aln(ar, hst->seq + seq_begin, rk, 1, conf)) {
	    Align *al = next_ref_AlignList(alist);
	    al->str = 1;
	    al->begin = seq_begin + ar->ref_begin1;
	    al->end = seq_begin + ar->ref_end1;
	    swaln_cpy_cigar2(al, ar, rk->len);
	  }
	}

	free_align(a);
	free_align(ar);
      }
    }
  }

  if (alist->size) {
    copy_best_aln(ba, alist, hst->pos-hst->rpos);
  }

  free_AlignList(alist);
	  
}


void global_aln_hsite (HomoSite *hst, bam_aln_t *ba, RKGen *rk, uint8_t mate_str, CallConf *conf) {

  rk_encode_nt256int8(rk);
  s_align *a;
  if (mate_str) {
    a = _ssw_align(rk->seq_sw, rk->len, hst->swseq, 2001);
    if (check_aln(a, hst->seq, rk, 0, conf)) {
      ba->str = 0;
      ba->pos = hst->pos - hst->rpos + a->ref_begin1;
      swaln_cpy_cigar(ba, a, rk->len);
    }
  } else {
    a = _ssw_align(rk->seq_sw_r, rk->len, hst->swseq, 2001);
    if (check_aln(a, hst->seq, rk, 1, conf)) {
      ba->str = 1;
      ba->pos = hst->pos - hst->rpos + a->ref_begin1;
      swaln_cpy_cigar(ba, a, rk->len);
    }
  }
  free_align(a);
    /* NO GLOBAL SW for homologous sites */

  /* 	  else {		/\* otherwise perform a global SW in case all the local mappings fail *\/ */
  /* 	    if (swseq1) { */
  /* 	      _nt256char_encode_nt256int8(seq1_sw, seq1->seq.s, rlen1); */
  /* 	      _nt256int8_rev(seq1_sw_r, seq1_sw, rlen1); */
  /* 	      swseq1 = 0; */
  /* 	    } */


  /* 	    s_align *a=_ssw_align(seq1_sw, rlen1, hs->swseq, 2001); */
  /* 	    s_align *ar=_ssw_align(seq1_sw_r, rlen1, hs->swseq, 2001); */

  /* #ifdef DEBUGTALN */
  /* 	    fprintf(stdout, "[%s:%d] global sw of read 1: %u\t%u\n", __func__, __LINE__, a->score1, ar->score1); */
  /* 	    fprintf(stdout, "[%s:%d] ref: %s\n", __func__, __LINE__, get_RPool(rp, st)); */
  /* 	    fprintf(stdout, "[%s:%d] read1: %s\n", __func__, __LINE__, seq1->seq.s); */
  /* #endif // DEBUGTALN */

  /* 	    if (a->score1 > ar->score1) { */
  /* 	      if (a->score1 + ALIT > rlen1 * 2) { */
  /* 		b->str1 = 0; */
  /* 		b->pos1 = hs->pos-hs->rpos+a->ref_begin1; */

  /* 		/\* post-process cigar string, add soft-clipping *\/ */
  /* 		size_t qlen = a->read_begin1; size_t k; */
  /* 		b->n_cigar1 = a->cigarLen; */
  /* 		for (k=0; k<b->n_cigar1; ++k) { */
  /* 		  if (bam_cigar_type(bam_cigar_op(a->cigar[k]))&1) { */
  /* 		    qlen += bam_cigar_oplen(a->cigar[k]); */
  /* 		  } */
  /* 		} */
  /* 		if (a->read_begin1) b->n_cigar1++; */
  /* 		if (qlen<rlen1) b->n_cigar1++; */
  /* 		b->cigar1 = (uint32_t*) malloc(b->n_cigar1*sizeof(uint32_t)); */
  /* 		if (a->read_begin1) { */
  /* 		  b->cigar1[0] = (uint32_t) (a->read_begin1<<4|BAM_CSOFT_CLIP); */
  /* 		  memcpy(b->cigar1+1, a->cigar, a->cigarLen*sizeof(uint32_t)); */
  /* 		} else memcpy(b->cigar1, a->cigar, a->cigarLen*sizeof(uint32_t)); */
  /* 		if (qlen<rlen1) { */
  /* 		  b->cigar1[b->n_cigar1-1] = (uint32_t) ((rlen1-qlen)<<4|BAM_CSOFT_CLIP); */
  /* 		} */
  /* 	      } */
  /* 	    } else if (ar->score1 + ALIT > rlen2 * 2) { */
  /* 	      b->str1 = 1; */
  /* 	      b->pos1 = hs->pos-hs->rpos+ar->ref_begin1; */

  /* 	      /\* post-process cigar string, add soft-clipping *\/ */
  /* 	      size_t qlen = ar->read_begin1; size_t k; */
  /* 	      b->n_cigar1 = ar->cigarLen; */
  /* 	      for (k=0; k<b->n_cigar1; ++k) { */
  /* 		if (bam_cigar_type(bam_cigar_op(ar->cigar[k]))&1) { */
  /* 		  qlen += bam_cigar_oplen(ar->cigar[k]); */
  /* 		} */
  /* 	      } */

  /* 	      if (ar->read_begin1) b->n_cigar1++; */
  /* 	      if (qlen<rlen1) b->n_cigar1++; */

  /* 	      b->cigar1 = (uint32_t*) malloc(b->n_cigar1*sizeof(uint32_t)); */
  /* 	      if (ar->read_begin1) { */
  /* 		b->cigar1[0] = (uint32_t) (ar->read_begin1<<4|BAM_CSOFT_CLIP); */
  /* 		memcpy(b->cigar1+1, ar->cigar, ar->cigarLen*sizeof(uint32_t)); */
  /* 	      } else memcpy(b->cigar1, ar->cigar, ar->cigarLen*sizeof(uint32_t)); */
  /* 	      if (qlen<rlen1) { */
  /* 		b->cigar1[b->n_cigar1-1] = (uint32_t) ((rlen1-qlen)<<4|BAM_CSOFT_CLIP); */
  /* 	      } */
  /* 	    } */
  /* 	    free_align(a); */
  /* 	    free_align(ar); */
  /* 	  } */


}
