#include "tgt_aln.h"
#include "samtools/sam.h"
#include "site.h"
#include "tally.h"

/*
   return 1 if alignment pass check 0 otherwise
   alignment passes check if the alignment score is higher
   than the effective alignment length times the a ratio
   cutoff.
 */
int check_aln(s_align *aln, char *refseq, RKGen *rk,
	      int str, CallConf *conf) {

  /* accept if overall alignment score is high */
  if (aln->score1 > rk->len * conf->aln_ratio) return 1;
  /* reject if overall alignment score too small */
  else if (aln->score1 < rk->len) return 0;

  /* re-score high quality bases */
  int highqual_score = 0;
  int highqual_cnt = 0;

  char *q = str ? rk_revseq(rk) : rk->seq;
  char *qual = str ? rk_revqual(rk) : rk->qual;

  char *r = refseq + aln->ref_begin1;
  q += aln->read_begin1;

  int i;
  for (i=0; i<aln->read_begin1; ++i, ++qual) {
    if (*qual - 33 < 10) highqual_cnt++;
  }

  for (i=0; i<aln->cigarLen; ++i) {
    uint32_t op = bam_cigar_op(aln->cigar[i]);
    uint32_t len = bam_cigar_oplen(aln->cigar[i]);
    switch (op) {
    case BAM_CMATCH:
      for (; len--; ++r, ++q, ++qual) {
	if (*qual - 33 > 10) {
	  if (*r == *q || *q == 'N' || *q == '.') {
	    highqual_score += 2;
	  } else {
	    highqual_score -= 2;
	  }
	  highqual_cnt++;
	}
      }
      break;
    case BAM_CINS:
      if (*qual - 33 > 10) {
	highqual_score -= S_GAPOPEN;
	highqual_cnt++;
	len--;
      }
      for (; len--; ++q, ++qual) {
	if (*qual - 33 > 10) {
	  highqual_score -= S_GAPEXT;
	  highqual_cnt++;
	}
      }
      break;
    case BAM_CSOFT_CLIP:
      for (; len--; ++q, ++qual) {
	if (*qual - 33 > 10) {
	  highqual_score -= S_MISMATCH;
	  highqual_cnt++;
	}
      }
      break;
    case BAM_CDEL:
      if (*qual-33 > 10) {
	highqual_score -= S_GAPOPEN + S_GAPEXT * len;
	highqual_cnt++;
      }
      r += len;
      break;
    case BAM_CREF_SKIP:
      r += len;
      break;
    default:
      fprintf(stderr, "[%s:%d] Unknown cigar, %u\n",
	      __func__, __LINE__, op);
      exit(1);
      break;
    }
  }

  /* accept if alignment score is high
     compared against effective length */
  if (highqual_score > highqual_cnt * conf->aln_ratio) return 1;
  else return 0;
}

static uint32_t swaln_calend(s_align *a) {
  uint32_t qlen = a->read_begin1;
  int k;
  for (k=0; k<a->cigarLen; ++k) {
    if (bam_cigar_type(bam_cigar_op(a->cigar[k]))&1) {
      qlen += bam_cigar_oplen(a->cigar[k]);
    }
  }
  return qlen;
}

void swaln_cpy_cigar(bam_aln_t *ba, s_align *a, uint32_t rlen) {

  /* determine cigar length */
  uint32_t qlen = swaln_calend(a);
  ba->n_cigar = a->cigarLen;
  if (a->read_begin1) ba->n_cigar++;
  if (qlen<rlen) ba->n_cigar++;

  /* copy cigar, post-process cigar string, add soft-clipping */
  ba->cigar = (uint32_t*) malloc(ba->n_cigar*sizeof(uint32_t));
  if (a->read_begin1) {
    ba->cigar[0] = (uint32_t) (a->read_begin1<<4|BAM_CSOFT_CLIP);
    memcpy(ba->cigar+1, a->cigar, a->cigarLen*sizeof(uint32_t));
  } else memcpy(ba->cigar, a->cigar, a->cigarLen*sizeof(uint32_t));
  if (qlen<rlen) {
    ba->cigar[ba->n_cigar-1] = (uint32_t) ((rlen-qlen)<<4|BAM_CSOFT_CLIP);
  }
}

void swaln_cpy_cigar2(Align *al, s_align *a, uint32_t rlen) {

  /* determine cigar length */
  uint32_t qlen = swaln_calend(a);
  al->n_cigar = a->cigarLen;
  if (a->read_begin1) al->n_cigar++;
  if (qlen<rlen) al->n_cigar++;

  /* copy cigar, post-process cigar string, add soft-clipping */
  if (a->read_begin1) {
    al->cigar[0] = (uint32_t) (a->read_begin1<<4|BAM_CSOFT_CLIP);
    memcpy(al->cigar+1, a->cigar, a->cigarLen*sizeof(uint32_t));
  } else memcpy(al->cigar, a->cigar, a->cigarLen*sizeof(uint32_t));
  if (qlen<rlen) {
    al->cigar[al->n_cigar-1] = (uint32_t) ((rlen-qlen)<<4|BAM_CSOFT_CLIP);
  }
}

/* seed extension
 * negative strand,
 *
 * read is from right to left
 *
 * left side
 * reference: rrrrrrrrrrrrrrrrsrrrrrrrrr
 *                 |rpos+spos (w/r reference)
 * read         xxxkkkkxxx
 * k-mer           kkkk
 *                    |ks (w/r read)
 * 
 * right side
 * reference: rrsrrrrrrrrrrrrrrrrr
 *                 |rpos+spos (w/r reference)
 * read         xxxkkkkxxx
 * k-mer           kkkk
 *                    |ks (w/r read)
 */
int
seed_extend_neg(char *rs, RKGen *rk, Kmer *k,
		uint8_t *cs1, uint8_t *cs2, uint8_t *m, char *t) {

  size_t len; char *r, *q;
  *m = 0;
  len = rk->ks;
  r = rs + k->spos + rk->klen;
  q = rk->seq + rk->ks - 1;
  for (; len--; ++r, --q) {
    if ((*r)!=nt256char_rev_table[(unsigned char)(*q)] && (*q)!='N') {
      ++*m;
      if (*m>SEED_EXT_MAX_MISMATCH ||
	  (t && t == r)) return 0;
    }
  }
  *cs2 = 0;
  while ((*--r)!=nt256char_rev_table[(unsigned char)(*++q)] && (*q)!='N') ++*cs2;

  len = rk->len - rk->ks - rk->klen;
  r = rs + k->spos - 1;
  q = rk->seq + rk->ks + rk->klen;
  for (; len--; --r, ++q) {
    if ((*r)!=nt256char_rev_table[(unsigned char)(*q)] && (*q)!='N') {
      ++*m;
      if (*m>SEED_EXT_MAX_MISMATCH ||
	  (t && t == r)) return 0;
    }
  }
  *cs1 = 0;
  while ((*++r)!=nt256char_rev_table[(unsigned char)(*--q)] && (*q)!='N') ++*cs1;

  return 1;
}


/* seed extension
 * positive strand,
 * read is from left to right
 *
 * left side
 * reference: rrrrrrrrrrrrrrrrsrrrrrrrrr
 *                 |rpos+spos (w/r reference)
 * read         xxxkkkkxxx
 * k-mer           kkkk
 *                 |ks (w/r read)
 *
 * right side
 * reference: rrsrrrrrrrrrrrrrrr
 *                 |rpos+spos (w/r reference)
 * read         xxxkkkkxxx
 * k-mer           kkkk
 *                 |ks (w/r read)
 * 
 */
int
seed_extend_pos(char *rs, RKGen *rk, Kmer *k, 
		uint8_t *cs1, uint8_t *cs2, uint8_t *m, char *t) {


  size_t len; char *r, *q;
  *m = 0;
  len = rk->len - rk->ks - rk->klen;
  r = rs + k->spos + rk->klen;
  q = rk->seq + rk->ks + rk->klen;
  for (; len--; ++r, ++q) {
    if ((*r)!=(*q) && (*q)!='N') {
      ++*m;
      if (*m>SEED_EXT_MAX_MISMATCH ||
	  (t && t == r)) return 0;
    }
  }
  *cs2 = 0;
  while ((*--r)!=(*--q) && (*q)!='N') ++*cs2;

  len = rk->ks;
  r = rs + k->spos - 1;
  q = rk->seq + rk->ks - 1;
  for (; len--; --r, --q) {
    if ((*r)!=(*q) && (*q)!='N') {
      ++*m;
      if (*m>SEED_EXT_MAX_MISMATCH ||
	  (t && t == r)) return 0;
    }
  }
  *cs1 = 0;
  while ((*++r)!=(*++q) && (*q)!='N') ++*cs1;

  return 1;
}


void
seedext_set_cigar(Align *al, uint32_t rlen, uint8_t cs1, uint8_t cs2) {

  if (cs1) {
    if (cs2) {
      al->n_cigar = 3;
      al->cigar[0] = (uint32_t) (cs1<<4|BAM_CSOFT_CLIP);
      al->cigar[1] = (uint32_t) ((rlen-cs1-cs2)<<4|BAM_CMATCH);
      al->cigar[2] = (uint32_t) (cs2<<4|BAM_CSOFT_CLIP);
    } else {
      al->n_cigar = 2;
      al->cigar[0] = (uint32_t) (cs1<<4|BAM_CSOFT_CLIP);
      al->cigar[1] = (uint32_t) ((rlen-cs1)<<4|BAM_CMATCH);
    }
  } else if (cs2) {
    al->n_cigar = 2;
    al->cigar[0] = (uint32_t) ((rlen-cs2)<<4|BAM_CMATCH);
    al->cigar[1] = (uint32_t) (cs2<<4|BAM_CSOFT_CLIP);
  } else {
    al->n_cigar = 1;
    al->cigar[0] = (uint32_t) (rlen<<4|BAM_CMATCH);
  }

}


