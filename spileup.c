#include "spileup.h"

typedef struct {

  PlpAlleleData ad_t;
  PlpAlleleData ad_n;
  PlpBCFData bcf;
  PlpBamData bam_t;
  PlpBamData bam_n;
  PlpReadData rd_t;
  PlpReadData rd_n;
  PlpRefData refseq;

} SPlpData;


static char info_str[] = \
  "##INFO=<ID=ANT,Number=1,Type=Integer,Description=\"total allele count (in inserts) in the tumor sample\">\n"
  "##INFO=<ID=ACT,Number=A,Type=Integer,Description=\"alternative allele count (in inserts) in the tumor sample\">\n"
  "##INFO=<ID=AFT,Number=A,Type=Float,Description=\"allele frequency in the tumor sample\">\n"
  "##INFO=<ID=DPT,Number=1,Type=Integer,Description=\"raw read depth (without quality filtering) in the tumor sample\">\n"
  "##INFO=<ID=ACN,Number=A,Type=Integer,Description=\"alternative allele count (in inserts) in the normal sample\">\n"
  "##INFO=<ID=AFN,Number=A,Type=Float,Description=\"allele frequency in the normal sample\">\n"
  "##INFO=<ID=DPN,Number=1,Type=Integer,Description=\"raw read depth (without quality filtering) in the normal sample\">\n"
  "##INFO=<ID=SVTYPE,Number=1,Type=String,Description=\"Insertion or Deletion\">\n";

static char fmt_str[] = "";

SPlpData *init_SPlpData(char *bam_fn_t, char *bam_fn_n, char *bcf_fn, char *rfn) {
  SPlpData *pd = malloc(sizeof(SPlpData));
  prepare_pileup_bam(&pd->bam_t, bam_fn_t);
  prepare_pileup_bam(&pd->bam_n, bam_fn_n);
  prepare_pileup_allele(&pd->ad_t);
  prepare_pileup_allele(&pd->ad_n);
  prepare_pileup_read(&pd->rd_t);
  prepare_pileup_read(&pd->rd_n);
  prepare_pileup_bcf(bcf_fn, rfn, &pd->bam_t, &pd->bcf, 
		     info_str, fmt_str);
  pd->refseq.seq = NULL;
  return pd;
}

void free_SPlpData(SPlpData *pd) {
  free(pd->refseq.seq);
  clean_pileup_read(&pd->rd_t);
  clean_pileup_read(&pd->rd_n);
  clean_pileup_bam(&pd->bam_t);
  clean_pileup_bam(&pd->bam_n);
  clean_pileup_bcf(&pd->bcf);
  clean_pileup_allele(&pd->ad_t);
  clean_pileup_allele(&pd->ad_n);
  free(pd);
}

Allele *normal_find_alt(AllelePList *al, Allele *at) {
  uint32_t i;
  for (i=0; i<al->size; ++i) {
    Allele *an = get_AllelePList(al, i);
    if (an->rpos == at->rpos &&
	an->len == at->len &&
	memcmp(an->seq, at->seq, an->len) == 0) {
      return an;
    }
  }

  return NULL;
}


static void
somatic_write_bcf(PlpBCFData *bcf, BCFSomaticRec *rec,
		  uint32_t dp_t, uint32_t dp_n,
		  uint32_t an_t, uint32_t an_n) {

  /* order alternative alleles by insert counts */
  qsort(rec->alts->buffer, rec->alts->size,
	sizeof(Allele*), cmp_allelep_by_ic);

  bcf1_t *b = calloc(1, sizeof(bcf1_t));
  b->n_smpl = bcf->bh->n_smpl;

  Allele *a = get_AllelePList(rec->alts, 0);
  b->tid = a->tid;
  b->pos = a->rpos-1;		/* BCF position is zero-based */
  b->qual = max_qual(rec->quals);

  kstring_t s;
  s.s = 0; s.m = s.l = 0;
  /* b->str; s.m = b->m_str; s.l = 0; */

  /* ID */
  kputc('\0', &s);

  uint32_t i;
  if (a->cigar == BAM_CDIFF) {	/* substitution */

    /* REF */
    Allele *_a = get_AllelePList(rec->alts, 0);
    kputsn(rec->ref_t->seq-rec->ref_t->rpos+_a->rpos, _a->len, &s);
    kputc('\0', &s);

    /* ALT */
    for (i=0; i<rec->alts->size; ++i) {
      _a = get_AllelePList(rec->alts, i);
      if (i) kputc(',', &s);
      kputsn(_a->seq, _a->len, &s);
    }
    kputc('\0', &s);

  } else if (a->cigar == BAM_CDEL) { /* deletion */

    /* REF */
    kputsn(a->seq, a->len, &s);
    kputc('\0', &s);

    /* ALT */
    kputc(a->seq[0], &s);
    kputc('\0', &s);

  } else if (a->cigar == BAM_CINS) { /* insertion */

    /* REF */
    kputc(rec->ref_t->seq[0], &s);
    kputc('\0', &s);

    /* ALT */
    kputc(rec->ref_t->seq[0], &s);
    kputsn(a->seq, a->len, &s);
    kputc('\0', &s);

  } else {                      /* shouldn't see BAM_CREF_SKIP here for RNA-seq data */
    fprintf(stderr, "[%s] Unknown cigar\n", __func__);
    exit(1);
  }

  /* FILTER */
  if (b->qual>=1) kputs("PASS", &s);
  else kputs("LOWQUAL", &s);
  kputc('\0', &s);

  /* INFO */
  ksprintf(&s, "ANN=%d;DPN=%d", an_n, dp_n);
  kstring_t acn, afn;
  acn.m = acn.l = 0; acn.s = 0;
  afn.m = afn.l = 0; afn.s = 0;
  for (i=0; i<rec->alts->size; ++i) {
    if (i) {
      kputc(',', &acn);
      kputc(',', &afn);
    }
    uint32_t altcnt_n = get_IntList(rec->altcnts_n, i);
    ksprintf(&acn, "%d", altcnt_n);
    ksprintf(&afn, "%.4g", (float)altcnt_n/an_n);
  }
  ksprintf(&s, ";ACN=%s", acn.s);
  ksprintf(&s, ";AFN=%s", afn.s);
  free(acn.s); free(afn.s);

  ksprintf(&s, ";ANT=%d;DPT=%d", an_t, dp_t);
  kstring_t act, aft;
  act.m = act.l = 0; act.s = 0;
  aft.m = aft.l = 0; aft.s = 0;
  for (i=0; i<rec->alts->size; ++i) {
    if (i) {
      kputc(',', &act);
      kputc(',', &aft);
    }
    uint32_t altcnt_t = get_IntList(rec->altcnts_t, i);
    ksprintf(&act, "%d", altcnt_t);
    ksprintf(&aft, "%.4g", (float)altcnt_t/an_t);
  }
  ksprintf(&s, ";ACT=%s", act.s);
  ksprintf(&s, ";AFT=%s", aft.s);
  free(act.s); free(aft.s);

  if (a->cigar == BAM_CDEL) kputs(";SVTYPE=DEL", &s);
  if (a->cigar == BAM_CINS) kputs(";SVTYPE=INS", &s);

  /* I16, QS, VDB, RPB ... */
  kputc('\0', &s);

  /* FMT */
  /* PL, DP, DV, SP etc. */
  kputc('\0', &s);

  b->m_str = s.m; b->str = s.s;
  b->l_str = s.l;
  bcf_sync(b);
  bcf_write(bcf->bp, bcf->bh, b);
  bcf_destroy(b);

}

/* ac2bcf does not handle cases where indels and snps are mixed */
static void ac2bcf(PlpAlleleData *ad_t, PlpAlleleData *ad_n,
		   PlpBCFData *bcf, CallConf *conf) {

  uint32_t i;

  /* compute coverage information */
  /* uint32_t dp_t, an_t, dp_n, an_n; */
  /* get_coverage(ad_t, &dp_t, &an_t); */
  /* get_coverage(ad_n, &dp_n, &an_n); */

  /* classify records by mutation type and position */
  AllelePList *al = ad_t->al;
  AllelePList *al_rec = init_AllelePList(2); /* allele to be proecessed in this record */
  BCFSomaticRec *rec = init_BCFSomaticRec();
  while (check_remain(al)) {

    clear_AllelePList(al_rec);
    for (i=0; i<al->size; ++i) {
      Allele *a = get_AllelePList(al, i);
      if (!a->recorded) {
	if (al_rec->size>0) {
	  if (same_category(get_AllelePList(al_rec, 0), a)) {
	    push_AllelePList(al_rec, a);
	    a->recorded = 1;
	  }
	} else {
	  push_AllelePList(al_rec, a);
	  a->recorded = 1;
	}
      }
    }

    clear_BCFSomaticRec(rec);
    rec->ref_t = &ad_t->ref;
    rec->ref_n = &ad_n->ref;

    /* check quality */
    for (i=0; i<al_rec->size; ++i) {
      Allele *at = get_AllelePList(al_rec, i);
      Allele *an = normal_find_alt(ad_n->al, at);
      uint32_t altcnt_n = an ? an->ic : 0;
      uint32_t altcnt_t = at->ic;

      if (altcnt_t > 3) {
	double pval = somatic_posterior(ad_t->an - altcnt_t, altcnt_t,
					ad_n->an - altcnt_n, altcnt_n,
					conf->error, conf->mu,
					conf->mu_somatic, conf->contam);
	double qual = pval2qual(pval);
	/* float qual = pval2qual(somatic_posterior(rec->ref_t->rc, altcnt_t, rec->ref_n->rc, altcnt_n, conf->error, conf->mu, conf->mu_somatic, conf->contam)); */
	push_FloatList(rec->quals, qual);
	push_IntList(rec->altcnts_t, altcnt_t);
	push_IntList(rec->altcnts_n, altcnt_n);
	push_AllelePList(rec->alts, at);
      }
    }
    if (rec->alts->size > 0) {
      somatic_write_bcf(bcf, rec, ad_t->dp, ad_n->dp, ad_t->an, ad_n->an);
    }
  }
  free_AllelePList(al_rec);
  free_BCFSomaticRec(rec);
}

void spileup(char *bcf_fn, char *bam_fn_t, char *bam_fn_n,
	     KCnts *kc, uint8_t *siv) {


  SPlpData *pd = init_SPlpData(bam_fn_t, bam_fn_n,
			       bcf_fn, kc->rs->rfn);
  uint32_t i, c=0;
  for (i=0; i<kc->sl->size; ++i) {
    if (siv[i]) {

      fprintf(stderr, "\r[%s] pile up %u mutations", __func__, c);
      ++c;

      Site *st = get_SiteList(kc->sl, i);
      bind_site_reference(&pd->refseq, st, kc->rs);
      init_pileup_allele(&pd->ad_t, &pd->refseq, st);
      init_pileup_allele(&pd->ad_n, &pd->refseq, st);

      InsertList *insl_t=site_fetch_inserts(st, &pd->bam_t, &pd->rd_t, kc->conf->min_mapQ);
      plp_inserts(&pd->ad_t, insl_t, &pd->refseq, st, kc->conf->min_baseQ);

      InsertList *insl_n=site_fetch_inserts(st, &pd->bam_n, &pd->rd_n, kc->conf->min_mapQ);
      plp_inserts(&pd->ad_n, insl_n, &pd->refseq, st, kc->conf->min_baseQ);

      ac2bcf(&pd->ad_t, &pd->ad_n, &pd->bcf, kc->conf);

      destroy_InsertList(insl_t, &pd->rd_t);
      destroy_InsertList(insl_n, &pd->rd_n);
    }
  }
  fprintf(stderr, "\r[%s] pile up %u mutations\n", __func__, c);
  free_SPlpData(pd);

  return;
}

static int usage() {
  fprintf(stderr, "\n");
  fprintf(stderr, "Call somatic mutations from tumor-normal pairs.\n");
  fprintf(stderr, "Usage: clinsek spileup [options] -s sitelist -r ref.fa -t tumor.bam -n normal.bam -o out.bcf\n");
  fprintf(stderr, "Input options:\n");
  fprintf(stderr, "     -s FILE   site list [required]\n");
  fprintf(stderr, "     -r FILE   reference in fasta format [required]\n");
  fprintf(stderr, "     -t FILE   indexed tumor bam file [required]\n");
  fprintf(stderr, "     -n FILE   indexed normal bam file [required]\n");
  fprintf(stderr, "     -o STR    file name for bcf output [tmp.bcf]\n");
  fprintf(stderr, "     -m FLOAT  estimate of mutation rate [1e-3]\n");
  fprintf(stderr, "     -c FLOAT  estimate of contamination rate [1e-3]\n");
  fprintf(stderr, "     -u FLOAT  estimate of somatic mutation rate [1e-3]\n");
  fprintf(stderr, "     -e FLOAT  estimate of sequencing error rate [1e-3]\n");
  fprintf(stderr, "     -q INT    minimum mapping quality in variant calling [10]\n");
  fprintf(stderr, "     -b INT    minimum base quality in SNP calling [10]\n");
  fprintf(stderr, "     -d INT    base quality threshold in preliminary call [20]\n");
  fprintf(stderr, "     -l        site filter\n");
  fprintf(stderr, "     -h        this help\n");
  fprintf(stderr, "\n");
  return 1;
}

int main_spileup(int argc, char *argv[]) {

  CallConf conf;
  conf_init_default(&conf);

  int c; char *sfn=0, *rfn=0;
  char *bam_fn_t=0, *bam_fn_n=0, *bcf_fn=0;
  char *filter_fn=0;
  while ((c=getopt(argc, argv, "s:r:t:n:o:l:c:e:m:u:q:b:d:h"))>=0) {
    switch (c) {
    case 's': sfn	       = optarg; break;
    case 'r': rfn	       = optarg; break;
    case 't': bam_fn_t	       = optarg; break;
    case 'n': bam_fn_n	       = optarg; break;
    case 'o': bcf_fn	       = optarg; break;
    case 'l': filter_fn        = optarg; break;
    case 'c': conf.contam      = atof(optarg); break;
    case 'e': conf.error       = atof(optarg); break;
    case 'm': conf.mu	       = atof(optarg); break;
    case 'u': conf.mu_somatic  = atof(optarg); break;
    case 'q': conf.min_mapQ    = atoi(optarg); break;
    case 'b': conf.min_baseQ   = atoi(optarg); break;
    case 'd': conf.tbaseQ      = atoi(optarg); break;
    case 'h': return usage();
    default:
      fprintf(stderr, "[%s] Unrecognized command: %c.\n", __func__, c);
      exit(1);
    }
  }


  if (!(sfn && rfn && bam_fn_t && bam_fn_n)) {
    fprintf(stderr, "\n[%s:%d] Error: missing required argument.\n", __func__, __LINE__);
    return usage();
  }

  if (!bcf_fn) bcf_fn = "tmp.bcf";

  KCnts *kc = load_sites(sfn, rfn);
  kc->conf = &conf;

  SiteFilter *fter = load_filter(filter_fn, kc->sl->size);
  spileup(bcf_fn, bam_fn_t, bam_fn_n, kc, fter->buffer);

  free_KCnts(kc);
  free_SiteFilter(fter);

  return 0;

}
