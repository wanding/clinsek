/*
 * This utility requires bam file to be sorted before and after.
 *
 * All inserts with the same cigar string and start positions of
 * both mate reads form an insert group.
 *
 * For all the insert groups if one insert agrees with the other insert
 * on all of its high-quality bases, the first read is marked as a
 * duplicate. If this relationship exists mutually (e.g., when two
 * inserts match perfectly in their high quality bases), then the
 * read with less sum of base qualities is marked as a duplicate.
 *
 * This defines a partial order among inserts (a tree). Parent
 * node points to child node if
 * 1. all the high quality bases set is a proper subset of all high quality
 * bases of the child node, and the two inserts agree on the parent's set.
 * Or, 2. the two nodes have the same set of high-quality bases and
 * the inserts agree on this set, but the parent node is greater in the
 * sum of base qualities than the child node.
 * 
 * run the script on a bam with duplicate marking will re-mark the duplicates
 * 
 * The script identifies all the leaves of the tree as defined by
 * the above partial order.
 * */

#include <stdlib.h>
#include "samtools/sam.h"
#include "samtools/khash.h"
#include "samtools/klist.h"
#include "wvec.h"
#include "wstr.h"

void bam_sort_core_ext(int is_by_qname, const char *fn, const char *prefix, size_t _max_mem, int is_stdout, int n_threads, int level, int full_path);

typedef struct {
  uint32_t min_baseQ;		/* threshold for high quality base */
  uint8_t rmdup;
  uint8_t sort;
  uint32_t dup_cnt_pe;
  uint32_t cnt_pe;
  uint32_t dup_cnt_se;
  uint32_t cnt_se;
  int verbose;
  int mate_unmapped_as_se;
} MkDupConfig;


typedef struct {
  bam1_t *b1;
  bam1_t *b2;
  uint32_t sum_qual;
  uint8_t is_dup:1;
  uint8_t is_PE:1; /* differentiate single-end from paired-end with the other end unmapped */
} __attribute__((__packed__)) Insert;

DEFINE_VECTOR(InsertGroup, Insert*)

/* both mates must be equal in coordinates */
int insert_hash_equal(Insert *ins1, Insert *ins2) {

  if (ins1->is_PE != ins2->is_PE) return 0;
  
  /* compare mate 1 */
  bam1_t *b11 = ins1->b1;
  bam1_t *b21 = ins2->b1;
  if (b11) {
    if (b21) {
      bam1_core_t *c11 = &b11->core;
      bam1_core_t *c21 = &b21->core;
      if (c11->pos != c21->pos) return 0;
    } else return 0;
  } else if (b21) return 0;

  /* compare mate 2 */
  bam1_t *b12 = ins1->b2;
  bam1_t *b22 = ins2->b2;
  if (b12) {
    if (b22) {
      bam1_core_t *c12 = &b12->core;
      bam1_core_t *c22 = &b22->core;
      if (c12->pos != c22->pos) return 0;
    } else return 0;
  } else if (b22) return 0;

  return 1;
}

#define insert_hash_func(insert) (khint32_t) ((insert)->b1 ? (insert)->b1->core.pos : (insert)->b2->core.pos)

KHASH_INIT(IGMap, Insert*, InsertGroup*, 1, insert_hash_func, insert_hash_equal)

#define qname_hash_func(b) kh_str_hash_func(bam1_qname(b))

int read_match(bam1_t *b1, bam1_t *b2) {
  if ((strcmp(bam1_qname(b1), bam1_qname(b2)) == 0) &&
      (b1->core.mpos == b2->core.pos) &&
      (b1->core.pos == b2->core.mpos)) return 1;
  else return 0;
}

#define __mkdup_free_insert(key)	/* no-op */

KHASH_INIT(RIMap, bam1_t*, Insert*, 1, qname_hash_func, read_match)
/* KHASH_MAP_INIT_STR(NameInsMap, Insert*) */
#define bam_free1(b) free((b)->data)
KMEMPOOL_INIT(Read, bam1_t, bam_free1)
KMEMPOOL_INIT(Insert, Insert, __mkdup_free_insert)

static inline int
sum_qual(const bam1_t *b)
{
  if (!b) return 0;
  int i, q;
  uint8_t *qual = bam1_qual(b);
  for (i = q = 0; i < b->core.l_qseq; ++i) q += qual[i];
  return q;
}

int cmp_insert_qual(const void *p1, const void *p2) {
  return (*((Insert**)p2))->sum_qual - (*((Insert**)p1))->sum_qual;
}


static void flush_dangling_reads(khash_t(RIMap) *rim, kmempool_t(Read) *rmp, kmempool_t(Insert) *imp, samfile_t *out, MkDupConfig *conf) {
  khint_t k;
  for (k=kh_begin(rim); k<kh_end(rim); ++k) {
    if (kh_exist(rim, k)) {
      Insert *ins = kh_val(rim, k);
      if (ins->b1) {
        if (conf->verbose > 5) {
          fprintf(stderr, "[%s:%d] Warning: found dangling reads: %s\n", __func__, __LINE__, bam1_qname(ins->b1));
          fflush(stderr);
        }
        samwrite(out, ins->b1);
        kmp_free(Read, rmp, ins->b1);
      }
      if (ins->b2) {
        if (conf->verbose > 5) {
          fprintf(stderr, "[%s:%d] Warning: found dangling reads: %s\n", __func__, __LINE__, bam1_qname(ins->b2));
          fflush(stderr);
        }
        samwrite(out, ins->b2);
        kmp_free(Read, rmp, ins->b2);
      }
      kmp_free(Insert, imp, ins);
      kh_del(RIMap, rim, k);
    }
  }
}

int insert_seq_equal(Insert *ins1, Insert *ins2) {

  bam1_t *b11 = ins1->b1;
  bam1_t *b21 = ins2->b1;
  if (b11) {
    if (b21) {
      bam1_core_t *c11 = &b11->core;
      bam1_core_t *c21 = &b21->core;
      if (c11->l_qseq != c21->l_qseq) return 0;
      if (c11->l_qseq && memcmp(bam1_seq(b11), bam1_seq(b21), (c11->l_qseq+1)>>1) != 0) return 0;
    } else return 0;
  } else if (b21) return 0;

  bam1_t *b12 = ins1->b2;
  bam1_t *b22 = ins2->b2;
  if (b12) {
    if (b22) {
      bam1_core_t *c12 = &b12->core;
      bam1_core_t *c22 = &b22->core;
      if (c12->l_qseq != c22->l_qseq) return 0;
      if (c12->l_qseq && memcmp(bam1_seq(b12), bam1_seq(b22), (c12->l_qseq+1)>>1) != 0) return 0;
    } else return 0;
  } else if (b22) return 0;

  return 1;
}

/* maximize the quality string of the
 * first insert by the quality string of the
 * second insert */
void maximize_qual1(Insert *ins1, Insert *ins2) {
  uint8_t *q1;
  uint8_t *q2;
  int x;
  if (ins1->b1) {
    q1 = bam1_qual(ins1->b1);
    q2 = bam1_qual(ins2->b1);
    for (x = 0; x<ins1->b1->core.l_qseq; ++x) if (q1[x] < q2[x]) q1[x] = q2[x];
  }
  if (ins1->b2) {
    q1 = bam1_qual(ins1->b2);
    q2 = bam1_qual(ins2->b2);
    for (x = 0; x<ins1->b2->core.l_qseq; ++x) if (q1[x] < q2[x]) q1[x] = q2[x];
  }
}

/* if all the high quality base of test
   agree with good, return 1
   else return 0 */
int
highqual_equal(Insert *good, Insert *test, uint32_t min_baseQ) {

  int32_t i;

  /* compare mate 1 */
  bam1_t *b11 = good->b1;
  bam1_t *b21 = test->b1;
  if (b11) {
    if (b21) {
      uint8_t *s11 = bam1_seq(b11);
      uint8_t *s21 = bam1_seq(b21);
      uint8_t *q21 = bam1_qual(b21);
      bam1_core_t *c11 = &b11->core;
      bam1_core_t *c21 = &b21->core;
      if (c11->l_qseq != c21->l_qseq) return 0;
      for (i=0; i<c21->l_qseq; ++i) {
        if (bam1_seqi(s11, i) != bam1_seqi(s21, i) &&
            q21[i] > min_baseQ)
          return 0;
      }
    } else return 0;
  } else if (b21) return 0;

  /* compare mate 2 */
  bam1_t *b12 = good->b2;
  bam1_t *b22 = test->b2;
  if (b12) {
    if (b22) {
      uint8_t *s12 = bam1_seq(b12);
      uint8_t *s22 = bam1_seq(b22);
      uint8_t *q22 = bam1_qual(b22);
      bam1_core_t *c12 = &b12->core;
      bam1_core_t *c22 = &b22->core;
      if (c12->l_qseq != c22->l_qseq) return 0;
      for (i=0; i<c22->l_qseq; ++i) {
        if (bam1_seqi(s12, i) != bam1_seqi(s22, i) &&
            q22[i] > min_baseQ)
          return 0;
      }
    } else return 0;
  } else if (b22) return 0;

  return 1;
  
}

void resolve_dup(khash_t(IGMap) *igm, samfile_t *out,
                 kmempool_t(Read) *rmp,
                 kmempool_t(Insert) *imp,
                 MkDupConfig *conf) {

  uint32_t min_baseQ = conf->min_baseQ;
  uint8_t rmdup = conf->rmdup;
  khint_t k; uint8_t is_pe;
  for (k=kh_begin(igm); k<kh_end(igm); ++k) {
    if (kh_exist(igm, k)) {
      InsertGroup *ig = kh_val(igm, k);

      if (ig->size && ig->buffer[0]->b1 && ig->buffer[0]->b2) is_pe = 1;
      else is_pe = 0;

      unsigned i, j;
      for (i=0; i<ig->size; ++i) {
        Insert *ins = get_InsertGroup(ig, i);
        ins->sum_qual = sum_qual(ins->b1) + sum_qual(ins->b2);
      }

      /* sort by base quality, best quality first, */
      qsort(ig->buffer, ig->size, sizeof(Insert*), cmp_insert_qual);

      get_InsertGroup(ig, 0)->is_dup = 0;
      for (i=1; i<ig->size; ++i) {
        Insert *test = get_InsertGroup(ig, i);
        int alldiff = 1;
        for (j=0; j<i; ++j) {
          Insert *good = get_InsertGroup(ig, j);
          /* the candidate insert is an diff if 
             at least one high quality base is different from
             the target insert, no matter what quality the
             target insert is
          */
          if ((!good->is_dup) && highqual_equal(good, test, min_baseQ)) {
            alldiff = 0;
            break;
          }
        }
        if (alldiff) test->is_dup = 0;
        else test->is_dup = 1;
      }

      /* dump the insert group */
#ifdef DEBUGMKDUP
      if (ig->size) printf("%s\n", ig->buffer[0]->b1?bam1_qname(ig->buffer[0]->b1):bam1_qname(ig->buffer[0]->b2));
      int dupcnt1 = 0;
#endif // DEBUGMKDUP

      for (i=0; i<ig->size; ++i) {
        Insert *ins = get_InsertGroup(ig, i);
        if (ins->is_dup) {
#ifdef DEBUGMKDUP
          dupcnt1++;
#endif // DEBUGMKDUP
          if (is_pe) conf->dup_cnt_pe += 2;
          else ++conf->dup_cnt_se;
          if (ins->b1) ins->b1->core.flag |= BAM_FDUP;
          if (ins->b2) ins->b2->core.flag |= BAM_FDUP;
        }
        if (!(ins->is_dup && rmdup)) {
          if (ins->b1) {
            samwrite(out, ins->b1);
            kmp_free(Read, rmp, ins->b1);
          }
          if (ins->b2) {
            samwrite(out, ins->b2);
            kmp_free(Read, rmp, ins->b2);
          }
        }
        kmp_free(Insert, imp, ins);
      }
#ifdef DEBUGMKDUP
      printf("[%s:%d] ig size: %zu %d removed\n", __func__, __LINE__, ig->size, dupcnt1);
#endif // DEBUGMKDUP
      free_InsertGroup(ig);
    }
  }
  kh_clear(IGMap, igm);
}


int mark_dup(char *bam_in_fn, char *bam_out_fn, MkDupConfig *conf) {

  char *bam_out0_fn;
  if (conf->sort) {
    bam_out0_fn = malloc(strlen(bam_out_fn)+5);
    sprintf(bam_out0_fn, "%s.tmp", bam_out_fn);
  } else {
    bam_out0_fn = bam_out_fn;
  }

  samfile_t *in = samopen(bam_in_fn, "rb", 0);
  samfile_t *out = samopen(bam_out0_fn, "wb", in->header);

  int last_tid = -1, last_pos = -1;

  khash_t(IGMap) *igm = kh_init(IGMap);
  khash_t(RIMap) *rim = kh_init(RIMap);

  kmempool_t(Read) *rmp = kmp_init(Read);
  kmempool_t(Insert) *imp = kmp_init(Insert);

  unsigned cnt = 0;
  bam1_t *b = kmp_alloc(Read, rmp);
  while (samread(in, b)>=0) {
    cnt++;
    if ((cnt & 0xFFFF)==0) {
      fprintf(stderr, "\r[%s] parsed %u reads.", __func__, cnt);
      fflush(stderr);
    }

    bam1_core_t *c = &b->core;
    c->flag &= ~BAM_FDUP;

    if (c->tid != last_tid || c->pos != last_pos) {
      resolve_dup(igm, out, rmp, imp, conf);
      if (c->tid != last_tid)
        flush_dangling_reads(rim, rmp, imp, out, conf);
      last_tid = c->tid; last_pos = c->pos;
    }

    /* skip unmapped and secondary */
    if (c->flag & BAM_FUNMAP || c->flag & BAM_FSECONDARY) {
      samwrite(out, b);
      continue;
    }

    int ret;
    if ((c->flag & BAM_FPAIRED) && (c->tid==c->mtid) && (abs(c->isize)<10000) && !((c->flag&BAM_FMUNMAP) && conf->mate_unmapped_as_se)) { /* PE */
      /* treating mate-unmapped PE reads as SE might not be always
         desired since one may use read position in pair to de-duplicate */
      ++conf->cnt_pe;
      khint_t e = kh_put(RIMap, rim, b, &ret);
      Insert *ins;
      if (ret) {			/* empty */
        ins = kmp_alloc(Insert, imp);
        memset(ins, 0, sizeof(Insert));
        kh_val(rim, e) = ins;
      } else {
        ins = kh_val(rim, e);
      }

      ins->is_PE = 1;
      if (c->flag & BAM_FREAD1) {
        ins->b1 = b;
        b = kmp_alloc(Read, rmp);
      } else if (c->flag & BAM_FREAD2) {
        ins->b2 = b;
        b = kmp_alloc(Read, rmp);
      } else {
        fprintf(stderr, "[%s:%d] PE read position undecided: %s, treat as read 1.\n", __func__, __LINE__, bam1_qname(b));
        ins->b1 = b;
        b = kmp_alloc(Read, rmp);
      }

      /* Once the insert is matched. Push into insert group.
       * Dangling reads are handled at the end. */
      if (ins->b1 && ins->b2) {
        InsertGroup *ig;
        khint_t k = kh_put(IGMap, igm, ins, &ret);

        if (ret) {		/* empty */
          ig = init_InsertGroup(2);
          kh_val(igm, k) = ig;
        } else {
          ig = kh_val(igm, k);
        }
        push_InsertGroup(ig, ins);

        /* remove matched reads from read-insert map */
        kh_del(RIMap, rim, e);
      }
      
    } else {			 /* SE */

      ++conf->cnt_se;
      Insert *ins = kmp_alloc(Insert, imp);
      memset(ins, 0, sizeof(Insert));
      ins->is_PE = 0;
      ins->b1 = b;
      b = kmp_alloc(Read, rmp);

      /* group of inserts with same coordinates */
      InsertGroup *ig;
      khint_t k = kh_put(IGMap, igm, ins, &ret);
      if (ret) {		/* empty */
        ig = init_InsertGroup(2);
        kh_val(igm, k) = ig;
      } else {
        ig = kh_val(igm, k);
      }
      push_InsertGroup(ig, ins);
    }
  }
  resolve_dup(igm, out, rmp, imp, conf);
  flush_dangling_reads(rim, rmp, imp, out, conf);

  fprintf(stderr, "\r[%s] parsed %u reads\n", __func__, cnt);
  fprintf(stderr, "[%s] marked %d duplicates from %d paired-end reads (%.3g%%)\n", __func__, conf->dup_cnt_pe, conf->cnt_pe, (double) (conf->dup_cnt_pe) / conf->cnt_pe * 100);
  fprintf(stderr, "[%s] marked %d duplicates from %d single-end reads (%.3g%%)\n", __func__, conf->dup_cnt_se, conf->cnt_se, (double) (conf->dup_cnt_se) / conf->cnt_se * 100);
  fflush(stderr);

  samclose(in);
  samclose(out);

  kmp_free(Read, rmp, b);
  kmp_destroy(Read, rmp);
  kmp_destroy(Insert, imp);
  kh_destroy(IGMap, igm);
  kh_destroy(RIMap, rim);

  if (conf->sort) {
    fprintf(stderr, "[%s] sorting after mkdup\n", __func__);
    bam_sort_core_ext(0, bam_out0_fn, bam_out_fn, 768<<20, 0, 0, -1, 1);
    bam_index_build(bam_out_fn);
    remove(bam_out0_fn);
    free(bam_out0_fn);
  }

  return 0;
}

/* without sorting */
int mark_dup_nosort(char *bam_in_fn, char *bam_out_fn) {
  MkDupConfig conf = {
    .min_baseQ = 30,
    .rmdup = 0,
    .sort = 0,
    .dup_cnt_se = 0,
    .dup_cnt_pe = 0,
    .cnt_se = 0,
    .cnt_pe = 0,
    .verbose = 0,
    .mate_unmapped_as_se = 0,
  };

  return mark_dup(bam_in_fn, bam_out_fn, &conf);
}

static int usage() {
  fprintf(stderr, "\n");
  fprintf(stderr, "Usage: clinsek mkdup [options] <in.bam> <out.bam>\n");
  fprintf(stderr, "       <in.bam> must be sorted and indexed.\n");
  fprintf(stderr, "       <out.bam> is default to <in.bam.mkdup> if not provided.\n");
  fprintf(stderr, "Input options:\n");
  fprintf(stderr, "     -b INT    minimum base quality [30].\n");
  fprintf(stderr, "     -r        toggle to remove marked duplicate.\n");
  fprintf(stderr, "     -s        toggle to turn OFF sorting and indexing after marking duplicates.\n");
  fprintf(stderr, "     -u        treat mate-unmapped paired-end reads as single-end reads\n");
  fprintf(stderr, "     -V INT    verbose level [0]\n");
  fprintf(stderr, "     -h        this help.\n");
  fprintf(stderr, "\n");
  return 1;
}

int main_mkdup(int argc, char *argv[]) {

  MkDupConfig conf = {
    .min_baseQ = 30,
    .rmdup = 0,
    .sort = 1,
    .dup_cnt_se = 0,
    .dup_cnt_pe = 0,
    .cnt_se = 0,
    .cnt_pe = 0,
    .verbose = 0,
    .mate_unmapped_as_se = 0,
  };

  int c;
  if (argc < 2) return usage();
  while ((c = getopt(argc, argv, "b:V:ursh")) >= 0) {
    switch (c) {
    case 'b': conf.min_baseQ = atoi(optarg); break;
    case 'V': conf.verbose = atoi(optarg); break;
    case 'u': conf.mate_unmapped_as_se = 1; break;
    case 'r': conf.rmdup = 1; break;
    case 's': conf.sort = 0; break;
    case 'h': return usage();
    default:
      fprintf(stderr, "[%s:%d] Unrecognized command: %c.\n", __func__, __LINE__, c);
      exit(1);
      break;
    }
  }

  if (optind >= argc) {
    fprintf(stderr, "[%s:%d] missing input.\n", __func__, __LINE__);
    exit(1);
  }

  char *in_fn=0, *out_fn=0;
  in_fn = argv[optind];
  if (optind+1 < argc) out_fn = strdup(argv[optind+1]);
  else out_fn = wasprintf("%s.mkdup", in_fn);

  mark_dup(in_fn, out_fn, &conf);

  free(out_fn);

  return 0;
}
