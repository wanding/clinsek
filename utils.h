#ifndef _WZ_UTILS_H_
#define _WZ_UTILS_H_
#include <math.h>
#include <stdlib.h>
#include "stats.h"

/* the _a and _b are to avoid double evaluation
   such as max(x++, y++) */
#define max(a,b)				\
  ({ __typeof__ (a) _a = (a);			\
    __typeof__ (b) _b = (b);			\
    _a > _b ? _a : _b; })

#define min(a,b)				\
  ({ __typeof__ (a) _a = (a);			\
    __typeof__ (b) _b = (b);			\
    _a > _b ? _b : _a; })


/* static inline double max( double a, double b ) { return a > b ? a : b; } */

/* sum of numbers inside log10 function,
 * i.e., log_sum(log(a) + log(b)) = log(a+b) */
static inline double log_sum(double log1, double log2){
  double I = max(log1, log2);
  return log10(pow(10, log1-I) + pow(10, log2-I)) + I;
}

/* substract two numbers inside log function, i.e.,
 * log_sum(log(a) - log(b)) = log(a-b) */
static inline double log_subtract(double log1, double log2){
  double I = max(log1, log2);
  return log10(pow(10, log1-I) - pow(10, log2-I)) + I;
}

DEFINE_VECTOR(FList, char*)
static inline void destroy_FList(FList *fl) {
  uint32_t i;
  for (i=0; i<fl->size; ++i) {
    free(get_FList(fl, i));
  }
  free_FList(fl);
}

DEFINE_VECTOR(FloatList, double);
DEFINE_VECTOR(IntList, uint32_t);

static inline FILE*
wz_fopen(const char *func, char *fn, const char *mode) {
  FILE *fp = fopen(fn, mode);
  if (!fp) {
    fprintf(stderr, "[%s] [Error] Cannot open file %s with mode %s.\n", func, fn, mode);
    fflush(stderr);
    exit(1);
  }
  return fp;
}

static inline void
wz_pfclose(FILE **fpp) {
  if (*fpp == NULL) return;
  else {
    fclose(*fpp);
    *fpp = NULL;
  }
}


#endif /* _WZ_UTILS_H_ */

