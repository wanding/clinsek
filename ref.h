#ifndef _WZ_REF_H_
#define _WZ_REF_H_

/**
 * Site Sequence Pool (SSPool)
 * pool of reference sequences of sites
 * if the sequence of a site is not in the pool
 * the sequence is retrieved from the disk
 * a randomly chosen pre-stored sequence is free-ed
 * to set aside space.
**/

#include "site.h"
#include "refseq.h"
#include <stdio.h>
#include <stdlib.h>


typedef struct {
  char *seq;
  uint8_t sw:1;
  int8_t *swseq;
  uint32_t len;
} __attribute__((__packed__)) SiteSeq;

typedef struct {

  uint32_t len;		/* sequence length for each site */
  SiteSeq *seqs;
  size_t n;	      /* total number of sites */
  size_t max;	      /* maximum number of sites with sequence stored */
  size_t m;   /* number of sites with sequence stored, must be smaller than max */
  RefSeq *rs;

} SSPool;

/* mem_cap is in bytes */
static inline SSPool*
init_SSPool(uint32_t len, size_t n, size_t mem_cap, RefSeq *rs) {
  SSPool *ssp = malloc(sizeof(SSPool));
  ssp->seqs = calloc(n, sizeof(SiteSeq));
  ssp->len = len;
  ssp->n = n;
  ssp->max = mem_cap / (2*len+5);
  ssp->m = 0;
  ssp->rs = rs;
  return ssp;
}

static inline void
clear_SiteSeq(SiteSeq *ss) {
  if (ss->seq) free(ss->seq);
  if (ss->swseq) free(ss->swseq);
  ss->sw = 0;
  ss->seq = 0;
  ss->swseq = 0;
}

static inline void
clear_SSPool(SSPool *ssp) {
  size_t i;
  for (i=0; i<ssp->n; ++i) {
    SiteSeq *ss = ssp->seqs + i;
    clear_SiteSeq(ss);
  }
  ssp->m = 0;
}

static inline void
setseq_SSPool(SSPool *ssp, SiteSeq *ss, Site *st) {
  if (!ss->seq) {
    ss->seq = fetch_RefSeq(ssp->rs, st->chrm,
			   st->pos - st->rpos, ssp->len);
    ss->len = ssp->len;
    ss->sw = 0;
    ssp->m++;
  }
}

/* if no filter just put 0 */
static inline void
fill_SSPool(SSPool *ssp, SiteList *sl, uint8_t *fter) {

  clear_SSPool(ssp);
  size_t i;
  for (i=0; ssp->m < ssp->max && i < sl->size; ++i) {
    if (!fter || fter[i]) {
      Site *st = get_SiteList(sl, i);
      SiteSeq *ss = ssp->seqs + st->id;
      setseq_SSPool(ssp, ss, st);
    }
  }

  if (ssp->m == ssp->max) fprintf(stderr, "[%s:%d] Warning: too many sites (%zu) to store reference in memory (max = %zu).\n", __func__, __LINE__, sl->size, ssp->max);

}

static inline void
destroy_SSPool(SSPool *ssp) {
  clear_SSPool(ssp);
  free(ssp->seqs);
  free(ssp);
}


static inline SiteSeq*
get_SSPool(SSPool *ssp, Site *st) {
  SiteSeq *ss = ssp->seqs + st->id;
  if (!ss->seq) {
    if (ssp->m >= ssp->max) {
      /* randomly free existing seq if capacity is reached */
      SiteSeq *tofree = ssp->seqs + (size_t) (ssp->n * (double)(rand()/((long)RAND_MAX+1)));
      for (; !(tofree->seq); ++tofree)
	if (tofree > ssp->seqs + ssp->n) tofree = ssp->seqs;
      clear_SiteSeq(tofree);
      ssp->m--;
    }
    setseq_SSPool(ssp, ss, st);
  }
  return ss;
}


static inline void
ss_encode_nt256int8(SiteSeq *ss) {
  if (!ss->sw) {
    ss->swseq = nt256char_encode_nt256int8((unsigned char*) ss->seq, ss->len);
    ss->sw = 1;
  }
}

static inline int8_t*
ss_swseq(SiteSeq *ss) {
  ss_encode_nt256int8(ss);
  return ss->swseq;
}


#endif /* _WZ_REF_H_ */
