#include "samtools/klist.h"
#include "samtools/kstring.h"
#include "samtools/faidx.h"
#include "kcnts.h"
#include "encode.h"
#include "stats.h"
#include "pileup.h"
#include "utils.h"

/* 2 steps:
 * 1: scan the site and find all the reads covering it
 * 2. scan 1k bp up and down-stream the site and match-up
 * the inserts (if there's a mate) */
InsertList*
site_fetch_inserts(Site *st, 
		   PlpBamData *bam,
		   PlpReadData *rd,
		   uint32_t min_mapQ) {

  uint8_t tid = st->chrm;
  uint32_t pos = st->pos;
  khash_t(name) *kh = rd->kh;
  kmempool_t(Insert) *imp = rd->imp;
  kmempool_t(Read) *rmp = rd->rmp;

  kh_clear(name, kh);
  Insert **vp, *v; bam_iter_t iter;
  bam1_t *b = kmp_alloc(Read, rmp);

  /* scan 1 */
  iter = bam_iter_query(bam->idx, tid, pos-5, pos+5);
  while (bam_iter_read(bam->fp, iter, b)>=0) {

    if (b->core.qual < min_mapQ ||
    	b->core.flag & BAM_FDUP) continue; /* filter low mapping quality and duplicate insert */

    int ret;
    khint_t k = kh_put(name, kh, bam1_qname(b), &ret);
    vp = &kh_val(kh, k);
    if (ret) {			/* element absent */
      *vp = kmp_alloc(Insert, imp);
      memset(*vp, 0, sizeof(Insert));
    }
    if (b->core.flag & BAM_FREAD1) {
      (*vp)->a1.r = b;
    } else if (b->core.flag & BAM_FREAD2){
      (*vp)->a2.r = b;
    } else {
      fprintf(stderr, "[%s] Error, mal-formed reads, neither 1st nor 2nd\n", __func__);
      fprintf(stderr, "[%s] Exit.\n", __func__);
      exit(1);
    }
    b = kmp_alloc(Read, rmp);
  }
  bam_iter_destroy(iter);

  /* scan 2 */
  iter = bam_iter_query(bam->idx, tid, pos-1000, pos+1000);
  while (bam_iter_read(bam->fp, iter, b)>=0) {
    khint_t k = kh_get(name, kh, bam1_qname(b));
    if (k != kh_end(kh)) {
      v = kh_val(kh, k);
      if (b->core.flag & BAM_FREAD1) {
  	if (!v->a1.r) {
  	  v->a1.r = b;
  	  b = kmp_alloc(Read, rmp);
  	}
      } else if (b->core.flag & BAM_FREAD2) {
  	if (!v->a2.r) {
  	  v->a2.r = b;
  	  b = kmp_alloc(Read, rmp);
  	}
      } else {
  	fprintf(stderr, "[%s] Error, mal-formed reads, neither 1st nor 2nd\n", __func__);
  	fprintf(stderr, "[%s] Exit.\n", __func__);
  	exit(1);
      }
    }
  }
  bam_iter_destroy(iter);
  if (b) kmp_free(Read, rmp, b);

  InsertList *insl = init_InsertList(1000);
  khint_t k;
  for (k=kh_begin(kh); k<kh_end(kh); ++k) {
    if (kh_exist(kh, k)) push_InsertList(insl, kh_val(kh,k));
  }

  return insl;
}

/* compute the sum of base quality
 * for a region of a read, end is one past last base */
static inline uint32_t
qual_sum(bam1_t *b, uint32_t beg, uint32_t end) {
  size_t i; uint32_t sum = 0;
  for (i = beg; i < end; i++) sum += bam1_qual(b)[i];
  return sum;
}

/* find the cigar type and length at the given target site 

   return if 
   1) r == t and op is insertion
   2) r2 > t+1
   3) r2 == t+1 and not a deletion right after
   4) r, r2, op point to the last cigar

 */
void
type_until(uint32_t t, bam1_t *b, uint32_t *op, uint32_t *l,
	   uint32_t *r, uint32_t *r2, uint32_t *q, uint32_t *q2) {

  bam1_core_t *c = &b->core;
  uint32_t *cigar = bam1_cigar(b);

  *q  = 0;
  *r  = c->pos+1;
  *r2 = *r;
  *q2 = *q;
  *op = 0;
  *l  = 0;
  uint32_t i; 
  for (i=0; i<c->n_cigar; ++i) {
    *l = bam_cigar_oplen(cigar[i]);
    *op = bam_cigar_op(cigar[i]);

    if (*r>=t && *op==BAM_CINS) return;

    switch (*op) {
    case BAM_CMATCH:
      *r2 = *r+*l;
      *q2 = *q+*l;
      break;
    case BAM_CINS:
      *q2 = *q+*l;
      break;
    case BAM_CSOFT_CLIP:
      *q2 = *q+*l;
      break;
    case BAM_CDEL:
      *r2 = *r+*l;
      break;
    case BAM_CREF_SKIP:
      *r2 += *l;
      break;
    default:
      fprintf(stderr, "Unknown cigar, %u\n", *op);
      abort();
    }

    if (*r2 > t+1) return;
    if (*r2 == t+1) {
      if (i+1 < c->n_cigar) {
	if (bam_cigar_op(cigar[i+1]) != BAM_CDEL &&
	    bam_cigar_op(cigar[i+1]) != BAM_CINS)
	  return;
      } else {
	return;
      }
    }

    *r = *r2;
    *q = *q2;
  }
}

/* find alternative alleles
 * rseq : reference sequence
 * beg : beginning coordinate of rseq 
 * results is put to ra*/
void plp_alt1(AllelePList *al, Allele *ref,
	      ReadAlign *ra, char *rseq,
	      size_t beg, uint32_t t) {

  if (!ra->r) return;

  bam1_t *b = ra->r;
  uint8_t *s=bam1_seq(b);
  uint8_t *qual = bam1_qual(b);
  bam1_core_t *c=&b->core;
  if (c->flag & BAM_FDUP) return; /* skip duplicates */

  uint32_t i, l=0;
  uint32_t q, q2, r, r2, op;
  /* bam pos is zero-based */
  type_until(t, b, &op, &l, &r, &r2, &q, &q2);
  if (r>t && (q==0 || !(r==t+1 && (op == BAM_CDEL || op == BAM_CINS)))) return;
  if ((r2<=t)||(op==BAM_CSOFT_CLIP)) return; /* failure */
  if (bscall(s, q+t-r) == 'N') return;	     /* return if "N" */

  Allele *a;
  switch (op) {
  case BAM_CMATCH:

    if (bscall(s, q+t-r) == rseq[t-beg]) { /* match */
      /* if (qual[q+t-r] > min_baseQ) ref->rc++; */
      ra->qpos = q+(t-r);
      ra->a = ref;
      ra->squal = qual[q+t-r];
      return;

    } else {			/* mismatch */

      a = calloc(1, sizeof(Allele));
      a->tid = c->tid;

      a->cigar = BAM_CDIFF;
      uint8_t lm = 1;

      /* extend backward */
      q2 = q+(t-r);
      r2 = t;
      while (q2>q && (bscall(s, q2-1) != rseq[r2-1-beg])) {
      	--q2; --r2; ++lm;
      }
      a->rpos = r2;
      ra->qpos = q2;

      /* extend forward */
      q2 = q+(t-r)+1;
      r2 = t;
      while (q2<(unsigned)c->l_qseq && bscall(s, q2) != rseq[r2+1-beg]) {
      	++q2; ++r2; ++lm;
      }

      a->seq = (char*) malloc(lm*sizeof(char));
      for (i=0; i<lm; ++i) {
      	a->seq[i] = bscall(s, ra->qpos+i);
      }
      ra->squal = (double) qual_sum(b, ra->qpos, ra->qpos+lm) / (double) lm;
      a->len = lm;
      ra->a = insert_allele(al, a);
    }
    return;

  case BAM_CINS:

    a = calloc(1, sizeof(Allele));
    a->tid = c->tid;

    a->cigar = BAM_CINS;
    a->len = l;
    /* seq does not include the base before insertion */
    a->seq = (char*) malloc(l*sizeof(char));
    /* a->seq = realloc(a->seq, l*sizeof(char)); */
    for (i=0; i<l; ++i) a->seq[i] = bscall(s, q+i);
    a->rpos = r-1; 		/* record previous base position */
    ra->squal = (double) qual_sum(b, q, q+l) / (double) l;
    ra->qpos = q;
    ra->a = insert_allele(al, a);

    return;

  case BAM_CDEL:
    /* the base before the deletion is the 1st base in seq
     * squal is the average qualities of the base before and after */

    a = calloc(1, sizeof(Allele));
    a->tid = c->tid;
    a->cigar = BAM_CDEL;
    a->len = l+1;
    /* seq start from the base before insertion */
    a->seq = (char*) malloc(a->len*sizeof(char));
    /* a->seq = realloc(a->seq, a->len*sizeof(char)); */
    for (i=0; i<(unsigned)a->len; ++i) a->seq[i] = rseq[r+i-beg-1];
    a->rpos = r-1; 		/* record previous base position */
    ra->squal = (double) qual_sum(b, q, q+2) / 2.0;
    ra->qpos = q;
    ra->a = insert_allele(al, a);

    return;

  case BAM_CREF_SKIP:           /* shouldn't come here for RNA-seq data */
    return;
  default:
    fprintf(stderr, "Unknown cigar, %u\n", op);
    exit(1);
    break;
  }

}

void prepare_pileup_bcf(char *bcf_fn, char *ref_fn,
			PlpBamData *bam, PlpBCFData *bcf, 
			char *info_str, char *fmt_str) {

  bcf->bcf_fn = bcf_fn;
  bcf->bp = bcf_open(bcf_fn, "w");
  if (!bcf->bp->fp) {
    fprintf(stderr, "[%s] Error: cannot open file for write: %s\nAbort.\n", __func__, bcf_fn);
    fflush(stderr);
    exit(1);
  }
  bcf->bh = calloc(1, sizeof(bcf_hdr_t));
  bam_header_t *h = bam_header_read(bam->fp);

  bcf_hdr_t *bh = bcf->bh;
  bcf_t *bp = bcf->bp;

  kstring_t s;
  int i;

  /* sequence names */
  s.l = s.m = 0; s.s = 0;
  for (i = 0; i < h->n_targets; ++i) {
    kputs(h->target_name[i], &s);
    kputc('\0', &s);
  }
  bh->l_nm = s.l;
  bh->name = malloc(s.l);
  memcpy(bh->name, s.s, s.l);

  /* sample names */
  s.l = 0;
  kputs("SAMPLE", &s); kputc('\0', &s);
  bh->l_smpl = s.l;
  bh->sname = malloc(s.l);
  memcpy(bh->sname, s.s, s.l);

  /* header text */
  s.l = 0;
  ksprintf(&s, "##clinsek\n");

  /* INFO */
  ksprintf(&s, info_str);
  /* ksprintf(&s, "##INFO=<ID=AN,Number=1,Type=Integer,Description=\"Total Allele Count\">\n"); */
  /* ksprintf(&s, "##INFO=<ID=AC,Number=A,Type=Integer,Description=\"Alternate Allele Count\">\n"); */
  /* ksprintf(&s, "##INFO=<ID=AF,Number=A,Type=Float,Description=\"Allele Frequency\">\n"); */
  /* ksprintf(&s, "##INFO=<ID=SVTYPE,Number=1,Type=String,Description=\"Allele Frequency\">\n"); */

  /* FORMAT */
  ksprintf(&s, fmt_str);
  /* ksprintf(&s, "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Raw read depth (without quality filtering)\">\n"); */
  /* ksprintf(&s, "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n"); */
  /* ksprintf(&s, "##FORMAT=<ID=GL,Number=G,Type=Float,Description=\"Genotype Likelihoods (ln-transformed)\">\n"); */
  /* ksprintf(&s, "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype quality (Phred-scaled from genotype probability)\">\n"); */

  /* reference */
  ksprintf(&s, "##reference=file://%s\n", ref_fn);
  h->dict = sam_header_parse2(h->text);
  int nseq;
  const char *tags[] = {"SN","LN","UR","M5",NULL};
  char **tbl = sam_header2tbl_n(h->dict, "SQ", tags, &nseq);
  for (i=0; i<nseq; i++) {
    ksprintf(&s, "##contig=<ID=%s", tbl[4*i]);
    if ( tbl[4*i+1] ) ksprintf(&s, ",length=%s", tbl[4*i+1]);
    if ( tbl[4*i+2] ) ksprintf(&s, ",URL=%s", tbl[4*i+2]);
    if ( tbl[4*i+3] ) ksprintf(&s, ",md5=%s", tbl[4*i+3]);
    kputs(">\n", &s);
  }
  if (tbl) free(tbl);
  bh->txt = s.s;
  bh->l_txt = 1 + s.l;


  bcf_hdr_sync(bh);
  bcf_hdr_write(bp, bh);
  bam_header_destroy(h);

}

void resolve_mate_aln(Insert *ins) {

  ReadAlign *ra1 = &ins->a1;
  ReadAlign *ra2 = &ins->a2;

  if (ra1->a) {
    if (ra2->a) {
      if (allele_equal(ra1->a, ra2->a)) {
	ins->a = ra1->a;
	ins->squal = max(ra1->squal, ra2->squal);
      } else {
	if (ra1->squal > ra2->squal) {
	  ins->a = ra1->a;
	  ins->squal = ra1->squal;
	} else {
	  ins->a = ra2->a;
	  ins->squal = ra2->squal;
	}
      }
    } else {
      ins->a = ra1->a;
      ins->squal = ra1->squal;
    }
  } else if (ra2->a) {
    ins->a = ra2->a;
    ins->squal = ra2->squal;
  } else {
    ins->a = 0;
  }

  return;
}

static void
plp_insert1(PlpAlleleData *ad, Insert *ins, PlpRefData *refseq, Site *st, uint8_t min_baseQ) {
  plp_alt1(ad->al, &ad->ref, &ins->a1, refseq->seq,
	   refseq->beg, st->pos);
  plp_alt1(ad->al, &ad->ref, &ins->a2, refseq->seq,
	   refseq->beg, st->pos);
  resolve_mate_aln(ins);
  if (ins->a) {
    /* if (ins->a->cigar != BAM_CDIFF || ins->squal > min_baseQ) { */
    if (ins->squal > min_baseQ) {
      ins->a->ic++;
      ad->an++;
    }
  }

  if (ins->a1.a) {
    ins->a1.a->rc++;
    ad->dp++;
  }
  if (ins->a2.a) {
    ins->a2.a->rc++;
    ad->dp++;
  }

  return;
}


void plp_inserts(PlpAlleleData *ad, InsertList *insl, PlpRefData *refseq, Site *st, uint8_t min_baseQ) {
  uint32_t i;
  for (i=0; i<insl->size; ++i) {
    Insert *ins = get_InsertList(insl, i);
    plp_insert1(ad, ins, refseq, st, min_baseQ);
  }
}

/* void get_coverage(PlpAlleleData *ad, uint32_t *dp, uint32_t *an) { */
/*   *dp = ad->ref.rc; */
/*   *an = ad->ref.ic; */
/*   uint32_t i; */
/*   for (i=0; i<ad->al->size; ++i) { */
/*     Allele *a = ref_AlleleList(ad->al, i); */
/*     if (!a->recorded) { */
/*       *dp += a->rc; */
/*       *an += a->ic; */
/*     } */
/*   } */
/* } */

