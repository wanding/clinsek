#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "kcnts.h"
#include "samtools/kstring.h"

static Site*
parse_target(SiteList *sl, kstring_t *str, RefSeq *rs, size_t rlen) {

  Site *s = malloc(sizeof(Site));
  push_SiteList(sl, s);
  char *pch = strtok(str->s, ":");
  s->chrm = chrm2tid(rs, pch);
  s->pos = (unsigned) atoi(strtok(NULL, ":"));
  s->hsl = init_HSiteList(2);
  s->kl = init_KmerList(6);
  s->rpos = rlen+50;
  str->l = 0;
  return s;
}

static void parse_homo(Site *st, kstring_t *str, RefSeq *rs) {
  if (!st) {
    fprintf(stderr, "[%s:%d] Homologous site format error.\n", __func__, __LINE__);
    fflush(stderr);
    exit(1);
  }
  HomoSite *hs = next_ref_HSiteList(st->hsl);
  strtok(str->s, " ");
  hs->chrm = chrm2tid(rs, strtok(NULL, ":"));
  hs->pos = (unsigned) atoi(strtok(NULL, ":"));
  hs->str = (uint8_t) atoi(strtok(NULL, " "));
  hs->h_start = atoi(strtok(NULL, "(,"));
  hs->h_end = atoi(strtok(NULL, ") "));
  char *dstr;
  hs->diffs = init_DiffList(2);
  while (1) {
    dstr = strtok(NULL, "(,/)");
    if (dstr) {
      SDiff *sd = next_ref_DiffList(hs->diffs);
      sd->tpos = atoi(dstr);
      sd->type = atoi(strtok(NULL, "/"));
      sd->alt_seq = strdup(strtok(NULL, ",)"));
    } else break;
  }
  hs->rpos = 1000;
  hs->seq = 0; hs->swseq = 0;
  str->l = 0;
}


KCnts* load_sites_core(char *sfn, char *rfn, kstring_t *sitestr) {

  /* fprintf(stderr, "rfn: %s", rfn); */
  /* fflush(stderr); */
  RefSeq *rfs = init_RefSeq(rfn);

  const size_t rlen = 100;

  SiteList *sl=init_SiteList(1023);
  if (sitestr) parse_target(sl, sitestr, rfs, rlen);

  Site *s = 0;
  uint32_t hs_cnt = 0;

  if (sfn) {
    FILE *LIB;
    LIB = fopen(sfn,"r");
    
    if (!LIB) {
      fprintf(stderr, "[%s] Error, cannot open site file.\n", __func__);
      fflush(stderr);
      exit(1);
    }

    kstring_t tst, hst;
    uint8_t ist = 1; 
    tst.l = tst.m = 0; tst.s = 0;
    hst.l = hst.m = 0; hst.s = 0;

    while (1) {
      int c=fgetc(LIB);
      if (c=='\n' || c==EOF) {
	if (ist) {
	  if (tst.l) s = parse_target(sl, &tst, rfs, rlen);
	} else {parse_homo(s, &hst, rfs); ++hs_cnt;}
	if (c==EOF) break;
	ist = 1;
      } else if (c=='[') {
	ist = 0;
	kputc(c, &hst);
      } else if (ist) {
	kputc(c, &tst);
      } else {
	kputc(c, &hst);
      }
    }
    free(tst.s);
    free(hst.s);
    fclose(LIB);
  }

  fprintf(stderr, "[%s] read %zu target sites and %u paralogous sites\n", __func__, sl->size, hs_cnt);
  fflush(stderr);

  qsort(sl->buffer, sl->size, sizeof(Site*), cmp_sites);

  size_t i;
  for (i=0; i<sl->size; ++i) get_SiteList(sl,i)->id = i;

  SSPool *ssp = init_SSPool((rlen+50)*2+1, sl->size, 0x1000000, rfs);
  fill_SSPool(ssp, sl, 0);

  char ksr[100];
  SeqHash  *sh = init_SeqHash();
  const int klocs[6] = {-75, -50, -25, 1, 26, 51};
  for (i=0; i<sl->size; ++i) {
    Site *s = get_SiteList(sl, i);
    SiteSeq *ss = get_SSPool(ssp, s);

    size_t j;
    for (j=0; j<6; ++j) {
      Kmer *k = (Kmer*) malloc(sizeof(Kmer));
      push_KmerList(s->kl, k);
      k->spos = klocs[j];
      k->site = s;

      Suffix *sf = insert_SeqHash(sh, ss->seq + s->rpos+k->spos);
      if (sf->data==NULL) sf->data = init_SeqList(2);
      Seq *sq = next_ref_SeqList((SeqList*) sf->data);
      sq->str = 0;
      sq->k = k;

      _nt256char_rev(ksr, ss->seq + s->rpos+k->spos, KLEN);
      sf = insert_SeqHash(sh, ksr);
      if (sf->data==NULL) sf->data = init_SeqList(2);
      sq = next_ref_SeqList((SeqList*) sf->data);
      sq->str = 1;
      sq->k = k;
    }
  }
  fprintf(stderr, "[%s] built %zu k-mers (%zu unique)\n", __func__, i*12, count_SeqHash(sh));
  fflush(stderr);

#ifdef DEBUGSITE
  
  for (i=0; i<sl->size; ++i) {
    Site *st = get_SiteList(sl, i);
    kstring_t str;
    str.l=str.m=0; str.s=0;
    ksprintf(&str, "%s:%u", ref_TargetList(tl, st->chrm)->name->string, st->pos);
    size_t j;
    for (j=0; j<st->hsl->size; ++j) {
      HomoSite *hs = ref_HSiteList(st->hsl, j);
      ksprintf(&str, "\t%s:%u", ref_TargetList(tl, hs->chrm)->name->string, hs->pos);
      ksprintf(&str, "(%u:%d/%d)", hs->str, hs->h_start, hs->h_end);
      size_t k;
      for (k=0; k<hs->diffs->size; ++k) {
	SDiff *d = ref_DiffList(hs->diffs, k);
	ksprintf(&str, "\t[%d/%u/%s]", d->tpos, d->type, d->alt_seq);
      }
      kputc('\n', &str);
    }
    puts(str.s);
  }

#endif // DEBUGSITE

  KCnts *kc = (KCnts*) malloc(sizeof(KCnts));
  
  kc->sl  = sl;
  kc->ssp = ssp;
  kc->sh  = sh;
  kc->rs  = rfs;
  kc->sfn = sfn;
  return kc;

}

#ifdef MAIN_SITE
int main() {
  
  const char *site_fn="cms46.hscan";
  const char *ref_fn="/workspace/zchong/clinsek/test_data/hg19.fa";

  KCnts *kc = load_sites(site_fn, ref_fn);

  free_KCnts(kc);

  return 0;
}

#endif
