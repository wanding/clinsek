#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "samtools/faidx.h"
#include "samtools/kstring.h"
#include "samtools/bam.h"
#include "samtools/klist.h"
#include "site.h"
#include "ref.h"
#include "ssw.h"
#include "fa_reader.h"
#include "utils.h"
#include "wstr.h"

#define _HS_HASH_SIZE 0x1000000
#define _HS_SITE_BATCH_SIZE 0x20000

/* The code find homologous sites given a list of target sites.
 * Each homologous site share homology in 1 of the 3 48-kmers
 * positioned around the site to the target site.
 * Homology is defined by sequence similarity upto 20 alignment
 * score (4-mis-matches or equivalent numbers of indels).
 *
 * In the alignment, one shouldn't fetch read that have more than
 * 4 mis-matches to the site. Otherwise, it can cause false mapping
 * from homologous sites that share 4+ mis-matches to the target site.
 *
 * */

/* 1. build list of 48-mers for target site
 * 2. split the 48-mer to 12-mer, and hash them
 * 3. scan the reference genome for the 25-mer within one edit distance away
 * 4. report homologous sites if a (homologous) site share 48-mer with the target site */

#define _HS_S_MATCH 2
#define _HS_S_MISMATCH 3
#define _HS_S_GAPOPEN 4
#define _HS_S_GAPEXT 2
#define _HS_S_NOISE 20

const int8_t _hs_score_matrix[25] = {
  _HS_S_MATCH,     -_HS_S_MISMATCH, -_HS_S_MISMATCH, -_HS_S_MISMATCH, 0,
  -_HS_S_MISMATCH,  _HS_S_MATCH,    -_HS_S_MISMATCH, -_HS_S_MISMATCH, 0,
  -_HS_S_MISMATCH, -_HS_S_MISMATCH,  _HS_S_MATCH,    -_HS_S_MISMATCH, 0,
  -_HS_S_MISMATCH, -_HS_S_MISMATCH, -_HS_S_MISMATCH, _HS_S_MATCH,     0,
  0,           0,           0,           0,           0
};

s_align* _hs_ssw_align(int8_t *query_seq, int query_len, int8_t *ref_seq, int ref_len) {
  s_profile *profile = ssw_init(query_seq, query_len, _hs_score_matrix, 5, 2);
  s_align *align = ssw_align(profile, ref_seq, ref_len, _HS_S_GAPOPEN, _HS_S_GAPEXT, 1, 0, 0, query_len / 2);
  ssw_destroy(profile);
  return align;
}

static inline int cnt_bl(uint8_t *bl, size_t len) {
  size_t i; int s = 0;
  for (i=0; i<len; ++i) if (bl[i]) ++s;
  return s;
}

char *int2bin(uint32_t a, char *buffer) {
  buffer += 32;
  int i;
  for (i=31; i>= 0; i--) {
    *buffer-- = (a & 1) + '0';
    a >>= 1;
  }
  return buffer;
}

/* typedef struct { */
/*   Kmer super; */
/*   int8_t seq[25]; */
/*   int8_t seq_r[25]; */
/* } __attribute__((__packed__)) KmerExt; */
typedef struct {
  uint32_t flen;	   /* length of flanking region to be aligned */
  uint32_t min_score;
  uint32_t max_par;
  uint8_t output_bl;
  int verbose;
} __HS_Conf;

typedef struct {
  Site    *st;
  char    *t_seq;
  int8_t  *t_seq2;
  uint32_t h_tid;
  uint32_t h_seq_beg;		/* the beginning coordinate of h_seq */
  uint32_t h_seq_end;		/* the ending coordinate of h_seq */
  char    *h_seq;
  int8_t  *h_seq2;
  uint8_t  str;
} AlnAux;

typedef struct {
  uint32_t t_beg;
  uint32_t h_beg;
  s_align *a;
} Aln;

/* rv: seed key
 * loc: current middle location
 * p and p2: pointers directed to current middle location
 * in the sequence buffer */
#define __RS_BUF_SIZE 0x10000
typedef struct {
  char *buf;
  char *buf_end;
  char *head, *tail, *p;
  int8_t *buf2;
  int8_t *head2, *tail2, *p2;
  uint32_t flen, rv, loc;
  FAReader *fr;
  int32_t skip;			/* number of bases to skip before reading */
} RefScanner;


static RefScanner* init_RS(FAReader *fr, uint32_t flen) {

  RefScanner *rs = malloc(sizeof(RefScanner));
  rs->flen = flen;
  rs->fr = fr;
  rs->rv = 0;

  rs->buf  = calloc(__RS_BUF_SIZE, sizeof(char));
  rs->buf_end = rs->buf+__RS_BUF_SIZE-1;
  rs->head = rs->buf;
  rs->tail = rs->buf + 2*rs->flen;
  rs->p    = rs->buf + rs->flen;

  rs->buf2  = calloc(__RS_BUF_SIZE, sizeof(int8_t));
  rs->head2 = rs->buf2;
  rs->tail2 = rs->buf2 + 2*rs->flen;
  rs->p2    = rs->buf2 + rs->flen;

  rs->loc = rs->flen+1;
  rs->skip = 0;
  /* fill buffers */
  uint32_t i; char base;
  for (i=0; i<2*rs->flen+1; ++i) {
    base = fa_getbase(rs->fr);
    rs->buf[i] = base;
    rs->buf2[i] = nt256char_to_nt256int8_table[(unsigned char) base];
    if (base == 'N') rs->skip = 2 * rs->flen + 1;
    else if (rs->skip>0) --rs->skip;
  }

  /* initialize seed key */
  char *p;
  for (p = rs->p-11; p<=rs->p; ++p) {
    if (*p!='N') rs->rv = ((((uint32_t) (rs->rv<<2)) & 0xFFFFFF) | char_to_nt4_table[(unsigned char) *p]);
  }

  return rs;
}

static void destroy_RS(RefScanner *rs) {
  free(rs->buf); free(rs->buf2); free(rs);
}

static int RS_adv(RefScanner *rs) {

  if (rs->tail == rs->buf_end) {
    memmove(rs->buf, rs->head, 2*rs->flen+1);
    memmove(rs->buf2, rs->head2, 2*rs->flen+1);
    rs->head  = rs->buf;
    rs->tail  = rs->buf + 2*rs->flen;
    rs->p     = rs->buf + rs->flen;
    rs->head2 = rs->buf2;
    rs->tail2 = rs->buf2 + 2*rs->flen;
    rs->p2    = rs->buf2 + rs->flen;
  }

  ++rs->loc;
  ++rs->head; ++rs->p; ++rs->tail;
  ++rs->head2; ++rs->p2; ++rs->tail2;
  *rs->tail = fa_getbase(rs->fr);
  *rs->tail2 = nt256char_to_nt256int8_table[(unsigned char) *rs->tail];
  if (*rs->tail == 0) return 0;

  return 1;
}

static int RS_nextloc(RefScanner *rs) {

  do {
    if (!RS_adv(rs)) return 0;

    if (*rs->tail == 'N') {
      rs->skip = 2*rs->flen + 1;
    } else {
      rs->rv = ((((uint32_t) (rs->rv<<2)) & 0xFFFFFF) | char_to_nt4_table[(unsigned char) (*rs->p)]);
      if (rs->skip>0) --rs->skip;
    }
  } while (rs->skip);

  /* while (rs->skip) { */
  /*   if (!RS_adv(rs)) return 0; */
  /*   if (*rs->tail == 'N') { */
  /* 	rs->skip = 2*rs->flen + 1; */
  /*   } else { */
  /* 	rs->rv = ((((uint32_t) (rs->rv<<2)) & 0xFFFFFF) | char_to_nt4_table[(unsigned char) (*rs->p)]); */
  /* 	--rs->skip; */
  /*   } */
  /* } */
    
  return 1;

}

static AlnAux *aln_prep_seqs(Site *st, RefScanner *rs, char *h_chrm_nm,
                             uint32_t h_seq_beg, uint32_t h_seq_end,
                             uint8_t str, SSPool *ssp) {

  AlnAux *aux = malloc(sizeof(AlnAux));
  /* assume h_seq_beg > rs->loc - rs->flen && h_seq_end < rs->loc + rs->flen */
  aux->h_seq_beg = h_seq_beg;
  aux->h_seq_end = h_seq_end;

  uint32_t h_len = h_seq_end - h_seq_beg + 1;
  aux->h_seq  = malloc(h_len);
  aux->h_seq2 = malloc(h_len);
  if (str) {
    _nt256char_rev(aux->h_seq, rs->p + (int) (h_seq_beg - rs->loc), h_len);
    _nt256int8_rev(aux->h_seq2, rs->p2 + (int) (h_seq_beg - rs->loc), h_len);
  } else {
    memcpy(aux->h_seq,  rs->p + (int) (h_seq_beg - rs->loc), h_len);
    memcpy(aux->h_seq2, rs->p2 + (int) (h_seq_beg - rs->loc), h_len);
  }

  SiteSeq *ss = get_SSPool(ssp, st);
  ss_encode_nt256int8(ss);
  aux->t_seq = ss->seq;
  aux->t_seq2 = ss->swseq;

  aux->h_tid = chrm2tid(ssp->rs, h_chrm_nm);
  aux->str = str;
  aux->st = st;
  return aux;
}

/* h_beg is absolute genomic coordinate
 * t_beg is relative to the target site */
Aln* aln_exec(AlnAux *aux, int32_t beg, uint32_t len) {

  Aln *aln = malloc(sizeof(Aln));
  aln->h_beg = (aux->h_seq_end - aux->h_seq_beg) / 2 + beg;
  aln->t_beg = aux->st->rpos + beg;
  aln->a = _hs_ssw_align(aux->h_seq2+aln->h_beg, len,
                         aux->t_seq2+aln->t_beg, len);
  return aln;
}

void free_aln(Aln *aln) {
  free_align(aln->a);
  free(aln);
}

void destroy_aln_aux(AlnAux *aux) {
  free(aux->h_seq);
  free(aux->h_seq2);
  free(aux);
}

/* find the position (0-based) of homologous site
 * if the homologous site is in a deletion, then
 * the last non-deletion base is regarded as
 * the homologous site */
int aln_resolve_hdisplc(Aln *aln, AlnAux *aux) {

  uint32_t h  = aln->h_beg + aln->a->read_begin1;
  uint32_t t  = aln->t_beg + aln->a->ref_begin1;
  uint32_t st = aux->st->rpos;
  uint32_t *cigar = aln->a->cigar;
  uint32_t op, len, t_inc=0, h_inc=0;
  do {
    t += t_inc; h += h_inc;
    op  = bam_cigar_op(*cigar);
    len = bam_cigar_oplen(*cigar);
    switch(op) {
    case BAM_CMATCH: t_inc = len; h_inc = len; break;
    case BAM_CINS: t_inc = 0; h_inc = len; break;
    case BAM_CDEL: t_inc = len; h_inc = 0; break;
    default:
      fprintf(stderr, "Unknown cigar, %u\n", op);
      fflush(stderr);
      exit(1);
    }
  } while (t+t_inc<st);

  switch (op) {
  case BAM_CMATCH: return h + (st - t);
  case BAM_CDEL:   return h;
  case BAM_CINS:
    fprintf(stderr, "Error, homologous site typed at insertion.\n");
    fflush(stderr);
    exit(1);
  default:
    fprintf(stderr, "Unknown cigar, %u\n", op);
    fflush(stderr);
    exit(1);
  }
}

/* pos is the putative position of the homologous
 * site (homologous to the target site) */
static HomoSite*
add_HSiteList(Aln *aln, AlnAux *aux, size_t h_pos) {

  if (aux->h_tid == aux->st->chrm && h_pos == aux->st->pos) return 0;
  unsigned i;
  HSiteList *hsl = aux->st->hsl;
  for (i=0; i<hsl->size; ++i) {
    HomoSite *qs = ref_HSiteList(hsl,i);
    if (h_pos == qs->pos && aux->h_tid == qs->chrm) return 0;
  }
  HomoSite *hs  = next_ref_HSiteList(hsl);
  hs->chrm = aux->h_tid;
  hs->str = aux->str;
  hs->pos  = h_pos;
  hs->diffs  = init_DiffList(2);
  hs->h_start = aln->t_beg + aln->a->ref_begin1 - aux->st->rpos;
  hs->h_end = aln->t_beg + aln->a->ref_end1 - aux->st->rpos;

  return hs;
}

/* analyze differences within the alignment */
static void aln_resolve_diffs(HomoSite *hs, Aln *aln, AlnAux *aux) {

  s_align *a = aln->a;
  char *tseq = aux->t_seq + aln->t_beg + a->ref_begin1;
  char *hseq = aux->h_seq + aln->h_beg + a->read_begin1;
  uint32_t t0 = aln->t_beg + a->ref_begin1 - aux->st->rpos;

  uint32_t op, len, j, w, t = 0, h = 0;
  uint32_t *cigars = aln->a->cigar;
  int i; SDiff *d; kstring_t str;
  for (i=0; i<a->cigarLen; ++i) {
    len = bam_cigar_oplen(cigars[i]);
    op = bam_cigar_op(cigars[i]);
    switch(op) {
    case BAM_CMATCH:
      for (j=0; j<len; ++j) {
        if (hseq[h+j] != tseq[t+j]) {
          d = next_ref_DiffList(hs->diffs);
          str.l = str.m = 0; str.s = 0;
          for (w = j; w<len && hseq[h+w] != tseq[t+w]; ++w) {
            kputc(hseq[w], &str);
          }
          kputc('\0', &str);
          d->alt_seq = str.s;
          d->type = BAM_CDIFF;
          d->tpos = t0 + t + j;
          j = w + 1;
        }
      }
      t += len;
      h += len;
      break;
    case BAM_CINS:
      d = next_ref_DiffList(hs->diffs);
      d->type = BAM_CINS;
      d->tpos = t0 + t;
      d->alt_seq = malloc(len+1);
      memcpy(d->alt_seq, hseq + h, len);
      d->alt_seq[len] = 0;
      h += len;
      break;
    case BAM_CDEL:
      d = next_ref_DiffList(hs->diffs);
      d->type = BAM_CDEL;
      d->tpos = t0 + t;
      d->alt_seq = malloc(len+1);
      memcpy(d->alt_seq, tseq + t, len);
      d->alt_seq[len] = 0;
      t += len;
      break;
    }
  }
}

static int cnt_seed_hash(SeqList **seed_hash) {
  size_t i; size_t cnt=0;
  for (i=0; i<_HS_HASH_SIZE; ++i) {
    if (seed_hash[i]) cnt+=1; /* seed_hash[i]->size; */
  }
  return cnt;
}


static SiteList*
load_sites(char *sfn, RefSeq *rs, __HS_Conf *conf) {

  FILE *s_fh = fopen(sfn,"r");
  char line[MAX_LINE_LEN];
  SiteList *sl=init_SiteList(1023);
  while (fgets(line, MAX_LINE_LEN, s_fh)) {
    char chrm[MAX_LINE_LEN];
    size_t pos;
    if (sscanf(line, "%[^:]:%zu", chrm, &pos) == 2) {
      Site *s=(Site*) malloc(sizeof(Site));
      push_SiteList(sl, s);
      s->chrm = chrm2tid(rs, chrm);
      s->pos  = pos;
      s->kl   = init_KmerList(6);
      s->rpos = conf->flen;	/* zero-based */
      s->hsl  = 0;
    }
  }
  fclose(s_fh);
  fprintf(stderr, "[%s] Read %zu sites\n", __func__, sl->size);
  fflush(stderr);

  return sl;
}

static SeqList**
build_seed_hash(SiteList *sl, SSPool *ssp) {

  char ksr[100];
  SeqList **seed_hash = calloc(_HS_HASH_SIZE, sizeof(SeqList*));
  const int klocs[8] = {-48, -36, -24, -12, 0, 12, 24, 36};
  uint32_t i;
  for (i=0; i<sl->size; ++i) {
    if ((i & 0xFFFFU) == 0) {
      fprintf(stderr, "\r[%s] Hashed %u sites.", __func__, i);
      fflush(stderr);
    }
    Site *st = get_SiteList(sl, i);
    SiteSeq *ss = get_SSPool(ssp, st);

    size_t j; char *s; Seq *sq; uint32_t v;
    for (j=0; j<8; ++j) {
      Kmer *k = (Kmer*) malloc(sizeof(Kmer));
      push_KmerList(st->kl, k);
      k->spos = klocs[j];
      k->site = st;
      k->homohits = 0;

      /* positive strand */
      size_t m;
      s = ss->seq + st->rpos + k->spos;
      for (v=0,m=12; m--; ++s) {
        v<<=2;
        v|=char_to_nt4_table[(unsigned char)*s];
      }
      if (seed_hash[v]==NULL) seed_hash[v] = init_SeqList(2);
      sq = next_ref_SeqList(seed_hash[v]);
      sq->str = 0;
      sq->k = k;

      /* negative strand */
      _nt256char_rev(ksr, ss->seq + st->rpos + k->spos, 12);

      s=ksr;
      for (v=0,m=12; m--; ++s) {
        v<<=2;
        v|=char_to_nt4_table[(unsigned char)*s];
      }
      if (seed_hash[v]==NULL) seed_hash[v] = init_SeqList(2);
      sq = next_ref_SeqList(seed_hash[v]);
      sq->str = 1;
      sq->k = k;
    }

#ifdef DEBUGHOMO
    printf("\n");
#endif
  }

  fprintf(stderr, "\r[%s] Hashed %u sites.\n", __func__, i);
  fprintf(stderr, "[%s] Built %u k-mers (%d unique)\n", __func__, i*16, cnt_seed_hash(seed_hash));
  fflush(stderr);

  return seed_hash;
}

static void
output_sites(FILE *o_fh, SiteList *sl, __HS_Conf *conf,
             RefSeq *rs, uint8_t *blacklist) {
  uint32_t i;
  for (i=0; i<sl->size; ++i) {
    Site *st = get_SiteList(sl, i);
    if (!conf->output_bl && blacklist[st->id]) continue;
    kstring_t str;
    str.l = str.m = 0; str.s = 0;
    ksprintf(&str, "%s:%u", tid2chrm(rs, st->chrm), st->pos);
    kputc('\n', &str);
    if (st->hsl) {
      size_t j;
      for (j=0; j<st->hsl->size; ++j) {
        HomoSite *hs = ref_HSiteList(st->hsl, j);
        ksprintf(&str, "[H] %s:%u:%u (%d,%d)", tid2chrm(rs, hs->chrm), hs->pos, hs->str, hs->h_start, hs->h_end);
        kputc('(', &str);
        size_t k;
        for (k = 0; k<hs->diffs->size; ++k) {
          if (k!=0) kputc(',', &str);
          SDiff *d = ref_DiffList(hs->diffs, k);
          ksprintf(&str, "%d/%u/%s", d->tpos, d->type, d->alt_seq);
        }
        kputc(')', &str); kputc('\n', &str);
      }
    }
    fputs(str.s, o_fh);
    free(str.s);
  }
}



static int
ref_scan(SiteList *sl, SeqList **seed_hash, char *rfn, FILE *log,
         __HS_Conf *conf, SSPool *ssp, uint8_t *blacklist) {

  FAReader *fr=init_fa(rfn);
  uint32_t i = 0;
  int homocnt = 0;
  while (fa_next_seq(fr)) {

    RefScanner *rs = init_RS(fr, 3*conf->flen);
    while (RS_nextloc(rs)) {

      if (conf->verbose>5) {
        fprintf(stderr, "\r[%s] processing %s:%u\033[K",
                __func__, fr->seq_name, rs->loc);
        fflush(stderr);
      }
      /* if ((rs->loc & 0xFFFFFU) == 0) { */
      if ((rs->loc & 0xFFFFU) == 0) {
        fprintf(stderr, "\r[%s] processing %s:%u\033[K",
                __func__, fr->seq_name, rs->loc);
        fflush(stderr);
      }

      SeqList *sql = seed_hash[rs->rv];
      if (sql != NULL) {

        for (i=0; i<sql->size; ++i) {
          Seq *sq = ref_SeqList(sql, i);
          Kmer *k = sq->k; Site *st=k->site;

          if (conf->verbose>6) {
            fprintf(stderr, "\r[%s] hit site %s:%zu\033[K",
                    __func__, tid2chrm(ssp->rs, st->chrm), st->pos);
            fflush(stderr);
          }
          if (k->homohits > 50) continue;
          k->homohits++;

          if (blacklist[st->id]) continue;
	      
          /* Find the homologous segment that contains the seed.
             Seed is shorter than the homologous segment.
             Multiple homologous segments could contain the seed.
             E.g., seed [-24,-12] is contained in both segment [-48,0] and [-24, 24] */
          const int trgt_starts[3] = {-48, -24, 0};
          uint8_t j;
          for (j=0; j<3; ++j) {
            if (trgt_starts[j]<=k->spos && trgt_starts[j]+48>=k->spos+12) {
              int trgt_start = trgt_starts[j];

              if (sq->str) { /* negative strand */

                /* |---------============--->|     |<---===========----------|
                 * |         |                 =>  |              |
                 * tgt_start k->spos               homo_start     loc
                 * <---------- 48 ---------->
                 *           <--- 12 --->  */

                /* h_pos is a rough estimate of the position
                 * of the homologous site */

                size_t h_pos = rs->loc+k->spos;
                if (h_pos == st->pos) continue;
                AlnAux *aux = aln_prep_seqs(st, rs, fr->seq_name,
                                            h_pos-conf->flen,
                                            h_pos+conf->flen,
                                            1, ssp);
                Aln *aln = aln_exec(aux, trgt_start, 48);
                if (aln->a->score1 > conf->min_score) {
                  free_aln(aln);
                  aln = aln_exec(aux, -conf->flen, 2*conf->flen+1);
                  h_pos = h_pos + conf->flen - aln_resolve_hdisplc(aln, aux);
                  if (!st->hsl) st->hsl = init_HSiteList(2);
                  if (st->hsl->size >= conf->max_par) {
                    fprintf(log, "[%s:%d] Too many homologous sites for site: %s:%zu. Skip.\n", __func__, __LINE__, tid2chrm(ssp->rs, st->chrm), st->pos);
                    fflush(stderr);
                    blacklist[st->id] = 1;
                  }

                  HomoSite *hs = add_HSiteList(aln, aux, h_pos);
                  if (hs) {
                    aln_resolve_diffs(hs, aln, aux);
                    ++homocnt;
                  }
                }
                destroy_aln_aux(aux);
                free_aln(aln);

              } else {	/* positive strand */

                size_t h_pos = rs->loc-11-k->spos;
                if (h_pos == st->pos) continue;
                AlnAux *aux = aln_prep_seqs(st, rs, fr->seq_name,
                                            h_pos-conf->flen,
                                            h_pos+conf->flen,
                                            0, ssp);
                Aln *aln = aln_exec(aux, trgt_start, 48);
                if (aln->a->score1 > conf->min_score) {
                  free_aln(aln);
                  aln = aln_exec(aux, -conf->flen, 2*conf->flen+1);
                  h_pos = h_pos - conf->flen + aln_resolve_hdisplc(aln, aux);
                  if (!st->hsl) st->hsl = init_HSiteList(2);
                  if (st->hsl->size >= conf->max_par) {
                    fprintf(log, "[%s:%d] Too many homologous sites for site: %s:%zu. Skip.\n", __func__, __LINE__, tid2chrm(ssp->rs, st->chrm), st->pos);
                    fflush(stderr);
                    blacklist[st->id] = 1;
                  }

                  HomoSite *hs = add_HSiteList(aln, aux, h_pos);
                  if (hs) {
#ifdef DEBUGHOMO
                    printf("\n[%s:%d] %u:%zu finds %u:%zu(%u) homologous\n",  __func__, __LINE__, st->chrm, st->pos, aux->h_tid, h_pos, 0);
#endif // DEBUGHOMO
                    aln_resolve_diffs(hs, aln, aux);
                    ++homocnt;
                  }
                }
                destroy_aln_aux(aux);
                free_aln(aln);
              }
            }
          }
        }
      }
    }
    destroy_RS(rs);
  }
  destroy_fa(fr);

  fprintf(stderr, "\r[%s] Finished all chromosomes.\033[K\n", __func__);
  fprintf(stderr, "[%s] found %d homologous sites.\n", __func__, homocnt);
  fprintf(stderr, "[%s] %d sites blacklisted for finding too many paralogs.\n", __func__, cnt_bl(blacklist, sl->size));
  fflush(stderr);

  return homocnt;
}

void destroy_seed_hash(SeqList **seed_hash) {
  uint32_t i;
  for (i=0; i<_HS_HASH_SIZE; ++i) {
    if (seed_hash[i]) free_SeqList(seed_hash[i]);
  }
  free(seed_hash);
}


int
homoscan(char *sfn, char *rfn, char *ofn, char *log_fn, __HS_Conf *conf) {

  SSPool *ssp = init_SSPool(2*conf->flen+1, _HS_SITE_BATCH_SIZE,
                            0x4000000, init_RefSeq(rfn));

  SiteList *sl0 = load_sites(sfn, ssp->rs, conf);

  if (!sl0->size) return 0;

  /* sort sites by chromosome and then coordinates */
  qsort(sl0->buffer, sl0->size, sizeof(Site*), cmp_sites);

  /* batch the sites */
  uint32_t n_batch = ((sl0->size-1) / _HS_SITE_BATCH_SIZE + 1);
  if (n_batch > 1) {
    fprintf(stderr, "[%s] Split the sites into %u batches. \n", __func__, n_batch);
    fflush(stderr);
  }

  FILE *output = wz_fopen(__func__, ofn, "w");
  FILE *log = wz_fopen(__func__, log_fn, "w");
  
  uint32_t ib;
  int homocnt=0;
  for (ib = 0; ib < n_batch; ++ib) {

    fprintf(stderr, "[%s] Processing batch %u/%u. \n", __func__, ib+1, n_batch);
    fflush(stderr);

    uint32_t b_start = ib * _HS_SITE_BATCH_SIZE;
    uint32_t b_end = min((ib+1)*_HS_SITE_BATCH_SIZE, sl0->size);
    SiteList *sl = init_SiteList(1023);
    uint32_t i;
    for (i=b_start; i<b_end; ++i) {
      Site *s = get_SiteList(sl0, i);
      s->id = i-b_start;
      push_SiteList(sl, s);
    }

    /* hash site sequences */
    clear_SSPool(ssp);
    fill_SSPool(ssp, sl, 0);

    uint8_t *blacklist = calloc(sl->size, sizeof(uint8_t));
    SeqList **seed_hash = build_seed_hash(sl, ssp);

    /* run through the entire reference */
    homocnt += ref_scan(sl, seed_hash, rfn, log, conf, ssp, blacklist);
    output_sites(output, sl, conf, ssp->rs, blacklist);
    free_SiteList(sl);
    free(blacklist);
    destroy_seed_hash(seed_hash);
  }

  fclose(output);

  /* clean up */
  free_RefSeq(ssp->rs);
  destroy_SSPool(ssp);
  destroy_SiteList(sl0);

  return 0;
}

static int usage() {
  fprintf(stderr, "\n");
  fprintf(stderr, "Usage: clinsek homo_scan [options] -r ref.fa -s site_list -o site_list.hscan.\n");
  fprintf(stderr, "Input options:\n");
  fprintf(stderr, "     -r FILE   reference file [REQUIRED]\n");
  fprintf(stderr, "     -s FILE   input site list [REQUIRED]\n");
  fprintf(stderr, "     -o FILE   output site list [input+.hscan]\n");
  fprintf(stderr, "     -g FILE   log file for blacklisting highly paralogous sites [input+.hscan.log]\n");
  fprintf(stderr, "     -l INT    length of flanking region to record differences [100].\n");
  fprintf(stderr, "     -m INT    minimum seed alignment score in 48 bp seed [80].\n");
  fprintf(stderr, "     -p INT    maximum paralogs allowed [50].\n");
  fprintf(stderr, "     -b        output sites blacklisted for too many homologous sites.\n");
  fprintf(stderr, "     -V INT    verbose level [0].\n");
  fprintf(stderr, "     -h        this help.\n");
  fprintf(stderr, "\n");
  return 1;
}

int main_homoscan(int argc, char *argv[]) {

  __HS_Conf conf = {.flen = 100, .min_score = 80,
                    .max_par = 50, .output_bl = 0, .verbose=0};

  char *sfn=0, *rfn=0, *ofn=0, *log_fn=0;
  int c;
  while ((c = getopt(argc, argv, "s:r:o:p:l:m:V:h")) >= 0) {
    switch (c) {
    case 's': sfn = optarg; break;
    case 'r': rfn = optarg; break;
    case 'o': ofn = strdup(optarg); break;
    case 'g': log_fn = strdup(optarg); break;
    case 'l': conf.flen = atoi(optarg); break;
    case 'm': conf.min_score = atoi(optarg); break;
    case 'p': conf.max_par = atoi(optarg); break;
    case 'b': conf.output_bl = 1; break;
    case 'V': conf.verbose = atoi(optarg); break;
    case 'h': return usage();
    default:
      fprintf(stderr, "[%s:%d] Unrecognized command: %c.\n", __func__, __LINE__, c);
      fflush(stderr);
      exit(1);
      break;
    }
  }

  if (!(sfn && rfn)) {
    fprintf(stderr, "[%s:%d] missing required arguments.\n", __func__, __LINE__);
    fflush(stderr);
    return usage();
  }

  if (!ofn) ofn = wasprintf("%s.hscan", sfn);
  if (!log_fn) log_fn = wasprintf("%s.hscan.log", sfn);

  homoscan(sfn, rfn, ofn, log_fn, &conf);
  free(ofn);
  free(log_fn);

  return 0;
}
