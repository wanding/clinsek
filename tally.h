#ifndef _WZ_TALLY_H_
#define _WZ_TALLY_H_

#include <unistd.h>
#include <zlib.h>
#include <time.h>
#include "samtools/sam.h"
#include "si_buffer.h"
#include "kcnts.h"
#include "ssw.h"
#include "utils.h"
#include "rkgen.h"
#include "aln.h"

/* quality iterator */

uint8_t type_SIVar(Site *st, int8_t *ref_seq, CallConf *conf, int8_t *rseq_sw, s_align *a, QIter *qter);

int tally(KCnts *kc, char **fq_files1, char **fq_files2, VarSIBuffer *vb, RefSIBuffer *rb);

SiteFilter* prelim_call(KCnts *kc, VarSIBuffer *vb, RefSIBuffer *rb);

uint8_t type_SIVar(Site *st, int8_t *ref_seq, CallConf *conf, int8_t *rseq_sw, s_align *a, QIter *qter);

void append_soft_clipping(VarSI_H *tvh, s_align* a, int8_t *rseq_sw, int8_t *ref_seq, uint32_t rlen);

#endif /* _WZ_TALLY_H_ */
