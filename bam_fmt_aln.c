#include <math.h>
#include "samtools/bam.h"
#include "samtools/kstring.h"
#include "tgt_aln.h"
#include "utils.h"

#define CLINSEK_VERSION "0.1"

bamFile bam_prepare_aln(char *bam_fn, TargetList *tl) {
  bam_header_t header;
  header.n_targets = tl->size;
  header.target_name = (char**) malloc(sizeof(char*)*header.n_targets);
  header.target_len = (uint32_t*) malloc(sizeof(uint32_t)*header.n_targets);
  uint32_t i;
  for (i=0; i<tl->size; ++i) {
    Target *t=ref_TargetList(tl, i);
    header.target_name[i] = t->name->string;
    header.target_len[i] = t->len;
  }
  kstring_t s;
  s.m = s.l = 0; s.s = 0;
  ksprintf(&s, "@PG\tID:clinsek\tPN:clinsek\tVN:%s\n", CLINSEK_VERSION);
  header.text = s.s;
  header.l_text = s.l;
  /* header.text = ; */
  /* header.l_text = strlen(header.text); /\* 0; *\/ */
  /* header.dict = NULL; */
  /* header.rg2lib = NULL; */

  /* samfile_t *BAM = samopen(bam_fn, "wb", &header); */
  bamFile bam = bam_open(bam_fn, "w");
  bam_header_write(bam, &header);
  free(header.target_name);
  free(header.target_len);

  return bam;
}

void bam_fmt_pe(bam1s_t *b, uint8_t pair, bamFile bam, bam1_t *b1, kseq_t *ksq, int isize, double mutQ_all) {
  
  bam_aln_t *ba = pair ? &b->a2 : &b->a1;
  bam_aln_t *ma = pair ? &b->a1 : &b->a2;
  if (!ba->pos) return;

  bam1_core_t *c = &b1->core;
  c->tid  = b->chrm;
  c->pos  = ba->pos-1;	/* zero-based */
  c->flag = BAM_FPAIRED;
  if (ba->str^ma->str) c->flag |= BAM_FPROPER_PAIR;
  c->mtid = b->chrm;
  c->mpos = ma->pos-1;
  if (ba->mutQ+ma->mutQ == -mutQ_all) c->qual = 60;
  else c->qual = round(-log_subtract(0.0, (-ba->mutQ-ma->mutQ-mutQ_all)));
  c->isize = isize;
  b1->l_aux = 0;
  if (ba->str) c->flag |= BAM_FREVERSE;
  else c->flag |= BAM_FMREVERSE;
  c->flag |= pair ? BAM_FREAD2 : BAM_FREAD1;

  /* QNAME */
  c->l_qname = strlen(ksq->name.s) + 1;
  strcpy(bam1_qname(b1), ksq->name.s);

  /* CIGAR */
  c->n_cigar = ba->n_cigar;
  memcpy(bam1_cigar(b1), ba->cigar, 4*ba->n_cigar);

  /* SEQ */
  size_t rlen = strlen(ksq->seq.s);
  c->l_qseq = rlen;
  _ENCODE(bam1_seq(b1), ksq->seq.s, rlen);
  if (ba->str) nt16_rev_ip(bam1_seq(b1), rlen);

  b1->data_len=c->l_qname+c->n_cigar*4+c->l_qseq+((c->l_qseq+1)>>1);

  /* QUAL */
  uint8_t *q = bam1_qual(b1);
  uint32_t j;
  if (ba->str) {
    uint8_t *q2 = (uint8_t*)ksq->qual.s+rlen-1;
    for (j=0; j<rlen; ++j, ++q, --q2) *q=*q2-33;
  } else {
    uint8_t *q2 = (uint8_t*)ksq->qual.s;
    for (j=0; j<rlen; ++j, ++q, ++q2) *q=*q2-33;
  }
  bam_write1(bam, b1);

#ifdef DEBUGTALN
  printf("[%s:%d] as pe: read name: %s pair (0 for first in pair, 1 for second): %u n_cigar: %u chr_id: %u pos: %llu mutQ: %u mate mutQ: %u mis_all: %.3f c->qual: %u\n", __func__, __LINE__, ksq->name.s, pair, ba->n_cigar, b->chrm, (unsigned long long) ba->pos, ba->mutQ, ma->mutQ, mutQ_all, c->qual);
#endif // DEBUGTALN
}

void bam_fmt_se(bam1s_t *b, uint8_t pair, bamFile bam, bam1_t *b1, kseq_t *ksq, double mutQ_all) {

  bam_aln_t *ba = pair ? &b->a2 : &b->a1;
  if (!ba->pos) return;

  bam1_core_t *c = &b1->core;
  c->tid  = b->chrm;
  c->pos  = ba->pos-1;	/* zero-based */
  c->flag = 0; /* BAM_FPAIRED; */
  c->mtid = -1;
  c->mpos = -1;
  if (ba->mutQ == -mutQ_all) c->qual = 60;
  else c->qual = round(-log_subtract(0.0, (-ba->mutQ-mutQ_all)));
  c->isize = 0;
  b1->l_aux = 0;
  if (ba->str) c->flag |= BAM_FREVERSE;
  else c->flag |= BAM_FMREVERSE;
  c->flag |= pair ? BAM_FREAD2 : BAM_FREAD1;
  c->flag |= BAM_FMUNMAP;

  /* QNAME */
  c->l_qname = strlen(ksq->name.s) + 1;
  strcpy(bam1_qname(b1), ksq->name.s);

  /* CIGAR */
  c->n_cigar = ba->n_cigar;
  memcpy(bam1_cigar(b1), ba->cigar, 4*ba->n_cigar);

  /* SEQ */
  size_t rlen = strlen(ksq->seq.s);
  c->l_qseq = rlen;
  _ENCODE(bam1_seq(b1), ksq->seq.s, rlen);
  if (ba->str) nt16_rev_ip(bam1_seq(b1), rlen);

  b1->data_len=c->l_qname+c->n_cigar*4+c->l_qseq+((c->l_qseq+1)>>1);

  /* QUAL */
  uint8_t *q = bam1_qual(b1);
  uint32_t j;
  if (ba->str) {
    uint8_t *q2 = (uint8_t*)ksq->qual.s+rlen-1;
    for (j=0; j<rlen; ++j, ++q, --q2) *q=*q2-33;
  } else {
    uint8_t *q2 = (uint8_t*)ksq->qual.s;
    for (j=0; j<rlen; ++j, ++q, ++q2) *q=*q2-33;
  }
  bam_write1(bam, b1);

#ifdef DEBUGTALN
  fprintf(stdout, "[%s:%d] as se: read name: %s pair (0 for first in pair, 1 for second): %u n_cigar: %u chr_id: %u pos: %llu mutQ: %u mis_all: %.3f c->qual: %u\n", __func__, __LINE__, ksq->name.s, pair, ba->n_cigar, b->chrm, (unsigned long long) ba->pos, ba->mutQ, mutQ_all, c->qual);
#endif // DEBUGTALN

}

static uint32_t bam_aln_qlen(bam_aln_t *ba) {
  uint32_t k, qlen = 0;
  for (k=0; k<ba->n_cigar; ++k) {
    if (bam_cigar_type(bam_cigar_op(ba->cigar[k]))&1) {
      qlen += bam_cigar_oplen(ba->cigar[k]);
    }
  }
  return qlen;
}


/* b1 is just a container */
void
bam_format_aln(BamPList *vb, bamFile bam, bam1_t *b1, kseq_t *seq1, kseq_t *seq2) {

  size_t i;

  uint8_t is_pe = 0;
  for (i=0; i<vb->size; ++i) {
    bam1s_t *b = get_BamPList(vb, i);
    if (b->a1.pos && b->a2.pos) {
      is_pe = 1;
      break;
    }
  }

  if (is_pe) {			/* PE */

    /* calculate mapping quality  */
    double mutQ_allsites = 1.0;	/* mutQ_allsites > 0 indicates uninitialized */
    for (i=0; i<vb->size; ++i) {
      bam1s_t *b = get_BamPList(vb, i);
      if (b->a1.pos && b->a2.pos) {
	if (mutQ_allsites>0) mutQ_allsites = - (double) b->a1.mutQ - (double) b->a2.mutQ;
	else mutQ_allsites = log_sum(mutQ_allsites, -(double)b->a1.mutQ-(double)b->a2.mutQ);
      }
    }

    for (i=0; i<vb->size; ++i) {
      bam1s_t *b = get_BamPList(vb, i);
      int isize = 0;
      if (b->a1.pos && b->a2.pos) {
	if (b->a1.str) {
	  isize = b->a2.pos - b->a1.pos - bam_aln_qlen(&b->a1);
	} else {
	  isize = b->a2.pos - b->a1.pos + bam_aln_qlen(&b->a2);
	}

	bam_fmt_pe(b, 0, bam, b1, seq1, isize,  mutQ_allsites);
	bam_fmt_pe(b, 1, bam, b1, seq2, -isize, mutQ_allsites);
      }
    }
  } else {			/* SE */
    /* the aligned reads should cover the target site
     * and hence the mapping quality should still be
     * correct */


    double mutQ_allsites1 = 1.0;
    double mutQ_allsites2 = 1.0;
    for (i=0; i<vb->size; ++i) {
      bam1s_t *b = get_BamPList(vb, i);
      if (b->a1.pos) {
	if (mutQ_allsites1>0) mutQ_allsites1 = -(double) b->a1.mutQ;
	else mutQ_allsites1 = log_sum(mutQ_allsites1, -(double)b->a1.mutQ);
      }
      if (b->a2.pos) {
	if (mutQ_allsites2>0) mutQ_allsites2 = -(double) b->a2.mutQ;
	else mutQ_allsites2 = log_sum(mutQ_allsites2, -(double)b->a2.mutQ);
      }
    }

    for (i=0; i<vb->size; ++i) {
      bam1s_t *b = get_BamPList(vb, i);
      bam_fmt_se(b, 0, bam, b1, seq1, mutQ_allsites1);
      bam_fmt_se(b, 1, bam, b1, seq2, mutQ_allsites2);
    }
  }
}

