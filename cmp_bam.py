from subprocess import Popen, PIPE
import sys

bam_fn1 = sys.argv[1]
bam_fn2 = sys.argv[2]

b1 = Popen(["samtools", "view", bam_fn1], stdout=PIPE).stdout

read2pos1 = {}
for line in b1:
    pair = line.split()
    flag = int(pair[1])

    if int(pair[4]) < 20:       # mapping quality
        continue
    if flag & 0x40:
        read12 = 1
    else:
        read12 = 0
    read2pos1[(pair[0], read12)] = (pair[2], pair[3])


b2 = Popen(["samtools", "view", bam_fn2], stdout=PIPE).stdout
n_agree = 0
n_disag = 0
for line in b2:
    pair = line.split()
    flag = int(pair[1])
    if flag & 0x40:
        read12 = 1
    else:
        read12 = 0

    if int(pair[4]) < 20:       # mapping quality
        continue

    key = (pair[0], read12)
    if key in read2pos1:
        pos = read2pos1[key]
        if pos[0] == pair[2] and (int(pos[1]) - int(pair[3]) < 10):
            n_agree += 1
        else:
            print key, "\t", read2pos1[key], "\t", (pair[2], pair[3])
            n_disag += 1

sys.stderr.write("%d records agree and %d records disagree (concordance: %.3f%%).\n" % (n_agree, n_disag, float(n_agree) / (n_agree+n_disag) * 100))
