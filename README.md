**ClinSeK** is a targeted sequence analysis toolkit that allows quick and sensitive investigation of genetic variation at sites of interest from high-throughput sequencing data. The toolkit builds in standard procedure for alignment, variant characterization, marking dupliate reads and re-alignment around indels, all optimized targeting the sites of interest. It provides native support to standard NGS file format (e.g., fastq, bam, bcf etc). Depending on the number of target sites of interest, ClinSeK can greatly reduce analysis turn-around time and mapping storage (e.g., obtained actionable mutation profile from tumor NGS data in minutes). It is designed for quickly and accurately investigating a limited number of sites and for quickly cross-validating results from a base-to-base analysis pipeline.

--------

[TOC]

--------

### Overview
![overview-hd.png](https://bitbucket.org/repo/5gaRRR/images/4231929871-overview-hd.png)
Overview of ClinSeK workflow. Each arrow represents a ClinSeK subcommand.

### Download and Install

Version 1.2 is available [here](https://bitbucket.org/wanding/clinsek/get/v1.2.zip),
Version 1.2e is also available including a bug fix for large target site selection [here](https://bitbucket.org/wanding/clinsek/get/v1.2e.zip)
or through command line
```
#!bash

 $ wget https://bitbucket.org/wanding/clinsek/get/v1.2.zip
 $ unzip [downloaded zip]
 $ make
```

### Quick start

Download sample reads ([s1.fq](https://bitbucket.org/wanding/clinsek/downloads/s1.fq) and [s2.fq](https://bitbucket.org/wanding/clinsek/downloads/s2.fq)) and a list of targets ([actionable_sites.hscan](https://bitbucket.org/wanding/clinsek/downloads/actionable_sites.hscan)). One also needs a reference assembly (hs37d5.fa) which can be found from the NCBI ftp site `ftp://ftp.ncbi.nih.gov/genomes/H_sapiens`.

```
#!bash

$ clinsek tcall -r hs37d5.fa -1 s1.fq -2 s2.fq -o out -s actionable_sites.hscan
```
produces `out.bam` and `out.bcf` file. `out.bam` can be viewed through
```
#!bash
bcftools view out.bcf | less
```
outputs
```
#!text
##fileformat=VCFv4.1
##clinsek
##INFO=<ID=AN,Number=1,Type=Integer,Description="Total Allele Count">
##INFO=<ID=AC,Number=A,Type=Integer,Description="Alternate Allele Count">
##INFO=<ID=AF,Number=A,Type=Float,Description="Allele Frequency">
##INFO=<ID=SVTYPE,Number=1,Type=String,Description="Insertion or Deletion">
##FORMAT=<ID=DP,Number=1,Type=Integer,Description="Raw read depth (without quality filtering)">
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
##FORMAT=<ID=GL,Number=G,Type=Float,Description="Genotype Likelihoods (ln-transformed)">
##FORMAT=<ID=GQ,Number=1,Type=Integer,Description="Genotype quality (Phred-scaled from genotype probability)">
##reference=file:///home/wzhou1/reference/hs37d5.fa
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	SAMPLE
chr12	25398285	.	C	T	208	PASS	AN=20;DP=27;AC=13;AF=0.65	GT:DP:GL:GQ	0/1:27:-58.81,-2.96,-22.33:84
chr19	1207021	.	C	T	173	PASS	AN=12;DP=13;AC=10;AF=0.8333	GT:DP:GL:GQ	0/1:13:-49.21,-4.13,-7.01:13
```

### Prebuilt target site compilations

+ 535 actionable cancer somatic mutations (including SNVs and indels) identified by MD Anderson Institute of Personalized Cancer Therapy [[**download**]](https://bitbucket.org/wanding/clinsek/downloads/pct_variants_2-13-15.uniq.sites).  For detailed clinical actionability annotation, visit [personalizedcancertherapy.org](http://pct.mdanderson.org) 

 + 2316 hotspot somatic mutations (including SNVs and indels) identified by comparing against a Poisson model parameterized by average number of mutations of each subtype from COSMIC database (Chen et al. in submission). [[**download**]](https://onedrive.live.com/download?resid=D99B59444B50F902!3732&authkey=!AB4UGbdq4kVB0n8&ithint=file%2chscan)

 + 9807 ClinVar sites on 202 cancer genes which is available. [[**download**]](https://bitbucket.org/wanding/clinsek/downloads/clinvar_20140430.sites.uniq.T200.hs37.hscan)

### Build your targets

ClinSeK takes target site list of the simple format (see prebuilt target list for more examples):
```
#!text
chr1:12345
chr2:23456
...
```
For calling mutations from raw reads, we recommend doing a paralogy scanning before running ClinSeK. This can be done through subcommand (see usage for details):
```
#!bash
$ clinsek homoscan -i input_list -o output_list -r ~/hs37d5.fa
```
This inserts paralogous sites into the target list, e.g.,
```
#!text
[H] chr3:12345
```

### Usage

#### targeted SNP/indel calling from FASTQ

```
#!bash
clinsek tcall -s sites.scan.txt -r hg19.fa -o prefix 
              -1 reads/*R1*.fq.gz -2 reads/*R2*.fq.gz
```
 + input: 1) homologous scanned target sites; 2) reference assembly (hg19.fa); 3) short reads in FASTQ
 + output: 1) tumor.bam (with bai); 2) prefix.bcf

ClinSeK tcall calls variants from fastq (either .gz compressed or not) reads given a list of target sites. Output includes a bcf file (compressed vcf) and a bam (compressed sam) file (sorted and indexed) containing the alignment of reads around the target sites. Only sites that are variant will be selected for output.

```
#!bash
clinsek tcall [options] -s sitelist -r ref.fa
              -1 p1.fq [p1.fq [...]] -2 p2.fq [p2.fq [...]]
```
 + input options:

```
#!text
    -s site list
    -r reference genome in fasta format (faidx indexed)
    -1 fastq file(s) (1st in pair)
    -2 fastq file(s) (2nd in pair). The i-th read is the mate of the i-th read fed by option -1.
    -o output file prefix
    -l site filter file to be output [optional]
    -c estimate for contamination
    -e estimate for sequencing error
    -m estimate for mutation rate
    -q minimum mapping quality in SNP calling
    -b minimum base quality in SNP calling
    -d base quality threshold in preliminary calling
    -a toggle to turn OFF indel re-alignment
    -k toggle to turn OFF duplicate marking
```

#### targeted somatic SNV/indel calling from FASTQ

```
#!bash
clinsek scall -s sites.scan.txt -r hg19.fa -o prefix
              -1 tumor/*R1*.fq.gz -2 tumor/*R2*.fq.gz 
              -3 normal/*R1*.fq.gz -4 normal/*R2*.fq.gz
```
 + input: 1) reference genome (hg19.fa); 2) tumor reads; 3) normal reads.

 + output: 1) prefix.tumor.bam (with bai); 2) prefix.normal.bam (with bai); 3) prefix.bcf

 + description: ClinSeK scall calls somatic mutations from fastq reads (tumor and matched normal) given a list of target sites. Only sites that are potentially variant in the tumor sample will be selected.

```
#!bash
clinsek scall [options] -s sitelist -r ref.fa
              -1 tumor_pair1.fq [tumor_pair1.fq [...]]
              -2 tumor_pair2.fq [tumor_pair2.fq [...]]
              -3 normal_pair1.fq [normal_pair1.fq [...]]
              -4 normal_pair2.fq [normal_pair2.fq [...]]
```

 + input options:

```
#!text
    -s site list
    -r reference genome in fasta format (faidx indexed)
    -1 fastq file(s) (1st in pair, tumor sample)
    -2 fastq file(s) (2nd in pair, tumor sample). The i-th read is the mate of the i-th read fed by -1.
    -3 fastq file(s) (1st in pair, normal sample)
    -4 fastq file(s) (2nd in pair, tumor sample). The i-th read is the mate of the i-th read fed by -3.
    -o output file prefix
    -l site filter file to be output [optional]
    -c estimate for contamination
    -e estimate for sequencing error
    -m estimate for mutation rate
    -q minimum mapping quality in SNP calling
    -b minimum base quality in SNP calling
    -d base quality threshold in preliminary calling
    -a toggle to turn OFF indel re-alignment
    -k toggle to turn OFF duplicate marking
```

#### targeted SNP/indel calling from alignment

```
#!bash
clinsek tpileup -s sites.txt -i in.bam -r hg19.fa -o out.bcf
```
 + input: in.bam

 + output: out.bcf

 + description: targeted variant calling a given list of sites and a bam file.

```
#!bash
clinsek tpileup [options] -s sitelist -r ref.fa -b aln.bam -o out.bcf
```
 + input options:
```
#!text
     -s FILE   site list [required]
     -r FILE   reference in fasta format [required]
     -i FILE   indexed bam file [required]
     -o STR    file name for bcf output [tmp]
     -c FLOAT  estimate for contamination rate [0.01]
     -e FLOAT  estimate for sequencing error rate [1e-3]
     -m FLOAT  estimate for mutation rate [1e-3]
     -q INT    minimum mapping quality in variant calling [10]
     -b INT    minimum base quality in SNP calling [10]
     -d INT    base quality threshold in preliminary call [20]
```

#### targeted somatic SNP/indel calling from alignment
```
#!bash
clinsek spileup -s sites.txt -r hg19.fa
                -t tumor.bam -n normal.bam -o out.bcf
```
Output: 1) out.bcf

Targeted somatic mutation calling a given list of sites and a pair of tumor and normal bam files. This can be applied to any alignment in bam format.

```
#!bash
clinsek spileup [options] -s sitelist -r ref.fa -t tumor.bam -n normal.bam -o out.bcf
```
Input options:
```
#!text
     -s FILE   site list [required]
     -r FILE   reference in fasta format [required]
     -t FILE   indexed tumor bam file [required]
     -n FILE   indexed normal bam file [required]
     -o STR    file name for bcf output [tmp.bcf]
     -c FLOAT  estimate of contamination rate [0.01]
     -e FLOAT  estimate of sequencing error rate [1e-3]
     -m FLOAT  estimate of mutation rate [1e-3]
     -u FLOAT  estimate of somatic mutation rate [1e-4]
     -q INT    minimum mapping quality in variant calling [10]
     -b INT    minimum base quality in SNP calling [10]
     -d INT    base quality threshold in preliminary call [20]
     -l        site filter
```

#### paralogy scanning

```
#!bash
clinsek homoscan -s sites.txt -o sites.scan.txt -r hg19.fa
```

 + input: 1) sites.txt; 

 + output: 1) sites.scan.txt;

#### duplicate read/insert marking
```
#!bash
clinsek mkdup -i in.bam -o out.bam
```
Output: 1) out.bam

ClinSeK mkdup marks duplicate inserts in a bam file. For two inserts to be duplicate, it is required that 1) the genomic coordinates of the two reads are in concordance; 2) The two reads have the same alignments (cigar string); and 3) High quality bases in the two inserts accord; The function has feature of: 1) marking paired-end and single-end reads simultaneously; 2) handling cross-chromosomal mate reads; 3) an optional read quality merging procedure so that the base quality of the output is the maximum of base qualities of all the duplicate reads; 4) an option to leave the reads in the bam or not.
```
#!bash
clinsek mkdup [options] -i in.bam -o out.bam
```

Input options

```
#!text
     -i input bam (sorted and indexed)
     -o output bam [input+.mkdup]
     -b minimum base quality to be considered high quality base [10]
     -r toggle to remove marked duplicate in the output
     -m toggle to maximize quality strings for identical reads
     -s toggle to turn OFF sorting after marking duplicates
```

#### targeted indel re-alignment
```
#!bash
clinsek realn -s sites.txt -r hg19.fa -i in.bam -o out.bam
```
Output: 1) out.bam

Investigate reads aligned around a list of target sites and re-align all these reads if at least one read suggest the presence of an indel. Only indels that neighbor any target site are considered. The alternative allele is discovered and reconstructed automatically. The new cigar is recovered from alignment against the best allele, reference or alternative. Alternative alignment is favored if the alignment score is greater than the reference alignment score by [gap open penalty] - [mismatch penalty].

```
#!bash
clinsek realn [options] -i in.bam -o out.bam -s sitelist -r hg19.fa
```
Input options:

```
#!text
     -i FILE   input bam [required]
     -s FILE   site list [required]
     -r FILE   reference file [required]
     -o FILE   output bam [input+.realn]
     -q INT    mininum mapping quality in suggesting indel [20]
     -w INT    width of flanking region extended from the indels in forming allele window [200]
     -m INT    maximum number of indels to allow in the alternative allele [2]
     -n INT    minimum reads support for candidate indel [2]
     -t        toggle to turn OFF sorting and indexing after re-alignment
     -f INT    span such that indels within the span of any target site is considered [20]
     -g        toggle to output only target region in the bam file
     -l        site filter
```

#### breakpoint/junction typing
```
#!bash
clinsek bcall -s allele_list.txt -f in.fq
```

Call differential allele support for reference and alternative allele (for detecting, for example, an SV breakpoint or an RNA-seq junction).

```
#!bash
clinsek bcall [options] -s allele_list -1 in1.fq [in1.fq [...]] -2 in2.fq [in2.fq [...]]
```

Input options:
```
#!text
     -s FILE   list of reference and alternative alleles [REQUIRED]
     -1 FILES  space-separated list of fastq (1st in pair) [REQUIRED]
     -2 FILES  space-separated list of fastq (2nd in pair) [REQUIRED]
     -o STR    output file prefix [test].
```


> Demo: gene fusions detectable through ClinSeK
> Given a reference and alternative assembly of breakpoint sequence from BCR-ABL1 and PML-RARA fusion, `clinsek bcall` is able to detect fusion events from TCGA RNA-seq samples in less than 10 mins per sample.

-----


### Reference

Under submission

### About

This work is a collaboration between Wanding Zhou, Zechen Chong, Hao Zhao and Prof. Ken Chen at University of Texas at MD Anderson Cancer Center.

This work was supported by the Sheikh Khalifa Bin Zayed Al Nahyan Institute for Personalized Cancer Therapy (IPCT), NCI R01 CA172652 (to KC), U01 CA180964, NCATS UL1 TR000371, the MD Anderson Cancer Center Support grant (NIH/NCI P30 CA016672), the MD Anderson Odyssey recruitment fellowship (to WZ) and the MD Anderson Moon Shot Program.
