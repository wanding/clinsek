#ifndef _WZ_SI_BUFFER_H_
#define _WZ_SI_BUFFER_H_

#include <stdint.h>
#include <unistd.h>
#include "site.h"
#include "utils.h"

typedef struct VarSI_H {
  uint8_t  tiv:1;
  uint32_t pos;
  uint8_t  str:1;
  uint8_t  n_cigar:7;
  uint16_t cigar[4];	/* store at most 4 cigars, otherwise re-align */
  uint8_t  css;
  uint8_t  cse;
} __attribute__((__packed__)) VarSI_H;

/* Variant Site Insert mapping
 * altogether 8+(4+1+8+2)+(4+1+8+2) = 38 bytes
 * cigar is 16 bits, high 12 bits 2^12=4096 long,
 * should be enough for most SNP/indel applications.
 * */
typedef struct VarSI {

  uint64_t ins_id:37;
  uint32_t site_id:25;

  VarSI_H h1;
  VarSI_H h2;

} __attribute__((__packed__)) VarSI;

/* count of variant base quality */
typedef struct {
  uint32_t A;
  uint32_t T;
  uint32_t G;
  uint32_t C;
  uint32_t O;			/* other counts, INDEL etc. */
} __attribute__((__packed__)) VarBQ;

DEFINE_VECTOR(VarSIList, VarSI);

typedef struct {
  VarSIList *buffer;
  size_t pos;
  size_t cap;
  char *dump_fn;
  FILE *FH;
} VarSIBuffer;

/* 4M*34=136M capacity, after 136M dump to file 
 */
static inline VarSIBuffer* 
init_VarSIBuffer(char *dump_fn) {
  VarSIBuffer *vb = (VarSIBuffer*) malloc(sizeof(VarSIBuffer));
  vb->cap=0x400000;
  vb->buffer=init_VarSIList(0x10000);
  unlink(dump_fn);
  vb->dump_fn = strdup(dump_fn);
  vb->FH=NULL;
  return vb;
}

static inline void
check_dump_VarSIBuffer(VarSIBuffer *vb) {
  if (vb->buffer->size > vb->cap) {
    vb->FH = wz_fopen(__func__, vb->dump_fn, "ab");
    fwrite(vb->buffer->buffer, sizeof(VarSI), vb->buffer->size, vb->FH);
    clear_VarSIList(vb->buffer);
    wz_pfclose(&vb->FH);
  }
}

static inline VarSI *
backtrace_VarSIBuffer(VarSIBuffer *vb, uint64_t ins_id, Site *st) {
  VarSI *v;
  for (v=vb->buffer->buffer+vb->buffer->size-1; 
       v>=vb->buffer->buffer && v->ins_id==ins_id; --v) {
    if (v->site_id == st->id) return v;
  }
  return 0;
}

static inline void
destroy_VarSIBuffer(VarSIBuffer *vb) {
  wz_pfclose(&vb->FH);
  unlink(vb->dump_fn);		/* does not check sucess, just delete */
  free_VarSIList(vb->buffer);
  free(vb->dump_fn);
  free(vb);
}

static inline void
rewind_VarSIBuffer(VarSIBuffer *vb) {
  if (access(vb->dump_fn, F_OK) == 0) {
    /* dump the rest of the data in memory to the dump file */
    vb->FH = wz_fopen(__func__, vb->dump_fn, "ab");
    fwrite(vb->buffer->buffer, sizeof(VarSI), vb->buffer->size, vb->FH);
    wz_pfclose(&vb->FH);

    /* clear the memory and start reading */
    vb->FH = wz_fopen(__func__, vb->dump_fn, "rb");
    clear_VarSIList(vb->buffer);
    int m = fread(vb->buffer->buffer, sizeof(VarSI), vb->cap, vb->FH);
    if (m>0) {
      vb->buffer->size = m;
      vb->pos = 0;
    }
    return;
  } else {
    vb->pos = 0;
    vb->FH = 0;
    return;
  }
}

static inline VarSI*
read_VarSIBuffer(VarSIBuffer *vb) {
  if (vb->pos < vb->buffer->size) {
    return ref_VarSIList(vb->buffer, vb->pos++);
  } else if (vb->FH) {
    int m = fread(vb->buffer->buffer, sizeof(VarSI), vb->cap, vb->FH);
    if (m>0) {
      vb->buffer->size=m;
      vb->pos = 1;
      return ref_VarSIList(vb->buffer, 0);
    } else return NULL;
  } else return NULL;
}

typedef struct RefSI_H {

  uint8_t  str:1;
  uint32_t pos;
  uint8_t  css:4;		/* soft-clips from left */
  uint8_t  cm;
  uint8_t  cse:4;		/* soft-clips from right */

} __attribute__((__packed__)) RefSI_H;

/* 20 bytes */
typedef struct RefSI {

  uint64_t ins_id:36;
  uint32_t site_id:26;

  RefSI_H h1;
  RefSI_H h2;

} __attribute__((__packed__)) RefSI;

DEFINE_VECTOR(RefSIList, RefSI);

/* pos: currently reading RefSI */
typedef struct {
  RefSIList *buffer;
  size_t pos;
  size_t cap;
  char *dump_fn;	/* the struct is responsible for freeing this */
  FILE *FH;
} RefSIBuffer;

/* 16M*16=256M capacity, after 256M dump to file 
 */
static inline RefSIBuffer*
init_RefSIBuffer(char *dump_fn) {
  RefSIBuffer *rb = (RefSIBuffer*) malloc(sizeof(RefSIBuffer));
  rb->cap=0x1000000;
  rb->buffer=init_RefSIList(0x10000);
  unlink(dump_fn);
  rb->dump_fn = strdup(dump_fn);
  rb->FH=NULL;
  return rb;
}

static inline RefSI *
backtrace_RefSIBuffer(RefSIBuffer *rb, uint64_t ins_id, Site *st) {
  RefSI *r;
  for (r = rb->buffer->buffer + rb->buffer->size - 1; 
       r >= rb->buffer->buffer && r->ins_id == ins_id; --r) {
    if (r->site_id == st->id) return r;
  }

  return 0;
}

static inline void
check_dump_RefSIBuffer(RefSIBuffer *rb) {
  if (rb->buffer->size+2>rb->cap) {
    rb->FH = wz_fopen(__func__, rb->dump_fn, "ab");
    fwrite(rb->buffer->buffer, sizeof(RefSI), rb->buffer->size, rb->FH);
    clear_RefSIList(rb->buffer);
    wz_pfclose(&rb->FH);
  }
}

static inline void
destroy_RefSIBuffer(RefSIBuffer *rb) {
  wz_pfclose(&rb->FH);
  unlink(rb->dump_fn);		/* does not check sucess, just delete */
  free_RefSIList(rb->buffer);
  free(rb->dump_fn);
  free(rb);
}

static inline void
rewind_RefSIBuffer(RefSIBuffer *rb) {
  if (access(rb->dump_fn, F_OK) == 0) {
    /* dump the rest of the data in memory to the dump file */
    rb->FH = wz_fopen(__func__, rb->dump_fn, "ab");
    fwrite(rb->buffer->buffer, sizeof(RefSI), rb->buffer->size, rb->FH);
    wz_pfclose(&rb->FH);

    /* clear the memory and start reading */
    rb->FH = wz_fopen(__func__, rb->dump_fn, "rb");
    clear_RefSIList(rb->buffer);
    int m = fread(rb->buffer->buffer, sizeof(RefSI), rb->cap, rb->FH);
    if (m>0) {
      rb->buffer->size = m;
      rb->pos = 0;
    }
    return;
  } else {
    rb->pos = 0;
    rb->FH = 0;
    return;
  }
}

static inline RefSI*
read_RefSIBuffer(RefSIBuffer *rb) {
  if (rb->pos < rb->buffer->size) {
    return ref_RefSIList(rb->buffer, rb->pos++);
  } else if (rb->FH) {
    int m=fread(rb->buffer->buffer, sizeof(RefSI), rb->cap, rb->FH);
    if (m>0) {
      rb->buffer->size=m;
      rb->pos = 1;
      return ref_RefSIList(rb->buffer, 0);
    } else return NULL;
  } else return NULL;
}

#endif /* _WZ_SI_BUFFER_H_ */
