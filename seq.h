#ifndef _WZ_SEQ_H
#define _WZ_SEQ_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "wvec.h"

#define PFIX_SIZE 0x100000
#define KLEN 25			/* seed length */
#define SEED_EXT_MAX_MISMATCH 2	/* maximal number of mismatch in seed extension */
/* for a 25-mer with no "N",
 * it takes 50 bits and 7 bytes.
 * The higher 20 bits (3 bytes) are for hashing,
 * The lower 30 bits (4 bytes) are for chaining */
typedef struct {
  uint32_t d;
  void *data;
} __attribute__((__packed__)) Suffix;

DEFINE_VECTOR(SufList, Suffix);

typedef struct {
  SufList *arr[PFIX_SIZE];
} SeqHash;

extern const uint32_t nt16_to_nt4_d_table[256];
extern const uint8_t nt16_rev_d_table[256];
extern const uint8_t char_to_nt4_table[128];

SeqHash* init_SeqHash();

Suffix* insert_SeqHash(SeqHash *sh, char *s);
Suffix* get_SeqHash(SeqHash *sh, uint32_t p, uint32_t d);

Suffix* insert_SeqHash2(SeqHash *sh, uint8_t *seq_nt16);
Suffix* get_SeqHash2(SeqHash *sh, uint8_t *seq_nt16);

size_t count_SeqHash(SeqHash *sh);
void destroy_SeqHash(SeqHash *sh, void(*free_data)(void*));

#endif
