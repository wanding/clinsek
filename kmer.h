#ifndef _KMER_H
#define _KMER_H
#include "encode.h"
#include "wvec.h"

/* #define KLEN 25 */
/* #define KBLEN 7 */
/* #define OFFSET 150 */

/* each Kmer is 20 byte */
typedef struct {
  void *site;
  int spos;			/* k-mer's location w/r site */
  uint32_t homohits;
  uint8_t str;			/* TODO: delete this field! */
} __attribute__((__packed__)) Kmer;

DEFINE_NATIVE_VECTOR(KmerList, Kmer*)

static inline void destroy_KmerList(KmerList *kl) {
  size_t i;
  for (i=0; i<kl->size; ++i) free(get_KmerList(kl, i));
  free_KmerList(kl);
}

typedef struct Seq {
  Kmer *k;
  uint8_t str;
} __attribute__((__packed__)) Seq;

DEFINE_VECTOR(SeqList, Seq)

static inline void _copy_subseq(uint8_t *dest, uint8_t *src, size_t pos, size_t len, size_t blen) {

  uint8_t *d=dest;
  uint8_t *s=src+(pos>>1);
  size_t l=(len>>1);
  if (pos & 1) {
    for (;l--;++d,++s) *d = ((*s)<<4) | (*(s+1)>>4);
    if (len & 1) *d = *s<<4;
  } else {
    memcpy(dest, s, blen);
    if (len & 1) dest[blen-1] &= 0xF0;
  }
}

static inline void _decode_subseq(char *seq_nt256char, const uint8_t *seq_nt16, size_t pos, size_t len) {
  size_t i, j;
  for (i=0, j=pos; i<len; ++i, ++j) {
    seq_nt256char[i] = nt16_to_nt256char_table[(uint8_t) (seq_nt16[j>>1] << ((j&1)<<2))];
  }
}
static inline char* decode_subseq(const uint8_t *seq_nt16, size_t pos, size_t len) {
  char *seq_nt256char = (char*) malloc(len);
  _decode_subseq(seq_nt256char, seq_nt16, pos, len);
  return seq_nt256char;
}

static inline char* decode_cstr(const uint8_t *seq_nt16, size_t pos, size_t len) {
  char *seq_nt256char = (char*) malloc(len+1);
  _decode_subseq(seq_nt256char, seq_nt16, pos, len);
  seq_nt256char[len]=0;
  return seq_nt256char;
}

#endif
