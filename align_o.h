#ifndef _WZ_ALIGN_H
#define _WZ_ALIGN_H

void bam_standardize(const char *bam_in, const char *bam_out);

#define S_MATCH 2
#define S_MISMATCH 2
#define S_GAPOPEN 3
#define S_GAPEXT 1
#define S_NOISE 20

typedef struct {
  s_align *read1_align;
  uint8_t read1_strand;
  s_align *read2_align;
  uint8_t read2_strand;
} InsertAlign;

static inline void free_InsertAlign(InsertAlign *ia) {
  free_align(ia->read1_align);
  free_align(ia->read2_align);
  free(ia);
}

#endif

