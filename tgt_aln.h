#ifndef _TGT_ALN_H_
#define _TGT_ALN_H_

#include <time.h>
#include <math.h>
#include "si_buffer.h"
#include "site.h"
#include "samtools/bam.h"
#include "bam_std.h"
#include "tgt_aln.h"
#include "utils.h"
#include "rkgen.h"
#include "aln.h"

typedef struct {
  uint32_t pos;
  uint8_t str:1;
  uint32_t *cigar;
  uint8_t n_cigar;
  uint32_t mutQ;		/* mutation quality */
} __attribute__((__packed__)) bam_aln_t;


typedef struct {
  uint64_t ins_id:36;
  uint32_t site_id:28;
  uint8_t chrm;
  bam_aln_t a1;
  bam_aln_t a2;
} __attribute__((__packed__)) bam1s_t;

static inline int bam_aln_equal(bam1s_t *b1, bam1s_t *b2) {
  if (b1->chrm == b2->chrm && 
      b1->a1.n_cigar == b2->a1.n_cigar && 
      b1->a2.n_cigar == b2->a2.n_cigar && 
      (b1->a1.n_cigar == 0 || b1->a1.pos == b2->a1.pos) && 
      (b1->a2.n_cigar == 0 || b1->a2.pos == b2->a2.pos)) return 0;
  else return 1;
}

DEFINE_VECTOR(BamList, bam1s_t);
DEFINE_VECTOR(BamPList, bam1s_t*);

typedef struct {
  SeqHash *sh;
  KmerList *kl;
} WideHash;

typedef struct {
  int8_t sf[1000];		/* forward */
  int8_t sr[1000];		/* reverse */
  uint8_t up;		/* whether the swseq is upto date */
} swseq_t;

static inline void swseq_update(swseq_t *ss, kseq_t *ksq, uint32_t rlen) {
  if (!ss->up) {
    _nt256char_encode_nt256int8(ss->sf, ksq->seq.s, rlen);
    _nt256int8_rev(ss->sr, ss->sf, rlen);
    ss->up = 1;
  }
}

typedef struct {
  uint8_t str;
  uint32_t score;
  size_t begin;
  size_t end;
  size_t n_cigar;
  uint32_t cigar[100];
} __attribute__((__packed__)) Align;

DEFINE_VECTOR(AlignList, Align);

/* tolerate two mismatches from seed extension */
/* #define MIST 2 */

void fix_mate(SeqHash *sh, Site *st, bam_aln_t *ba, RKGen *rk, SSPool *ssp, CallConf *conf);

bamFile bam_prepare_aln(char *bam_tmp_fn, TargetList *tl);
void bam_format_aln(BamPList *vb, bamFile bam, bam1_t *b1, kseq_t *seq1, kseq_t *seq2);
void type_mutQ(bam_aln_t *ba, char *rs, size_t tpos, RKGen *rk);

void insert_aln_hsite(SeqHash *sh, HomoSite *hst, bam_aln_t *ba, RKGen *rk, CallConf *conf);
void global_aln_hsite (HomoSite *hst, bam_aln_t *ba, RKGen *rk, uint8_t mate_str, CallConf *conf);

void tgt_aln (KCnts *kc, char **fq_files1, char **fq_files2, char *bam_fn, VarSIBuffer *vb, RefSIBuffer *rb, uint8_t *siv);
void tgt_aln_batch (KCnts *kc, char **fq_files1, char **fq_files2, bamFile bam, VarSIBuffer *vsi, RefSIBuffer *rsi, uint8_t *siv1);


void swaln_cpy_cigar(bam_aln_t *ba, s_align *a, uint32_t rlen);
void swaln_cpy_cigar2(Align *al, s_align *a, uint32_t rlen);
void copy_best_aln(bam_aln_t *ba, AlignList *alist, uint32_t st_pos);
void seedext_set_cigar(Align *al, uint32_t rlen, uint8_t cs1, uint8_t cs2);


#endif /* _TGT_ALN_H_ */
