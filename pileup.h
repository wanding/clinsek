/* 
The MIT License

Copyright (c) 2015
The University of Texas MD Anderson Cancer Center
Wanding Zhou, Ken Chen (kchen3@mdanderson.org)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef _WZ_PILEUP_H_
#define _WZ_PILEUP_H_

#include "kcnts.h"
#include "wvec.h"
#include "bcftools/bcf.h"
#include "samtools/bam.h"
#include "samtools/khash.h"
#include "samtools/klist.h"
#include "samtools/kstring.h"
#include "samtools/sam_header.h"
#include "stats.h"
#include "utils.h"
#include "read_pool.h"

#define TSFLEN 1000 		/* target site flanking region length */
#define bscall(seq, pos) bam_nt16_rev_table[bam1_seqi(seq, pos)]

/* 
 * recorded: for tracking if an allele has been recorded in bcf
 * this is important for long indel that span multiple target sites
 *
 * obsolete: whether the slot is obsolete
 * Note that the size of AlleleList is not necssarily equal to
 * the number of alleles. The number of non-obsolete alleles is. 
 * It is reset in init_pileup_allele for each target site.
 * 
 */

typedef struct {
  uint32_t cigar; /* cigar can only take BAM_CEQUAL, BAM_CDIFF, BAM_CINS, BAM_CDEL, only reference is BAM_CEQUAL */
  int tid;
  uint32_t rpos;	/* start position of the alt on the reference */
  /* seq is inserted/deleted sequence if indel */
  char *seq;			/* NOT 0-terminated */
  int len;			/* length of seq */
  uint32_t ic; /* number of supporting inserts */
  uint32_t rc; /* number of supporting reads */
  uint8_t recorded:1;		/* whether recorded in bcf */
} __attribute__((__packed__)) Allele;

DEFINE_VECTOR(AlleleList, Allele)
DEFINE_VECTOR(AllelePList, Allele*)

static inline void
destroy_AlleleList(AlleleList *al) {
  uint32_t i;
  for (i=0; i<al->size; ++i) {
    Allele *a = ref_AlleleList(al, i);
    if (a->seq) free(a->seq);
  }
  free_AlleleList(al);
}

static inline void
destroy_AllelePList(AllelePList *al) {
  uint32_t i;
  for (i=0; i<al->size; ++i) {
    Allele *a = get_AllelePList(al, i);
    if (a->seq) free(a->seq);
  }
  free_AllelePList(al);
}

static inline void
clean_AlleleList(AlleleList *al) {
  uint32_t i;
  for (i=0; i<al->size; ++i) {
    Allele *a = ref_AlleleList(al, i);
    if (a->seq) free(a->seq);
  }
  clear_AlleleList(al);
}

static inline void
copy_allele(Allele *dst, Allele *src) {
  *dst = *src;
  if (src->seq) {
    dst->seq = malloc(src->len * sizeof(char));
    memcpy(dst->seq, src->seq, src->len * sizeof(char));
  }
}

/* /\* does not reset seq and obsolete *\/ */
/* static inline void reset_allele(Allele *a) { */
/*   a->cigar    = 0; */
/*   a->tid      = 0; */
/*   a->rpos     = 0; */
/*   a->len      = 0; */
/*   a->ic	      = 0; */
/*   a->rc	      = 0; */
/*   a->recorded = 0; */
/* } */

/* static inline Allele *next_obsolete_AlleleList(AlleleList *al) { */
/*   uint32_t i; */
/*   for (i=0; i<al->size; ++i) { */
/*     Allele *a = ref_AlleleList(al, i); */
/*     if (a->obsolete) { */
/*       reset_allele(a); */
/*       return a; */
/*     } */
/*   } */
/*   Allele *a = next_ref_AlleleList(al); */
/*   reset_allele(a); */
/*   a->seq = 0; */
/*   a->obsolete = 1; */

/*   return a; */
/* } */

static inline uint32_t max_alt_ic(AllelePList *alts) {
  uint32_t i;
  uint32_t maxic = 0;
  for (i=0; i<alts->size; ++i) {
    Allele *a = get_AllelePList(alts, i);
    if (a->ic > maxic) maxic = a->ic;
  }
  return maxic;
}

static inline int check_remain(AllelePList *al) {
  uint32_t i;
  for (i=0; i<al->size; ++i) {
    Allele *a = get_AllelePList(al, i);
    if (!a->recorded) return 1;
  }
  return 0;
}

/* two mutations are in the same category if
   they are both SNV at the same position */
static inline
int same_category(Allele *a1, Allele *a2) {
  if (a1->rpos == a2->rpos &&
      a1->cigar == a2->cigar &&
      a1->cigar == BAM_CDIFF &&
      a1->len == a2->len &&
      a1->len == 1) return 1;
  return 0;
}

static inline int
cmp_allele(const void *p1, const void *p2) {
  Allele *a1 = *((Allele**)p1);
  Allele *a2 = *((Allele**)p2);
  return a2->rc - a1->rc;
}

/* for sorting alternative alleles in VCF reports,
   better represented allele comes first */
static inline int
cmp_allelep_by_ic(const void *p1, const void *p2) {
  return (*(Allele**)p2)->ic - (*(Allele**)p1)->ic;
}

static inline uint8_t
allele_equal(Allele *a1, Allele *a2) {
  if (a1 == a2) return 1;
  if (a1 == NULL || a2 == NULL) return 0;
  if ((a1->rpos == a2->rpos) &&
      (a1->cigar == a2->cigar) &&
      (a1->len == a2->len) &&
      (memcmp(a1->seq, a2->seq, a1->len)==0)) return 1;
  return 0;
}


static inline int
cmp_allele_sort(const void *p1, const void *p2) {
  return ((Allele*) p1)->rpos - ((Allele*) p2)->rpos;
}

static inline int
cmp_allele_by_pos(const void *p1, const void *p2) {
  Allele *a1 = *((Allele**)p1);
  Allele *a2 = *((Allele**)p2);
  return a1->rpos - a2->rpos;
}

/* return 0 if allele exist and 
   is recorded at other location */
static inline Allele*
insert_allele(AllelePList *al, Allele *t) {
  uint32_t i;
  for (i=0; i<al->size; ++i) {
    Allele *a = get_AllelePList(al, i);
    if (allele_equal(a, t)) {
      if (t->seq) free(t->seq);
      free(t);
      if (a->recorded) return NULL;
      else return a;
    }
  }
  push_AllelePList(al, t);
  return t;
}

/* read and insert
 * 
 * squal:
 * site quality based on average base quality
 * insertion: average of inserted sequence
 * deletion: average of the base before and the base after
 ***/
typedef struct {
  bam1_t *r;
  Allele *a;
  uint32_t qpos;
  double squal;
} ReadAlign;

typedef struct {
  Allele *a;
  double squal;	  /* site quality, inferred from the two member reads */
  ReadAlign a1;
  ReadAlign a2;
} Insert;


DEFINE_VECTOR(InsertList, Insert*)

/* reference */

typedef struct {
  char *seq;
  uint32_t beg;
  uint32_t len;
} PlpRefData;

static inline void bind_site_reference(PlpRefData *rf, Site *st, RefSeq *rs) {
  rf->beg = st->pos-TSFLEN;
  rf->len = TSFLEN*2+1;
  free(rf->seq);  /* do nothing if pd->rseq == NULL */
  rf->seq = fetch_RefSeq(rs, st->chrm, rf->beg, rf->len);
}

/* read */

KHASH_MAP_INIT_STR(name, Insert*)
#define __plp_free_insert(key)	/* no-op */
KMEMPOOL_INIT(Insert, Insert, __plp_free_insert)

typedef struct {
  khash_t(name) *kh;
  kmempool_t(Insert) *imp;
  kmempool_t(Read) *rmp;
} PlpReadData;

static inline void prepare_pileup_read(PlpReadData *rd) {
  rd->kh  = kh_init(name);
  rd->imp = kmp_init(Insert);
  rd->rmp = kmp_init(Read);
}

static inline void clean_pileup_read(PlpReadData *rd) {
  kh_destroy(name, rd->kh);
  kmp_destroy(Read, rd->rmp);
  kmp_destroy(Insert, rd->imp);
}

/* free insert list to memory pool */
static inline void destroy_InsertList(InsertList *insl, PlpReadData *rd) {
  
  uint32_t i;
  for (i=0; i<insl->size; ++i) {
    Insert *ins = get_InsertList(insl, i);
    if (ins->a1.r) kmp_free(Read, rd->rmp, ins->a1.r);
    if (ins->a2.r) kmp_free(Read, rd->rmp, ins->a2.r);
    kmp_free(Insert, rd->imp, ins);
  }
  free_InsertList(insl);
}

/* bam */

typedef struct {
  char *bam_fn;
  bamFile fp;
  bam_index_t *idx;
} PlpBamData;


static inline void prepare_pileup_bam(PlpBamData *bam, char *bam_fn) {
  bam->bam_fn = bam_fn;
  bam->fp = bam_open(bam_fn, "r");
  bam->idx = bam_index_load(bam_fn);
}

static inline void clean_pileup_bam(PlpBamData *bam) {
  bam_close(bam->fp);
  bam_index_destroy(bam->idx);
}

/* bcf */

typedef struct {
  char *bcf_fn;
  bcf_t *bp;
  bcf_hdr_t *bh;
} PlpBCFData;

void prepare_pileup_bcf(char *bcf_fn, char *ref_fn, PlpBamData *bam, PlpBCFData *bcf, char *info_str, char *fmt_str);

static inline void clean_pileup_bcf(PlpBCFData *bcf) {
  bcf_close(bcf->bp);
  bcf_hdr_destroy(bcf->bh);
}

/* allele
 * al is never really cleared.
 * Only Alleles are set to obsolete.
 * This is to reduce cross-sites redundancies in the vcf.
 */
typedef struct {
  Allele ref;
  AllelePList *al;
  uint32_t dp; 			/* read depth */
  uint32_t an;			/* insert depth */
} PlpAlleleData;

static inline void prepare_pileup_allele(PlpAlleleData *ad) {
  ad->al = init_AllelePList(10);
}

#define absdiff(x,y) ((x>y)?(x-y):(y-x))

static inline void
init_pileup_allele(PlpAlleleData *ad, PlpRefData *refseq, Site *st) {

  /* update alternative allele list */
  uint32_t i;
  AllelePList *al2 = init_AllelePList(2);
  for (i=0; i<ad->al->size; ++i) {
    Allele *a = get_AllelePList(ad->al, i);
    if (absdiff(a->rpos, st->pos) > 10000) {
      if (a->seq) free(a->seq);
      free(a);
    } else {
      push_AllelePList(al2, a);
    }
  }
  free_AllelePList(ad->al);
  ad->al = al2;

  /*   uint32_t i; */
  /*   for (i=0; i<ad->al->size; ++i) { */
  /*     Allele *a = ref_AlleleList(ad->al, i); */
  /*     /\* if allele is too far away from the current site, */
  /*        set obsolete to 1 *\/ */
  /*     if (abs(a->rpos-st->pos) > 10000) a->obsolete = 1; */
  /*   } */

  /* reset reference allele */
  ad->ref.tid = st->chrm;
  ad->ref.rpos = st->pos;
  ad->ref.cigar = BAM_CEQUAL;	/* only reference is BAM_CEQUAL */
  ad->ref.ic = 0;
  ad->ref.rc = 0;
  ad->ref.seq = refseq->seq+TSFLEN;
  ad->ref.len = 1;

  /* initiate coverage */
  ad->dp = 0;
  ad->an = 0;
}

static inline void
clean_pileup_allele(PlpAlleleData *ad) {
  destroy_AllelePList(ad->al);
  /* free_AllelePList(ad->majors); */
}

void get_coverage(PlpAlleleData *ad, uint32_t *dp, uint32_t *an);

/* Pileup working functions */

InsertList *site_fetch_inserts(Site *st, PlpBamData *bam, PlpReadData *rd, uint32_t min_mapQ);

/* void type_major(PlpAlleleData *ad, InsertList *insl, PlpRefData *refseq, Site *st, uint8_t min_baseQ); */

/* void realign_major(PlpAlleleData *ad, InsertList *insl); */
 
void plp_inserts(PlpAlleleData *ad, InsertList *insl, PlpRefData *refseq, Site *st, uint8_t min_baseQ);

static inline float max_qual(FloatList *quals) {
  uint32_t i;
  float mqual = 0;
  for (i=0; i<quals->size; ++i) {
    float qual = get_FloatList(quals, i);
    if (qual>mqual) mqual = qual;
  }
  return mqual;
}


#endif /* _PILEUP_H_ */
