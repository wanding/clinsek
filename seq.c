#include "seq.h"

const uint32_t nt16_to_nt4_d_table[256] = {
  0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0,
  4, 4, 5, 4, 6, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4,
  0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0,
  8, 8, 9, 8,10, 8, 8, 8,11, 8, 8, 8, 8, 8, 8, 8,
  0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0,
 12,12,13,12,14,12,12,12,15,12,12,12,12,12,12,12,
  0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0
};

const uint8_t nt16_rev_d_table[256] = {
  0,128,64, 0,32, 0, 0, 0,16, 0, 0, 0, 0, 0, 0, 0,
  8,136,72, 8,40, 8, 8, 8,24, 8, 8, 8, 8, 8, 8, 8,
  4,132,68, 4,36, 4, 4, 4,20, 4, 4, 4, 4, 4, 4, 4,
  0,128,64, 0,32, 0, 0, 0,16, 0, 0, 0, 0, 0, 0, 0,
  2,130,66, 2,34, 2, 2, 2,18, 2, 2, 2, 2, 2, 2, 2,
  0,128,64, 0,32, 0, 0, 0,16, 0, 0, 0, 0, 0, 0, 0,
  0,128,64, 0,32, 0, 0, 0,16, 0, 0, 0, 0, 0, 0, 0,
  0,128,64, 0,32, 0, 0, 0,16, 0, 0, 0, 0, 0, 0, 0,
  1,129,65, 1,33, 1, 1, 1,17, 1, 1, 1, 1, 1, 1, 1,
  0,128,64, 0,32, 0, 0, 0,16, 0, 0, 0, 0, 0, 0, 0,
  0,128,64, 0,32, 0, 0, 0,16, 0, 0, 0, 0, 0, 0, 0,
  0,128,64, 0,32, 0, 0, 0,16, 0, 0, 0, 0, 0, 0, 0,
  0,128,64, 0,32, 0, 0, 0,16, 0, 0, 0, 0, 0, 0, 0,
  0,128,64, 0,32, 0, 0, 0,16, 0, 0, 0, 0, 0, 0, 0,
  0,128,64, 0,32, 0, 0, 0,16, 0, 0, 0, 0, 0, 0, 0,
  0,128,64, 0,32, 0, 0, 0,16, 0, 0, 0, 0, 0, 0, 0
};

const uint8_t char_to_nt4_table[128] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,1,0,0,0,2,0,0,0,0,0,0,0,0,
  0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0  
};

SeqHash* init_SeqHash() {
  SeqHash *sh = (SeqHash*) calloc(1,sizeof(SeqHash));
  return sh;
}

/* it's the user's duty to make sure that
 * the inserted sequence contains no 'N' */
Suffix* insert_SeqHash(SeqHash *sh, char *seq) {
  unsigned char *s=(unsigned char*)seq; uint8_t i;
  uint32_t p=0;
  for(i=10;i--;++s) {
    p<<=2;
    p|=char_to_nt4_table[*s];
  }

  uint32_t d=0;
  for(i=15;i--;++s) {
    d<<=2;
    d|=char_to_nt4_table[*s];
  }

  SufList **sl=sh->arr+p;
  Suffix *sf;
  if ((*sl) == NULL) {
    *sl = init_SufList(2);
    sf = next_ref_SufList(*sl);
    sf->d = d;
    sf->data = NULL;
    return sf;
  } else {
    size_t k;
    for (k=0; k<(*sl)->size; ++k) {
      sf = ref_SufList((*sl), k);
      if (sf->d==d) return sf;
    }
    sf = next_ref_SufList((*sl));
    sf->d=d;
    sf->data = NULL;
    return sf;
  }
  return sf;
}

/* if not found return NULL */
Suffix* get_SeqHash(SeqHash *sh, uint32_t p, uint32_t d) {
  SufList *sl=sh->arr[p];
  if (sl == NULL) return NULL;
  else {
    size_t k;
    for (k=0; k<sl->size; ++k) {
      Suffix *sf = ref_SufList(sl, k);
      if (sf->d==d) return sf;
    }
    return NULL;
  }
}

size_t count_SeqHash(SeqHash *sh) {
  size_t i, cnt;
  cnt=0;
  for (i=0; i<PFIX_SIZE; ++i) {
    SufList *sf = sh->arr[i];
    if (sf) cnt += sf->size;
  }
  return cnt;
}

void destroy_SeqHash(SeqHash *sh, void(*free_data)(void*)) {
  size_t i;
  for (i=0; i<PFIX_SIZE; ++i) {
    SufList *sl = sh->arr[i];
    if (sl != NULL) {
      size_t j;
      for (j=0; j<sl->size; ++j) {
	Suffix *s = ref_SufList(sl, j);
	if (s->data) free_data(s->data);
      }
      free_SufList(sl);
    }
  }
  free(sh);
}


#ifdef _TEST_SEQHASH

#include <limits.h>

static inline void bitarr_show(const unsigned char *bitarr, size_t bitarr_len) {
  const unsigned char *byte;
  for ( byte = bitarr; bitarr_len--; ++byte ) {
    unsigned char mask;
    for ( mask = (1 << (CHAR_BIT - 1)); mask; mask >>= 1 ) {
      putchar(mask & *byte ? '1' : '0');
    }
    putchar(' ');
  }
  putchar('\n');
}

int main() {

  char *seq="ATGCGTTCCAAGTCAATGCGTTCCT";
  char *s=seq; uint8_t i;
  uint32_t p=0;
  for(i=10;i--;++s) {
    p<<=2;
    p|=char_to_nt4_table[*s];
  }

  uint32_t d=0;
  for(i=15;i--;++s) {
    d|=char_to_nt4_table[*s];
    d<<=2;
  }

  printf("%u\t%d\n", p,d);
  return 0;
}
#endif

#ifdef _BUILD_LOOKUP_TABLE
int main() {
  
  size_t i;
  puts("\n\nnt16 => nt4 (double)");
  for (i=0; i<256; i++) {
    uint8_t h,l;
    if ((i & 0xF0) == 0x10) h=0;
    else if ((i & 0xF0) == 0x20) h=1;
    else if ((i & 0xF0) == 0x40) h=2;
    else if ((i & 0xF0) == 0x80) h=3;
    else h=0;
    if ((i & 0x0F) == 0x01) l=0;
    else if ((i & 0x0F) == 0x02) l=1;
    else if ((i & 0x0F) == 0x04) l=2;
    else if ((i & 0x0F) == 0x08) l=3;
    else l=0;
    printf("%2u,", ((h<<2)|l));
    if (((i+1) & 0xf) == 0) printf("\n");
  }

  puts("\n\nnt16 rev (double)");
  for (i=0; i<256; i++) {
    uint8_t h,l;
    if ((i & 0xF0) == 0x10) l=0x08;
    else if ((i & 0xF0) == 0x20) l=0x04;
    else if ((i & 0xF0) == 0x40) l=0x02;
    else if ((i & 0xF0) == 0x80) l=0x01;
    else l=0;
    if ((i & 0x0F) == 0x01) h=0x80;
    else if ((i & 0x0F) == 0x02) h=0x40;
    else if ((i & 0x0F) == 0x04) h=0x20;
    else if ((i & 0x0F) == 0x08) h=0x10;
    else h=0;
    printf("%2u,", (h|l));
    if (((i+1) & 0xf) == 0) printf("\n");
  }

  puts("\n\nchar => nt4 1");
  for (i=0; i<128; i++) {
    if (i == 'A')      printf("%d,", 0x00);
    else if (i == 'C') printf("%d,", 0x40);
    else if (i == 'G') printf("%d,", 0x80);
    else if (i == 'T') printf("%d,", 0xC0);
    else printf("%d,", 0x00);
    if (((i+1) & 0xf) == 0) printf("\n");
  }

  puts("\n\nchar => nt4 2");
  for (i=0; i<128; i++) {
    if (i == 'A')      printf("%d,", 0x00);
    else if (i == 'C') printf("%d,", 0x10);
    else if (i == 'G') printf("%d,", 0x20);
    else if (i == 'T') printf("%d,", 0x30);
    else printf("%d,", 0x00);
    if (((i+1) & 0xf) == 0) printf("\n");
  }

  puts("\n\nchar => nt4 3");
  for (i=0; i<128; i++) {
    if (i == 'A')      printf("%d,", 0x00);
    else if (i == 'C') printf("%d,", 0x04);
    else if (i == 'G') printf("%d,", 0x08);
    else if (i == 'T') printf("%d,", 0x0C);
    else printf("%d,", 0x00);
    if (((i+1) & 0xf) == 0) printf("\n");
  }

  puts("\n\nchar => nt4 4");
  for (i=0; i<128; i++) {
    if (i == 'A')      printf("%d,", 0x00);
    else if (i == 'C') printf("%d,", 0x01);
    else if (i == 'G') printf("%d,", 0x02);
    else if (i == 'T') printf("%d,", 0x03);
    else printf("%d,", 0x00);
    if (((i+1) & 0xf) == 0) printf("\n");
  }

}
#endif
