#include "tally.h"

WZ_KSEQ_INIT(gzFile, gzread)

int
tally(KCnts *kc, char **fq_files1, char **fq_files2,
      VarSIBuffer *vb, RefSIBuffer *rb) {

  RKGen *rk = rk_init(KLEN);

  SeqHash *sh = kc->sh;
  CallConf *conf = kc->conf;
  SiteList *stl = init_SiteList(100);
  QIter qter;

  uint64_t ins_id = 0;
  char **fq_file1=fq_files1;
  char **fq_file2=fq_files2;
  while (1) {
    if (*fq_file1 == NULL || *fq_file2 == NULL)
      break;

    fprintf(stderr, "[%s] processing: %s and %s\n",
	    __func__, *fq_file1, *fq_file2);

    gzFile FQ_FILE1 = gzopen(*fq_file1,"r");
    gzFile FQ_FILE2 = gzopen(*fq_file2,"r");
    kseq_t *seq1 = kseq_init(FQ_FILE1);
    kseq_t *seq2 = kseq_init(FQ_FILE2);

    int l;
    size_t rid = 0;

    while ((l = kseq_read(seq1) >= 0) && (l = kseq_read(seq2) >= 0)) {

      rid++;
      if ((rid & 0xFFFFU) == 0) {
	fprintf(stderr, "\r[%s] parsed %zu inserts.", __func__, rid);
	fflush(stderr);
	check_dump_VarSIBuffer(vb);
	check_dump_RefSIBuffer(rb);
      }

      rk_bind(rk, seq1->seq.s, seq1->qual.s);
      clear_SiteList(stl);

      while (rk_generate(rk) > 0) {

	Suffix *sf = get_SeqHash(sh, rk->p, rk->d);
      	if (sf==NULL) continue;
	SeqList *sl=(SeqList*)sf->data;

	size_t si;
	for (si=0;si<sl->size;++si) {
	  Seq *s = ref_SeqList(sl, si);
	  Kmer *k=s->k; Site *st=k->site;
	  if (exists_SiteList(stl, st)) continue;

	  SiteSeq *ss = get_SSPool(kc->ssp, st);
	  uint8_t sw = 0; uint8_t m; uint8_t cs1, cs2;
	  char *t = ss->seq + st->rpos;
	  if (s->str) {
	    if ((k->spos<0 && KLEN+rk->ks>(unsigned)(-k->spos)) ||
		(k->spos>0 && (unsigned) k->spos<=rk->len-KLEN-rk->ks)) {

	      if (seed_extend_neg(t, rk, k, &cs1, &cs2, &m, t)) {
		push_SiteList(stl, st);
		RefSI *tr = next_ref_RefSIList(rb->buffer);
		tr->ins_id = ins_id;
		tr->site_id = st->id;
		tr->h1.str = 1;
		tr->h1.pos = st->pos + k->spos + KLEN - rk->len + rk->ks + cs1;
		tr->h2.pos = 0;
		tr->h1.css = cs1;
		tr->h1.cse = cs2;
		tr->h1.cm = rk->len-cs1-cs2;
	      } else {
		sw=1;
		goto READ1_SW;
	      }
	    }
	  } else {

	    if ((k->spos<0 && rk->len-rk->ks>(unsigned)(-k->spos)) ||
		(k->spos>0 && rk->ks>=(unsigned) k->spos)) {

	      if (seed_extend_pos(t, rk, k, &cs1, &cs2, &m, t)) {
		push_SiteList(stl, st);
		RefSI *tr = next_ref_RefSIList(rb->buffer);
		tr->ins_id = ins_id;
		tr->site_id = st->id;
		tr->h1.str = 0;
		tr->h1.pos = st->pos + k->spos - rk->ks + cs1;
		tr->h2.pos = 0;
		tr->h1.css = cs1;
		tr->h1.cse = cs2;
		tr->h1.cm = rk->len-cs1-cs2;
	      } else {
		sw=1;
		goto READ1_SW;
	      }
	    }
	  }

	READ1_SW:
	  if (sw) {

	    SiteSeq *ss = get_SSPool(kc->ssp, st);
	    ss_encode_nt256int8(ss);
	    rk_encode_nt256int8(rk);
	    s_align *a  = _ssw_align(rk->seq_sw, rk->len,
				     ss->swseq, ss->len);
	    s_align *ar = _ssw_align(rk->seq_sw_r, rk->len,
				     ss->swseq, ss->len);
	    if (a->score1 > ar->score1) {
	      if (check_aln(a, ss->seq, rk, 0, conf)) {
		push_SiteList(stl, st);
		VarSI *tv = next_ref_VarSIList(vb->buffer);
		tv->ins_id = ins_id;
		tv->site_id = st->id;
		tv->h1.str = 0;
		tv->h1.pos = st->pos-st->rpos+a->ref_begin1;
		tv->h2.pos = 0;
		tv->h2.n_cigar = 0;
		qter.p = seq1->qual.s;
		qter.d = 0;
		tv->h1.tiv = type_SIVar(st, ss->swseq, conf,
					rk->seq_sw, a, &qter);
		append_soft_clipping(&tv->h1, a, rk->seq_sw,
				     ss->swseq, rk->len);
	      }
	    } else {
	      if (check_aln(ar, ss->seq, rk, 1, conf)) {
		push_SiteList(stl, st);
		VarSI *tv = next_ref_VarSIList(vb->buffer);
		tv->ins_id = ins_id;
		tv->site_id = st->id;
		tv->h1.str = 1;
		tv->h1.pos = st->pos-st->rpos+ar->ref_begin1;
		tv->h2.pos = 0;
		tv->h2.n_cigar = 0;
		qter.p = seq1->qual.s + rk->len - 1;
		qter.d = 1;
		tv->h1.tiv = type_SIVar(st, ss->swseq, conf,
					rk->seq_sw_r, ar, &qter);
		append_soft_clipping(&tv->h1, ar, rk->seq_sw_r,
				     ss->swseq, rk->len);
	      }
	    }

	    free_align(a);
	    free_align(ar);
	  }
	}
      }


      /* read 2 */
      rk_bind(rk, seq2->seq.s, seq2->qual.s);
      clear_SiteList(stl);
      while (rk_generate(rk) > 0) {

	Suffix *sf = get_SeqHash(sh, rk->p, rk->d);
      	if (sf==NULL) continue;
	SeqList *sl=(SeqList*)sf->data;

	size_t si;
	for (si=0;si<sl->size;++si) {
	  Seq *s = ref_SeqList(sl, si);
	  Kmer *k=s->k; Site *st=k->site;
	  if (exists_SiteList(stl, st)) continue;

	  SiteSeq *ss = get_SSPool(kc->ssp, st);
	  uint8_t sw = 0; uint8_t m; uint8_t cs1, cs2;
	  char *t = ss->seq + st->rpos;
	  if (s->str) {
	    if ((k->spos<0 && KLEN+rk->ks>(unsigned)(-k->spos)) ||
		(k->spos>0 && (unsigned) k->spos<=rk->len-KLEN-rk->ks)) {

	      if (seed_extend_neg(t, rk, k, &cs1, &cs2, &m, t)) {
		push_SiteList(stl, st);
		RefSI *tr = backtrace_RefSIBuffer(rb, ins_id, st);
		if (!tr) {
		  tr = next_ref_RefSIList(rb->buffer);
		  tr->ins_id = ins_id;
		  tr->site_id = st->id;
		  tr->h1.pos = 0;
		}
		tr->h2.str = 1;
		tr->h2.pos = st->pos+k->spos+KLEN-rk->len+rk->ks+cs1;
		tr->h2.css = cs1;
		tr->h2.cse = cs2;
		tr->h2.cm  = rk->len-cs1-cs2;
	      } else {
		sw = 1;
		goto READ2_SW;
	      }
	    }
	      
	  } else {
	    
	    if ((k->spos<0 && rk->len-rk->ks>(unsigned)(-k->spos)) ||
		(k->spos>0 && rk->ks>=(unsigned) k->spos)) {

	      if (seed_extend_pos(t, rk, k, &cs1, &cs2, &m, t)) {
		push_SiteList(stl, st);
		RefSI *tr = backtrace_RefSIBuffer(rb, ins_id, st);
		if (!tr) {
		  tr = next_ref_RefSIList(rb->buffer);
		  tr->ins_id = ins_id;
		  tr->site_id = st->id;
		  tr->h1.pos = 0;
		}
		tr->h2.str = 0;
		tr->h2.pos = st->pos+k->spos-rk->ks+cs1;
		tr->h2.css = cs1;
		tr->h2.cse = cs2;
		tr->h2.cm  = rk->len-cs1-cs2;
	      } else {
		sw = 1;
		goto READ2_SW;
	      }
	    }
	  }

	READ2_SW:
	  if (sw) {

	    rk_encode_nt256int8(rk);
	    SiteSeq *ss = get_SSPool(kc->ssp, st);
	    ss_encode_nt256int8(ss);
	    s_align *a=_ssw_align(rk->seq_sw, rk->len,
				  ss->swseq, ss->len);
	    s_align *ar=_ssw_align(rk->seq_sw_r, rk->len,
				   ss->swseq, ss->len);

	    if (a->score1 > ar->score1) {
	      if (check_aln(a, ss->seq, rk, 0, conf)) {
		push_SiteList(stl, st);

		VarSI *tv = NULL;
		VarSI *v;
		for (v=vb->buffer->buffer+vb->buffer->size-1; 
		     v>=vb->buffer->buffer && v->ins_id==ins_id; --v) {
		  if (v->site_id == st->id) {tv = v; break;}
		}
		if (!tv) {
		  tv = next_ref_VarSIList(vb->buffer);
		  tv->ins_id = ins_id;
		  tv->site_id = st->id;
		  tv->h1.pos = 0;
		  tv->h1.n_cigar = 0;
		}
		tv->h2.str = 0;
		tv->h2.pos = st->pos-st->rpos+a->ref_begin1;
		qter.p = seq2->qual.s;
		qter.d = 0;
		tv->h2.tiv = type_SIVar(st, ss->swseq, conf,
					rk->seq_sw, a, &qter);
		append_soft_clipping(&tv->h2, a, rk->seq_sw,
				     ss->swseq, rk->len);

	      }
	    } else {
	      if (check_aln(ar, ss->seq, rk, 1, conf)) {
		push_SiteList(stl, st);

		VarSI *tv = backtrace_VarSIBuffer(vb, ins_id, st);
		if (!tv) {
		  tv = next_ref_VarSIList(vb->buffer);
		  tv->ins_id = ins_id;
		  tv->site_id = st->id;
		  tv->h1.pos = 0;
		  tv->h1.n_cigar = 0;
		}
		tv->h2.str = 1;
		tv->h2.pos = st->pos-st->rpos+ar->ref_begin1;
		qter.p = seq2->qual.s + rk->len - 1;
		qter.d = 1;
		tv->h2.tiv = type_SIVar(st, ss->swseq, conf,
					rk->seq_sw_r, ar, &qter);
		append_soft_clipping(&tv->h2, ar, rk->seq_sw_r,
				     ss->swseq, rk->len);

	      }
	    }
	    free_align(a);
	    free_align(ar);
	  }
	}
      }


      ins_id++;
    }

    kseq_destroy(seq1);
    kseq_destroy(seq2);
    gzclose(FQ_FILE1);
    gzclose(FQ_FILE2);

    fprintf(stderr, "\r[%s] parsed %zu inserts.\n", __func__, rid);
    fflush(stderr);

    ++fq_file1;
    ++fq_file2;

  }

  fprintf(stderr, "[%s] captured %zu reference alignments and %zu variant alignments\n", __func__, rb->buffer->size, vb->buffer->size);

  free_SiteList(stl);		/* don't destroy the kc->sl */
  free(rk);

  return 0;
}
