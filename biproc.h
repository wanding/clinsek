#ifndef _WZ_BIPROC_H_
#define _WZ_BIPROC_H_

/* this code inputs by reads from bam file
   outputs by inserts
   mates are matched by read name and (mpos, mtid)
*/


#include "samtools/khash.h"
#include "samtools/klist.h"
#include "read_pool.h"

typedef struct {
  char *qname;
  int32_t tid;
  int32_t pos;
} FirstReadKey;

typedef struct {
  bam1_t *b1;
  bam1_t *b2;
} __attribute__((__packed__)) insert1_t;

static inline int read_key_match(FirstReadKey k1, FirstReadKey k2) {
  if ((strcmp(k1.qname, k2.qname) == 0 ) && 
      (k1.tid == k2.tid) &&
      (k1.pos == k2.pos)) return 1;
  else return 0;
}

#define qname_hash_func(k) kh_str_hash_func((k).qname)
KHASH_INIT(InsMap, FirstReadKey, insert1_t*, 1, qname_hash_func, read_key_match)

#define __realn_free_insert(key)	/* no-op */
KMEMPOOL_INIT(insert1_t, insert1_t, __realn_free_insert)

typedef struct {
  khash_t(InsMap) *im;
  kmempool_t(insert1_t) *imp;
  kmempool_t(Read) *rmp;
  samfile_t *in;
  samfile_t *out;
  FirstReadKey k;
  insert1_t *ins;
  khint_t i;			/* index of the current read key */
} BIProc;

static inline BIProc *init_BIProc(samfile_t *in, samfile_t *out) {
  BIProc *bip = calloc(1, sizeof(BIProc));
  bip->im = kh_init(InsMap);
  bip->rmp = kmp_init(Read);
  bip->imp = kmp_init(insert1_t);
  bip->in = in; bip->out = out;
  return bip;
}

static inline void free_BIProc(BIProc *bip) {
  kh_destroy(InsMap, bip->im);
  kmp_destroy(Read, bip->rmp);
  kmp_destroy(insert1_t, bip->imp);
  free(bip);
}

static inline bam1_t *bip_read1(BIProc *bip) {
  bam1_t *b = kmp_alloc(Read, bip->rmp);
  /* memset(b, 0, sizeof(bam1_t)); */
  if (samread(bip->in, b) >= 0) return b;
  else {
    kmp_free(Read, bip->rmp, b);
    return NULL;
  }
}

/*
  search read by mate location
  if mate is present, return the inserted insert
  if mate is not present, insert the read and return the insert
 */
static inline void bip_put_read(BIProc *bip, bam1_t *b) {
  bam1_core_t *c = &b->core;
  bip->k.qname = bam1_qname(b);
  if (c->flag & BAM_FREAD1) {
    bip->k.tid = c->tid;
    bip->k.pos = c->pos;
  } else {
    bip->k.tid = c->mtid;
    bip->k.pos = c->mpos;
  }
  int ret;
  bip->i = kh_put(InsMap, bip->im, bip->k, &ret);
  if (ret) {			/* empty */
    bip->ins = kmp_alloc(insert1_t, bip->imp);
    kh_val(bip->im, bip->i) = bip->ins;
    memset(bip->ins, 0, sizeof(insert1_t));
    if (c->flag & BAM_FREAD1) bip->ins->b1 = b;
    else bip->ins->b2 = b;
  } else {
    bip->ins = kh_val(bip->im, bip->i);
    if (c->flag & BAM_FREAD1) bip->ins->b1 = b;
    else bip->ins->b2 = b;
  }
}

/* immediately output the read */
void bip_write1r(BIProc *bip, bam1_t *b) {
  samwrite(bip->out, b);
  kmp_free(Read, bip->rmp, b);
}

/* write and delete insert */
void bip_write1i(BIProc *bip) {
  insert1_t *ins = bip->ins;
  if (ins->b1 && ins->b2) {
    samwrite(bip->out, ins->b1);
    samwrite(bip->out, ins->b2);
    kh_del(InsMap, bip->im, bip->i);
    kmp_free(Read, bip->rmp, ins->b1);
    kmp_free(Read, bip->rmp, ins->b2);
    kmp_free(insert1_t, bip->imp, ins);
  } else if (ins->b1) {
    bam1_core_t *c = &ins->b1->core;
    if (!(c->flag & BAM_FPAIRED)) {
      samwrite(bip->out, ins->b1);
      kh_del(InsMap, bip->im, bip->i);
      kmp_free(Read, bip->rmp, ins->b1);
      kmp_free(insert1_t, bip->imp, ins);
    }
  } else if (ins->b2) {
    bam1_core_t *c = &ins->b2->core;
    if (!(c->flag & BAM_FPAIRED)) {
      samwrite(bip->out, ins->b2);
      kh_del(InsMap, bip->im, bip->i);
      kmp_free(Read, bip->rmp, ins->b2);
      kmp_free(insert1_t, bip->imp, ins);
    }
  }
}


#endif /* _WZ_BIPROC_H_ */
