#ifndef _WZ_RKGEN_H_
#define _WZ_RKGEN_H_

#include <stdint.h>
#include "encode.h"

/* 
   Usage:

   RKGen *rk = rk_init(25);
   rk_bind(&rk, seq.s);
   while (rk_generate(&rk) > 0) {
   ## work on rk->p and rk->d
   }
   free(rk);

 */

extern const uint8_t char_to_nt4_table[128];

typedef struct {
  char *seq;			/* read sequence */
  char *qual;
  uint32_t len;			/* read length */
  uint32_t klen;
  uint8_t taboo[1000];	  /* the positions where no k-mer can be found */
  uint32_t ks;			/* k-mer position */
  char *se;			/* pointer to k-mer sequence end */
  uint8_t en;			/* need to initate p, d?  */
  uint32_t p;			/* prefix */
  uint32_t d;			/* suffix */
  uint8_t sw:1;			/* has encoded to nt256int8? */
  int8_t seq_sw[1000];
  int8_t seq_sw_r[1000];
  uint8_t revseq:1;
  char seq_r[1000];
  uint8_t revqual:1;
  char qual_r[1000];
} RKGen;

static inline RKGen *
rk_init(uint32_t klen) {
  RKGen *rk = malloc(sizeof(RKGen));
  rk->klen = klen;
  return rk;
}

/* /\* return 1 if the read does not contain too many 'N', '.' */
/*    0 if there are too many ambiguous bases ('N', '.') *\/ */
/* static inline int */
/* rk_bind(RKGen *rk, char *seq, char *qual) { */
/*   rk->len = strlen(seq); */
/*   size_t i; int j=-1; uint8_t ncnt = 0; */
/*   for (i=rk->len;i--;--j) { */
/*     if (seq[i] == 'N' || seq[i] == '.') { /\* in some seq data, missing data is a '.' *\/ */
/*       j=rk->klen; */
/*       ++ncnt;  */
/*       if (ncnt>2) return 0;	/\* too many 'N' *\/ */
/*     } */
/*     if (j>0) rk->taboo[i] = 1; */
/*     else rk->taboo[i] = 0; */
/*   } */
  
/*   rk->seq = seq; */
/*   rk->qual = qual; */
/*   rk->ks = -1; */
/*   rk->se = seq + rk->klen - 2; */
/*   rk->en = 1; */
/*   rk->p = 0; */
/*   rk->d = 0; */
/*   rk->sw = 0; */
/*   rk->revseq = 0; */
/*   rk->revqual = 0; */
/*   return 1; */
/* } */

static inline void
rk_bind(RKGen *rk, char *seq, char *qual) {
  rk->len = strlen(seq);
  size_t i; int j=-1;
  for (i=rk->len;i--;--j) {
    if (seq[i] == 'N' || seq[i] == '.') {
      /* in some seq data, missing data is a '.' */
      j=rk->klen;
    }
    if (j>0) rk->taboo[i] = 1;
    else rk->taboo[i] = 0;
  }
  
  rk->seq = seq;
  rk->qual = qual;
  rk->ks = -1;
  rk->se = rk->seq + rk->klen - 2;
  rk->en = 1;
  rk->p = 0;
  rk->d = 0;
  rk->sw = 0;
  rk->revseq = 0;
  rk->revqual = 0;
}

static inline void
rk_rewind(RKGen *rk) {
  rk->ks = -1;
  rk->se = rk->seq + rk->klen - 2;
  rk->en = 1;
  rk->p = 0;
  rk->d = 0;  
}


static inline char*
rk_revseq(RKGen *rk) {
  if (!rk->revseq) {
    _nt256char_rev(rk->seq_r, rk->seq, rk->len);
    rk->revseq = 1;
  }
  return rk->seq_r;
}

static inline char*
rk_revqual(RKGen *rk) {
  if (!rk->revqual) {
    uint32_t i;
    for (i=0; i<rk->len; ++i) {
      rk->qual_r[rk->len-1-i] = rk->qual[i];
    }
    rk->revqual = 1;
  }
  return rk->qual_r;
}

static inline void
rk_encode_nt256int8(RKGen *rk) {
  if (rk->sw) return;
  _nt256char_encode_nt256int8(rk->seq_sw, rk->seq, rk->len);
  _nt256int8_rev(rk->seq_sw_r, rk->seq_sw, rk->len);
  rk->sw = 1;
}

static inline int
rk_generate(RKGen *rk) {

  ++rk->se;
  ++rk->ks;

  if (rk->ks >= rk->len-rk->klen+1) return 0;
  while (rk->taboo[rk->ks]) {
    ++rk->ks; ++rk->se;
    if (rk->ks >= rk->len-rk->klen+1) return 0;
    rk->en=1; continue;
  }

  if (rk->en) {
    rk->p = 0; rk->d = 0;
    char *s = rk->seq + rk->ks;
    uint32_t i;
    for(i=10;i--;++s) {
      rk->p<<=2;
      rk->p|=char_to_nt4_table[(unsigned char)*s];
    }
    for(i=15;i--;++s) {
      rk->d<<=2;
      rk->d|=char_to_nt4_table[(unsigned char)*s];
    }
    rk->en = 0;
  } else {
    rk->p=((rk->p<<2) | (rk->d>>28))&0xFFFFF;
    rk->d=((rk->d<<2) |
	   char_to_nt4_table[(unsigned char)*(rk->se)]) & 0x3FFFFFFF;
  }
  
  return 1;
}


#endif /* _WZ_RKGEN_H_ */
