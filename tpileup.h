#ifndef _WZ_TPILEUP_H_
#define _WZ_TPILEUP_H_

#include "pileup.h"

typedef struct {

  PlpAlleleData ad;
  PlpBCFData bcf;
  PlpBamData bam;
  PlpReadData rd;
  PlpRefData refseq;

} PlpData;

typedef struct {
  Allele *ref;
  AllelePList *alts;
  FloatList *quals;
} BCFRec;

static inline BCFRec* init_BCFRec() {
  BCFRec *rec = malloc(sizeof(BCFRec));
  rec->alts = init_AllelePList(2);
  rec->quals = init_FloatList(2);

  return rec;
}

static inline void clear_BCFRec(BCFRec *rec) {
  clear_AllelePList(rec->alts);
  clear_FloatList(rec->quals);
}

static inline void free_BCFRec(BCFRec *rec) {
  free_AllelePList(rec->alts);
  free_FloatList(rec->quals);
  free(rec);
}


void tpileup(char *bcf_fn, char *bam_fn, KCnts *kc, uint8_t *siv);

#endif /* _WZ_TPILEUP_H_ */
