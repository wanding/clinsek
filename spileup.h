#ifndef _WZ_SPILEUP_H_
#define _WZ_SPILEUP_H_

#include "pileup.h"
#include "stats.h"

typedef struct {
  Allele *ref_t;
  Allele *ref_n;
  AllelePList *alts;
  FloatList *quals;
  IntList *altcnts_t;
  IntList *altcnts_n;
} BCFSomaticRec;

static inline BCFSomaticRec* init_BCFSomaticRec() {
  BCFSomaticRec *rec = malloc(sizeof(BCFSomaticRec));
  rec->alts      = init_AllelePList(2);
  rec->quals     = init_FloatList(2);
  rec->altcnts_t = init_IntList(2);
  rec->altcnts_n = init_IntList(2);

  return rec;
}

static inline void clear_BCFSomaticRec(BCFSomaticRec *rec) {
  clear_AllelePList(rec->alts);
  clear_FloatList(rec->quals);
  clear_IntList(rec->altcnts_t);
  clear_IntList(rec->altcnts_n);
}

static inline void free_BCFSomaticRec(BCFSomaticRec *rec) {
  free_AllelePList(rec->alts);
  free_FloatList(rec->quals);
  free_IntList(rec->altcnts_t);
  free_IntList(rec->altcnts_n);
  free(rec);
}

void spileup(char *bcf_fn, char *bam_fn_t, char *bam_fn_n, KCnts *kc, uint8_t *siv);


#endif /* _WZ_SPILEUP_H_ */
