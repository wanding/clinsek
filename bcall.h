#include <unistd.h>
#include <stdint.h>
#include <zlib.h>
#include "samtools/kseq.h"
#include "samtools/kstring.h"
#include "tsv_parser.h"
#include "encode.h"
#include "utils.h"
#include "rkgen.h"
#include "wvec.h"
#include "seq.h"
#include "ssw.h"

typedef struct {
  uint32_t seed_len;
  uint32_t seed_extend_max_mismatch;
} BCallConf;

typedef struct {
  char *seq;
  int8_t *seq_sw;
  uint32_t len;			/* length of the sequence */
  uint32_t score;
  uint32_t bp_pos;
} ASeq;
DEFINE_VECTOR(ASeqList, ASeq);

typedef struct {
  char *id;
  ASeq alt;
  ASeqList *refs;
  uint32_t alt_support;
} APair;

DEFINE_VECTOR(APairList, APair);
/* the allele support information for 1 read */
DEFINE_VECTOR(APairPList, APair*);

void destroy_APairList(APairList *pl) {
  uint32_t i;
  for (i=0; i<pl->size; ++i) {
    APair *ap = ref_APairList(pl, i);
    free(ap->id);
    uint32_t j;
    for (j=0; j<ap->refs->size; ++j) {
      ASeq *as = ref_ASeqList(ap->refs, j);
      free(as->seq);
      free(as->seq_sw);
    }
    free_ASeqList(ap->refs);
    free(ap->alt.seq);
    free(ap->alt.seq_sw);
  }
  free_APairList(pl);
}

/* if seq == &pair->ref, position is on reference allele
   if seq == &pair->alt, position is on alternative allele */
typedef struct {
  APair *pair;
  ASeq *seq;
  uint32_t pos;	/* start position of the k-mer on the sequence*/
  uint8_t str:1;
} AKmer;

DEFINE_VECTOR(AKmerList, AKmer);
DEFINE_VECTOR(AKmerPList, AKmer*);

typedef struct {
  APairList *pl;
  SeqHash *sh;
  AKmerList *kl;
} BCnts;

static inline void free_bcnts(BCnts *bc) {
  destroy_SeqHash(bc->sh, (void (*)(void *)) free_AKmerPList);
  destroy_APairList(bc->pl);
  free_AKmerList(bc->kl);
}

void hash_allele_seq(ASeq *as, APair *ap, AKmerList *kl, SeqHash *sh);

