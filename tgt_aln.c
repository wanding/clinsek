#include "tgt_aln.h"

WZ_KSEQ_INIT(gzFile, gzread)

/* KSTREAM_INIT(gzFile, gzread, 4096) */
/* __KSEQ_BASIC(gzFile) */
/* __KSEQ_READ */

void
VarSI2aln_copy_cigar(bam_aln_t *ba, VarSI_H *vh) {

  if (vh->pos) {
    ba->pos = vh->pos;
    ba->str = vh->str;
    uint32_t k;
    if (vh->n_cigar && vh->cigar[0]) {
      if (vh->css) {
	if (vh->cse) {		/* soft-clipped from both */
	  ba->n_cigar = vh->n_cigar + 2;
	  ba->cigar = (uint32_t*) malloc(ba->n_cigar*sizeof(uint32_t));
	  ba->cigar[0] = (uint32_t) ((vh->css<<4)|BAM_CSOFT_CLIP);
	  for (k=0; k<vh->n_cigar; ++k) ba->cigar[k+1] = vh->cigar[k];
	  ba->cigar[ba->n_cigar-1] = ((vh->cse<<4)|BAM_CSOFT_CLIP);
	} else {			/* soft-clipped from left */
	  ba->n_cigar  = vh->n_cigar + 1;
	  ba->cigar = (uint32_t*) malloc(ba->n_cigar*sizeof(uint32_t));
	  ba->cigar[0] = (uint32_t) ((vh->css<<4)|BAM_CSOFT_CLIP);
	  for (k=0; k<vh->n_cigar; ++k) ba->cigar[k+1] = vh->cigar[k];
	}
      } else if (vh->cse) {		/* soft-clipped from right */
	ba->n_cigar = vh->n_cigar + 1;
	ba->cigar = (uint32_t*) malloc(ba->n_cigar*sizeof(uint32_t));
	for (k=0; k<vh->n_cigar; ++k) ba->cigar[k] = vh->cigar[k];
	ba->cigar[ba->n_cigar-1] = (uint32_t) ((vh->cse<<4)|BAM_CSOFT_CLIP);
      } else {			/* no soft-clipping */
	ba->n_cigar = vh->n_cigar;
	ba->cigar = (uint32_t*) malloc(ba->n_cigar*sizeof(uint32_t));
	for (k=0; k<vh->n_cigar; ++k) ba->cigar[k] = vh->cigar[k];
      }
    } else ba->n_cigar = 0;
  } else ba->pos = 0;
}

void RefSI2aln_copy_cigar(bam_aln_t *ba, RefSI_H *rh) {

  if (rh->pos && !ba->pos) {
    ba->pos = rh->pos;
    ba->str = rh->str;
    if (rh->css) {
      if (rh->cse) {		/* soft-clipped from both */
	ba->n_cigar  = 3;
	ba->cigar    = (uint32_t*) malloc(3*sizeof(uint32_t));
	ba->cigar[0] = (uint32_t) ((rh->css<<4)|BAM_CSOFT_CLIP);
	ba->cigar[1] = (uint32_t) ((rh->cm<<4)|BAM_CMATCH);
	ba->cigar[2] = (uint32_t) ((rh->cse<<4)|BAM_CSOFT_CLIP);
      } else {			/* soft-clipped from left */
	ba->n_cigar  = 2;
	ba->cigar    = (uint32_t*) malloc(2*sizeof(uint32_t));
	ba->cigar[0] = (uint32_t) ((rh->css<<4)|BAM_CSOFT_CLIP);
	ba->cigar[1] = (uint32_t) ((rh->cm<<4)|BAM_CMATCH);
      }
    } else if (rh->cse) {	/* soft-clipped from right */
      ba->n_cigar  = 2;
      ba->cigar    = (uint32_t*) malloc(2*sizeof(uint32_t));
      ba->cigar[0] = (uint32_t) ((rh->cm<<4)|BAM_CMATCH);
      ba->cigar[1] = (uint32_t) ((rh->cse<<4)|BAM_CSOFT_CLIP);
    } else {			/* no soft-clipping */
      ba->n_cigar  = 1;
      ba->cigar    = (uint32_t*) malloc(sizeof(uint32_t));
      ba->cigar[0] = (uint32_t) ((rh->cm<<4)|BAM_CMATCH);
    }
  }
}

/* merge vb and rb using siv1 */
BamList* merge_aln(VarSIBuffer *vb, RefSIBuffer *rb, uint8_t *siv1) {

  BamList *bl = init_BamList(1023);

  rewind_VarSIBuffer(vb);
  rewind_RefSIBuffer(rb);
  VarSI *v = read_VarSIBuffer(vb);
  RefSI *r = read_RefSIBuffer(rb);
  while (v||r) {

    if (r==NULL || 
	(v!=NULL && v->ins_id <= r->ins_id)) { /* VarSI */
      if (siv1[v->site_id]) {
	bam1s_t *b = next_ref_BamList(bl);
	b->ins_id  = v->ins_id;
	b->site_id = v->site_id;

	VarSI2aln_copy_cigar(&b->a1, &v->h1);
	VarSI2aln_copy_cigar(&b->a2, &v->h2);
      }
      v = read_VarSIBuffer(vb);
    } else {			/* RefSI */
      if (siv1[r->site_id]) {
	bam1s_t *b=NULL; uint32_t i;
	for (i=bl->size; i--;) {
	  bam1s_t *qb = ref_BamList(bl, i);
	  if(qb->ins_id < r->ins_id) break;
	  if(qb->ins_id == r->ins_id &&
	     qb->site_id == r->site_id) {
	    b=qb;
	    break;
	  }
	}
	if (!b) {
	  b = next_ref_BamList(bl);
	  b->ins_id  = r->ins_id;
	  b->site_id = r->site_id;
	  b->a1.pos  = 0;
	  b->a2.pos  = 0;
	}

	RefSI2aln_copy_cigar(&b->a1, &r->h1);
	RefSI2aln_copy_cigar(&b->a2, &r->h2);
      }
      r = read_RefSIBuffer(rb);
    }
  }

  fprintf(stderr, "[%s] merged %zu site insert pairs\n", __func__, bl->size);

  return bl;
}


/* find the base quality of mutations
 * for calculating mapping quality
 * 
 * rs points to the target site on the
 * reference sequence. The reference
 * seq must be long enough
 *
 * tpos is (target_site)->pos */
void type_mutQ(bam_aln_t *ba, char *rs, size_t tpos, RKGen *rk) {

  size_t j;

  if (!ba->pos) return;

  char *r = rs + ba->pos - tpos;
  char *q = ba->str ? rk_revseq(rk) : rk->seq;
  char *qual = ba->str ? rk_revqual(rk) : rk->qual;

  ba->mutQ = 0;
  for (j=0; j<ba->n_cigar; ++j) {
    uint32_t op = bam_cigar_op(ba->cigar[j]);
    uint32_t len = bam_cigar_oplen(ba->cigar[j]);
    switch (op) {
    case BAM_CMATCH:
      for (; len--; ++r, ++q, ++qual)
	if (*r != *q && *q != 'N' && *q != '.')
	  ba->mutQ += *qual - 33;
      break;
    case BAM_CINS:
      ba->mutQ += 93*len;
      q += len;
      qual += len;
      break;
    case BAM_CSOFT_CLIP:
      for (; len--; ++q, ++qual)
	ba->mutQ += *qual - 33;
      break;
    case BAM_CDEL:
      ba->mutQ += 93*len;
      r += len;
      break;
    case BAM_CREF_SKIP:
      r += len;
      break;
    default:
      fprintf(stderr, "[%s:%d] Unknown cigar, %u\n",
	      __func__, __LINE__, op);
      exit(1);
      break;
    }
  }
}

void
refine_insert_aln(bam_aln_t *ba, SSPool *ssp,
		  SeqHash *sh, Site *st, RKGen *rk, CallConf *conf) {
  if (ba->pos) {		/* read has been aligned */
    if (!ba->n_cigar) {
      /* re-align target read because of missing storage of large cigar */
      SiteSeq *ss = get_SSPool(ssp, st);
      ss_encode_nt256int8(ss);
      rk_encode_nt256int8(rk);
      s_align *a;
      a = _ssw_align(ba->str ? rk->seq_sw_r : rk->seq_sw, rk->len,
		     ss->swseq + st->rpos + ba->pos - st->pos - rk->len/2,
		     2 * rk->len);
      swaln_cpy_cigar(ba, a, rk->len);
      free_align(a);
    }
  } else {			/* read has not been aligned */
    fix_mate(sh, st, ba, rk, ssp, conf);
  }
}

SiteFilter*
prelim_call(KCnts *kc, VarSIBuffer *vb, RefSIBuffer *rb) {
  SiteFilter *fter = init_SiteFilter(2);
  uint32_t *vcnts = (uint32_t*) calloc(kc->sl->size, sizeof(uint32_t));
  uint32_t *rcnts = (uint32_t*) calloc(kc->sl->size, sizeof(uint32_t));

  VarSIList *vl = vb->buffer;
  uint32_t i;
  for (i=0; i<vl->size; ++i) {
    VarSI *v = ref_VarSIList(vl, i);
    if ((v->h1.n_cigar && v->h1.tiv) ||
	(v->h2.n_cigar && v->h2.tiv)) {
      vcnts[v->site_id]++;
    } else {
      rcnts[v->site_id]++;
    }
  }

  RefSIList *rl = rb->buffer;
  for (i=0; i<rl->size; ++i) {
    RefSI *r = ref_RefSIList(rl, i);
    rcnts[r->site_id]++;
  }

  for (i=0; i<kc->sl->size; ++i) {
    if (vcnts[i] > kc->conf->min_var &&
	(double) vcnts[i] / (double) (vcnts[i] + rcnts[i]) > kc->conf->min_var_p) {
      push_SiteFilter(fter, 1);
    } else push_SiteFilter(fter, 0);
  }


  free(vcnts);
  free(rcnts);

  return fter;
}

/* 
   wide-hash target sites for mate alignment
   build anchor hash and kmer list into sh and kl
 */
void
widehash_targets(SeqHash *sh, KmerList *kl, SiteList *sl, 
		 SSPool *ssp, uint8_t *siv1) {

  uint32_t i;
  char ksr[100];
  for (i=0; i<sl->size; ++i) {
    if (siv1[i]) {
      Site *s = get_SiteList(sl, i);
      SiteSeq *ss = get_SSPool(ssp, s);
      size_t as;
      for (as=150; as<2000-150; as+=25) {
	Kmer *k = (Kmer*) malloc(sizeof(Kmer));
	push_KmerList(kl, k);
	k->spos = as;
	k->site = s;

	Suffix *sf = insert_SeqHash(sh, ss->seq + k->spos);
	if (sf->data==NULL) sf->data = init_SeqList(2);
	Seq *sq = next_ref_SeqList((SeqList*) sf->data);
	sq->str = 0;
	sq->k = k;

	_nt256char_rev(ksr, ss->seq + k->spos, KLEN);
	sf = insert_SeqHash(sh, ksr);
	if (sf->data==NULL) sf->data = init_SeqList(2);
	sq = next_ref_SeqList((SeqList*) sf->data);
	sq->str = 1;
	sq->k = k;
      }
    }
  }
}

/* wide-hash paralogous sites 
   prepare homologous sites sequences
   anchor hash for these homologous sites
 */
void
widehash_paralogs(SeqHash *sh_homo, KmerList *kl_homo,
		  SiteList *sl, RefSeq *rs, uint8_t *siv1) {

  uint32_t i;
  char ksr[100];
  for (i=0; i<sl->size; ++i) {
    if (siv1[i]) {
      Site *st = get_SiteList(sl, i);
      size_t j;
      for (j=0; j<st->hsl->size; ++j) {
	HomoSite *hs=ref_HSiteList(st->hsl, j);
	hs->seq = fetch_RefSeq(rs, hs->chrm, hs->pos-hs->rpos, 2001);
	hs->swseq = (int8_t*) malloc(2001*sizeof(int8_t));
	_nt256char_encode_nt256int8(hs->swseq, hs->seq, 2001);

	size_t as;
	/* for (as=150; as<2000-150; as+=25) { */
	for (as=150; as<2000-150; as+=12) {
	  Kmer *k = (Kmer*) malloc(sizeof(Kmer));
	  push_KmerList(kl_homo, k);
	  k->spos = as;
	  k->site = hs;

	  Suffix *sf = insert_SeqHash(sh_homo, hs->seq+k->spos);
	  if (sf->data==NULL) sf->data = init_SeqList(2);
	  Seq *sq = next_ref_SeqList((SeqList*) sf->data);
	  sq->str = 0;
	  sq->k = k;

	  _nt256char_rev(ksr, hs->seq+k->spos, KLEN);
	  sf = insert_SeqHash(sh_homo, ksr);
	  if (sf->data==NULL) sf->data = init_SeqList(2);
	  sq = next_ref_SeqList((SeqList*) sf->data);
	  sq->str = 1;
	  sq->k = k;
	}
      }
    }
  }
}


void
tgt_aln_batch (KCnts *kc, char **fq_files1, char **fq_files2, bamFile bam,
	       VarSIBuffer *vsi, RefSIBuffer *rsi, uint8_t *siv1) {

  CallConf *conf = kc->conf;

  bam1_t *b1 = bam_init1();
  b1->m_data = 1000;
  b1->data = (uint8_t*) malloc(b1->m_data);

  BamList *bl = merge_aln(vsi, rsi, siv1);

  /* uint32_t ii; size_t ins_id2=-1; uint32_t pos1=1; uint32_t pos2=1;size_t readcnts=0; */
  /* for (ii=0; ii<bl->size; ++ii) { */
  /*   bam1s_t *b = ref_BamList(bl, ii); */
  /*   if (b->ins_id != ins_id2) { */
  /*     ins_id2 = b->ins_id; */
  /*     pos1 = 1; pos2 = 1; */
  /*   } */
  /*   if (pos1 && b->a1.pos) { */
  /*     readcnts++; pos1=0; */
  /*   } */
  /*   if (pos2 && b->a2.pos) { */
  /*     readcnts++; pos2=0; */
  /*   } */
  /* } */
  /* fprintf(stderr, "%zu readsnnn\n", readcnts); */
  /* fflush(stderr); */

  SSPool *ssp = init_SSPool(2001, kc->sl->size, 0x8000000, kc->rs);
  fill_SSPool(ssp, kc->sl, siv1);
  SeqHash  *sh = init_SeqHash();
  KmerList *kl = init_KmerList(1023);
  widehash_targets(sh, kl, kc->sl, ssp, siv1);
  SeqHash  *sh_homo = init_SeqHash();
  KmerList *kl_homo = init_KmerList(1023);
  widehash_paralogs(sh_homo, kl_homo, kc->sl, kc->rs, siv1);

  /* prepare read parsing */
  char **fq_file1 = fq_files1;
  char **fq_file2 = fq_files2;
  gzFile FQ_FILE1 = gzopen(*fq_file1,"r");
  gzFile FQ_FILE2 = gzopen(*fq_file2,"r");
  fprintf(stderr, "[%s] processing %s and %s\n",
	  __func__, *fq_file1, *fq_file2);
  kseq_t *seq1 = kseq_init(FQ_FILE1);
  kseq_t *seq2 = kseq_init(FQ_FILE2);
  BamPList *vb = init_BamPList(1023); /* for holding distinct alignments for each insert */
  BamList *bl_homo = init_BamList(1023); /* for holding alignments of an insert against homologous sites (they were not previously aligned) , it needs to be freed at the end of each bam output*/
  size_t ins_id = 0;		/* current insert id */
  kseq_read(seq1); kseq_read(seq2);

  RKGen *rk1 = rk_init(KLEN);
  RKGen *rk2 = rk_init(KLEN);
  rk_bind(rk1, seq1->seq.s, seq1->qual.s);
  rk_bind(rk2, seq2->seq.s, seq2->qual.s);

  Site *st;
  size_t bi;
  for (bi=0; bi<bl->size; ++bi) {
    bam1s_t *b = ref_BamList(bl, bi);

    /* if this record is the last in the insert
     * output bam and retrieve new sequence */
    if (b->ins_id != ins_id) {
      if (vb->size) bam_format_aln(vb, bam, b1, seq1, seq2);
      size_t ns = b->ins_id - ins_id - 1;
      kseq_skip(seq1, ns);
      ns = kseq_skip(seq2, ns);
      while (ns!=0) {
	gzclose(FQ_FILE1);
	gzclose(FQ_FILE2);
	FQ_FILE1 = gzopen(*(++fq_file1),"r");
	FQ_FILE2 = gzopen(*(++fq_file2),"r");
	fprintf(stderr, "[%s] processing %s and %s\n", __func__, *fq_file1, *fq_file2);
	seq1 = kseq_init(FQ_FILE1);
	seq2 = kseq_init(FQ_FILE2);
	kseq_skip(seq1, ns);
	ns = kseq_skip(seq2, ns);
      }
      ins_id = b->ins_id;
      kseq_read(seq1);
      int l = kseq_read(seq2);
      while (l < 0) {
	gzclose(FQ_FILE1);
	gzclose(FQ_FILE2);
	FQ_FILE1 = gzopen(*(++fq_file1),"r");
	FQ_FILE2 = gzopen(*(++fq_file2),"r");
	fprintf(stderr, "[%s] processing %s and %s\n", __func__, *fq_file1, *fq_file2);
	seq1 = kseq_init(FQ_FILE1);
	seq2 = kseq_init(FQ_FILE2);
	kseq_read(seq1); l = kseq_read(seq2);
      }

      rk_bind(rk1, seq1->seq.s, seq1->qual.s);
      rk_bind(rk2, seq2->seq.s, seq2->qual.s);

      clear_BamPList(vb);
      clear_BamList(bl_homo);
    }

    /* check if previous alignment of this insert
     * has the same coordinate for one of the aligned
     * mate reads. */
    st = get_SiteList(kc->sl, b->site_id);
    b->chrm = st->chrm;
    uint8_t aligned=0; uint32_t i;
    for (i=0; i<vb->size; ++i) {
      bam1s_t *qb = get_BamPList(vb, i);
      if (qb->chrm == b->chrm &&
	  (qb->a1.pos == b->a1.pos ||
	   qb->a2.pos == b->a2.pos)) {
	aligned=1;
	break;
      }
    }
    if (aligned) continue;

    /*********************
     * align this insert *
     *********************/

    refine_insert_aln(&b->a1, ssp, sh, st, rk1, conf);
    refine_insert_aln(&b->a2, ssp, sh, st, rk2, conf);

    /* check if vb contain the alignment.
     * if not, push to vb */
    uint8_t new_align = 1;
    for (i=0; i<vb->size; ++i) {
      bam1s_t *qb = get_BamPList(vb, i);
      if (bam_aln_equal(qb, b) == 0) {
	new_align = 0;
	break;
      }
    }
    if (new_align) {
      SiteSeq *ss = get_SSPool(ssp, st);
      type_mutQ(&b->a1, ss->seq + st->rpos, st->pos, rk1);
      type_mutQ(&b->a2, ss->seq + st->rpos, st->pos, rk2);
      push_BamPList(vb, b);
    }

    /***************************************
     * align the insert against homologous *
     * sites (if any)                      *
     ***************************************/

    if (st->hsl->size) {

      /* investigate all the homologous sites */
      size_t h;
      for (h=0; h<st->hsl->size; ++h) {
	HomoSite *hs = ref_HSiteList(st->hsl, h);
	bam1s_t *b = next_ref_BamList(bl_homo);
	b->a1.pos = b->a2.pos = 0;
	b->chrm = hs->chrm;

	/*** align read 1 to homologous site ***/
	insert_aln_hsite(sh_homo, hs, &b->a1, rk1, conf);
	/*** align read 2 to homologous site ***/
	insert_aln_hsite(sh_homo, hs, &b->a2, rk2, conf);

	/* perform global sw if one mate read is well aligned */
	if (b->a2.pos && !b->a1.pos) 
	  global_aln_hsite(hs, &b->a1, rk1, b->a2.str, conf);
	if (b->a1.pos && !b->a2.pos)
	  global_aln_hsite(hs, &b->a2, rk2, b->a1.str, conf);

	uint8_t new_align;
	if (b->a1.pos || b->a2.pos) {
	  /* check if vb contain the alignment */
	  new_align = 1;
	  for (i=0; i<vb->size; ++i) {
	    bam1s_t *qb = get_BamPList(vb, i);
	    if (bam_aln_equal(qb, b) == 0) {
	      new_align = 0;
	      break;
	    }
	  }
	} else new_align = 0;

	if (new_align) {
	  type_mutQ(&b->a1, hs->seq+hs->rpos, hs->pos, rk1);
	  type_mutQ(&b->a2, hs->seq+hs->rpos, hs->pos, rk2);

	  push_BamPList(vb, b);
	} else bl_homo->size--;	/* discard the alignment */

      }
    }

  } /* end of looping insert */

    /* output the last insert to bam */
  bam_format_aln(vb, bam, b1, seq1, seq2);

  free(rk1); free(rk2);
  free_BamList(bl_homo);
  bam_destroy1(b1);
  gzclose(FQ_FILE1);
  gzclose(FQ_FILE2);
  kseq_destroy(seq1);
  kseq_destroy(seq2);
}

void
tgt_aln (KCnts *kc, char **fq_files1, char **fq_files2, char *bam_fn,
	 VarSIBuffer *vb, RefSIBuffer *rb, uint8_t *siv) {
  
#ifdef DEBUGTIME
  clock_t t = clock();
#endif // DEBUGTIME

  uint32_t vscnts=0;
  uint32_t i;
  for (i=0; i<kc->sl->size; ++i) {
    if (siv[i])
      vscnts += 1;
  }

  /* prepare bam header */
  bamFile bam = bam_prepare_aln(bam_fn, kc->rs->tl);

  uint8_t *siv1=(uint8_t*) malloc(kc->sl->size);
  uint32_t vs_ind = 0;
  while (vscnts) {
    size_t n_sites=0;
    memset(siv1, 0, kc->sl->size);
    for (i=vs_ind; i<kc->sl->size; ++i) {
      if (siv[i]) {
	siv1[i] = 1;
	get_SiteList(kc->sl, i)->rpos=1000; /* zero-based */

	/* the current implementation supports
	 * processing 5K variant sites in one batch */
	if ((++n_sites)>=5000) break;
	if (!(--vscnts)) break;
      }
    }
    vs_ind = i;

    fprintf(stderr, "[%s] identified %zu variant sites.\n", __func__, n_sites);
    tgt_aln_batch(kc, fq_files1, fq_files2, bam, vb, rb, siv1);
  }
  free(siv1);

  bam_close(bam);

#ifdef DEBUGTIME

  fprintf(stderr, "[%s:%d] time: %.3f min\n", __func__, __LINE__, (float) (clock() - t) / CLOCKS_PER_SEC / 60.);

#endif // DEBUGTIME

}
