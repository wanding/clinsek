#include "bcall.h"
#include <assert.h>

KSEQ_INIT(gzFile, gzread)

void bcnts_format(BCnts *bc, char *prefix) {
  uint32_t i;
  FILE *fh_out = fopen(prefix, "w");
  for (i=0; i<bc->pl->size; ++i) {
    APair *ap = ref_APairList(bc->pl, i);
    fprintf(fh_out, "%s\t%d\n", ap->id, ap->alt_support);
    /* uint32_t j; */
    /* for (j=0; j<ap->refs->size; ++j) { */
    /*   ASeq *as = ref_ASeqList(ap->refs, j); */
    /*   fprintf(fh_out, "\t%d", as->cnt); */
    /* } */
    /* fputc('\n', fh_out); */
  }
  fclose(fh_out);
}

/* 
   format:
   first line: id string
   second line: reference allele sequence
   third line: alternative allele sequence
*/

BCnts* load_allele_pairs(char *alist_fn) {

  TSVParser *tsv = tsv_open(alist_fn);
  APairList *pl = init_APairList(1023);
  APair *ap=0;
  while (1) {
    tsv_readline(tsv);
    if (!tsv->fields->size) break;
    /* fprintf(stderr, "%s\n", tsv_formatline(tsv)); */
    assert(tsv->fields->size == 4);
    if (strcmp(tsv->fields->buffer[0], "alt") == 0) {
      ap = next_ref_APairList(pl);
      ap->alt.seq = strdup(tsv_field(tsv, 3));
      ap->alt.bp_pos = atoi(tsv_field(tsv, 1));
      ap->alt.len = strlen(ap->alt.seq);
      ap->alt.seq_sw = nt256char_encode_nt256int8(ap->alt.seq, ap->alt.len);
      ap->refs = init_ASeqList(2);
      ap->id = strdup(tsv_field(tsv, 2));
    } else if (strcmp(tsv->fields->buffer[0], "ref") == 0) {
      assert(ap!=0);
      ASeq *ref = next_ref_ASeqList(ap->refs);
      ref->seq = strdup(tsv_field(tsv, 3));
      ref->bp_pos = atoi(tsv_field(tsv, 1));
      ref->len = strlen(ref->seq);
      ref->seq_sw = nt256char_encode_nt256int8(ref->seq, ref->len);
    } else {
      fprintf(stderr, "[%s] Malformated line:\n %s",
	      __func__, tsv_formatline(tsv));
      exit(1);
    }
  }

  uint32_t i = 0;
  SeqHash *sh = init_SeqHash();
  AKmerList *kl = init_AKmerList(1023);
  for (i=0; i<pl->size; ++i) {
    APair *ap = ref_APairList(pl, i);
    hash_allele_seq(&ap->alt, ap, kl, sh);
    uint32_t j;
    for (j=0; j<ap->refs->size; ++j) {
      ASeq *ref = ref_ASeqList(ap->refs, j);
      hash_allele_seq(ref, ap, kl, sh);
    }
  }

  fprintf(stderr, "[%s] built %zu allele pairs (%zu unique seqs)\n",
	  __func__, pl->size, count_SeqHash(sh));
  fflush(stderr);

  BCnts *bc = malloc(sizeof(BCnts));
  bc->pl = pl;
  bc->sh = sh;
  bc->kl = kl;
  return bc;
}

#define SEED_FLEN 100
void hash_allele_seq(ASeq *as, APair *ap, AKmerList *kl, SeqHash *sh) {

  char ksr[100];
  AKmer *ak; Suffix *sf;
  uint32_t pos;
  for (pos=as->bp_pos-SEED_FLEN; pos < as->len - SEED_FLEN; pos += KLEN) {

    if(as->bp_pos < SEED_FLEN || pos+KLEN >= as->len) {
      fprintf(stderr, "[%s] Error, not enough flanking region around breakpoint.\n", __func__);
      fflush(stderr);
      abort();
    }

    sf = insert_SeqHash(sh, as->seq + pos);
    ak = next_ref_AKmerList(kl);
    if (sf->data == NULL) sf->data = init_AKmerPList(2);
    push_AKmerPList((AKmerPList*) sf->data, ak);
    ak->pos = pos;
    ak->pair = ap;
    ak->seq = as;
    ak->str = 0;

    _nt256char_rev(ksr, as->seq+pos, KLEN);
    sf = insert_SeqHash(sh, ksr);
    ak = next_ref_AKmerList(kl);
    if (sf->data == NULL) sf->data = init_AKmerPList(2);
    push_AKmerPList((AKmerPList*) sf->data, ak);
    ak->pos = pos;
    ak->pair = ap;
    ak->seq = as;
    ak->str = 1;

  }
}

APair* find_allele_pair(APairPList *pl, APair *ap) {
  uint32_t i; APair *ap1;
  for (i=0; i<pl->size; ++i) {
    ap1 = get_APairPList(pl, i);
    if (ap1 == ap) return ap1;
  }
  return NULL;
}

#define AK_IS_REF(k) (k)->seq == &(k)->pair->ref
#define AK_IS_ALT(k) (k)->seq == &(k)->pair->alt


static void
seed_extend_rev(char *read_seq, uint32_t read_kpos,
		char *ref_seq, uint32_t ref_kpos,
		uint32_t read_len, int *m) {
  *m = 0;
  char *r, *q; uint32_t len;

  /*
    ref  ========|========|====
    read     <<==|========|===<<
    extend >>>>>
  */
  len = read_kpos;
  r = ref_seq + ref_kpos + KLEN;
  q = read_seq + read_kpos - 1;
  for (; len--; ++r, --q) {
    if ((*r)!=nt256char_rev_table[(unsigned char)(*q)] && (*q)!='N') {
      ++*m;
      if (*m>SEED_EXT_MAX_MISMATCH) return;
    }
  }

  /* 
     ref  ========|========|====
     read     <<==|========|===<<
     extend <<<<<
   */
  len = read_len - read_kpos - KLEN;
  r = ref_seq + ref_kpos - 1;
  q = read_seq + read_kpos + KLEN;
  for (; len--; --r, ++q) {
    if ((*r)!=nt256char_rev_table[(unsigned char)(*q)] && (*q)!='N') {
      ++*m;
      if (*m>SEED_EXT_MAX_MISMATCH) return;
    }
  }
}

/* reset reference and alternative
   allele score to zero */

void reset_allele_score(APair *ap) {
  ap->alt.score = 0;
  uint32_t i;
  for (i=0; i<ap->refs->size; ++i) {
    ASeq *as = ref_ASeqList(ap->refs, i);
    as->score = 0;
  }
}


static void
seed_extend(char *read_seq, uint32_t read_kpos,
	    char *ref_seq, uint32_t ref_kpos, 
	    uint32_t read_len, int *m) {
  *m = 0;
  char *r, *q; uint32_t len;

  /*
    ref  =======|========|====
    read    >>==|========|====>>
    extend >>>>>
  */
  len = read_len - read_kpos - KLEN;
  r = ref_seq + ref_kpos + KLEN;
  q = read_seq + read_kpos + KLEN;
  for (; len--; ++r, ++q) {
    if ((*r)!=(*q) && (*q)!='N') {
      ++*m;
      if (*m>SEED_EXT_MAX_MISMATCH) return;
    }
  }

  /* 
     ref  ========|========|====
     read     >>==|========|===>>
     extend <<<<<
   */
  len = read_kpos;
  r = ref_seq + ref_kpos - 1;
  q = read_seq + read_kpos - 1;
  for (; len--; --r, --q) {
    if ((*r)!=(*q) && (*q)!='N') {
      ++*m;
      if (*m>SEED_EXT_MAX_MISMATCH) return;
    }
  }
}

/* spos: start position of k-mer on the ref/alt sequence
   ks: start position of k-mer on the read
   return the alignment score */
uint32_t
read_aln_seq(RKGen *rk, ASeq *as, uint32_t spos,
	     uint32_t ks, uint8_t str) {
  int m;			/* number of mismatches */
  if (str) seed_extend_rev(rk->seq, ks, as->seq, spos, rk->len, &m);
  else seed_extend(rk->seq, ks, as->seq, spos, rk->len, &m);
  if (m <= SEED_EXT_MAX_MISMATCH) return rk->len*2 - 4*m;

  rk_encode_nt256int8(rk);
  s_align *a=_ssw_align(rk->seq_sw, rk->len, as->seq_sw, as->len);
  s_align *ar=_ssw_align(rk->seq_sw_r, rk->len, as->seq_sw, as->len);
  uint32_t score = max(a->score1, ar->score1);
  free_align(a);
  free_align(ar);

  return score;
}

/* bcall on one file */
void bcall_file1(char *fq_file, SeqHash *sh, size_t *rid) {
  gzFile FQ_FILE = gzopen(fq_file, "r");
  kseq_t *seq = kseq_init(FQ_FILE);

  /* for holding discovered allele pairs */
  RKGen *rk = rk_init(KLEN);
  APairPList *pl = init_APairPList(2);
  int l;
  while ((l = kseq_read(seq)) >= 0) {
    (*rid)++;
    if (((*rid) & 0xFFFFU) == 0) {
      fprintf(stderr, "\r[%s] parsed %zu reads\033[K", __func__, *rid);
      fflush(stderr);
    }

    rk_bind(rk, seq->seq.s, seq->qual.s);
    clear_APairPList(pl);
    while (rk_generate(rk)>0) {
      Suffix *sf = get_SeqHash(sh, rk->p, rk->d);
      if (sf) {
	uint32_t ki, score;
	AKmerPList *kl = (AKmerPList*) sf->data;
	for (ki=0; ki<kl->size; ++ki) {
	  AKmer *k = get_AKmerPList(kl, ki);
	  if (k->seq->score > 0) continue;
	  score = read_aln_seq(rk, k->seq, k->pos, rk->ks, k->str);
	  if (score > rk->len*2-ALIT) {
	    if (!find_allele_pair(pl, k->pair)) {
	      push_APairPList(pl, k->pair);
	    }
	    k->seq->score = score;
	  }
	}
      }
    }

    uint32_t i, j;
    for (i=0; i<pl->size; ++i) {
      APair *ap = get_APairPList(pl, i);
      if (ap->alt.score > 0) {
	uint8_t support_alt = 1;
	for (j=0; j<ap->refs->size; ++j) {
	  ASeq *ref = ref_ASeqList(ap->refs, j);
	  if (ap->alt.score <= ref->score) {
	    support_alt = 0;
	    break;
	  }
	}
	if (support_alt) {
	  /* fprintf(stdout, "[%s] %s\t%s\talt_score\t%u\tref_score", */
	  /* 	  __func__, ap->id, seq->name.s, ap->alt.score); */
	  for (j=0; j<ap->refs->size; ++j) {
	    ASeq *ref = ref_ASeqList(ap->refs, j);
	    fprintf(stdout, "\t%u", ref->score);
	  }
	  fprintf(stdout, "\n");
	  fflush(stdout);
	  ap->alt_support++;
	}
      }
      reset_allele_score(ap);
    }
  }

  fprintf(stderr, "\r[%s] parsed %zu reads\033[K\n", __func__, *rid);
  fflush(stderr);

  free(rk);
  kseq_destroy(seq);

  return;
}

void bcall(BCnts *bc, char **fq_files1, char **fq_files2) {

  char **fq_file1=fq_files1;
  char **fq_file2=fq_files2;

  size_t rid = 0;
  while (1) {
    if (*fq_file1 == NULL || *fq_file2 == NULL)
      break;

    fprintf(stderr, "[%s] processing: %s\n", __func__, *fq_file1);
    bcall_file1(*fq_file1, bc->sh, &rid);
    fprintf(stderr, "[%s] processing: %s\n", __func__, *fq_file2);
    bcall_file1(*fq_file2, bc->sh, &rid);

    fq_file1++; fq_file2++;
  }

}



static int usage() {
  fprintf(stderr, "\n");
  fprintf(stderr, "Usage: clinsek bcall [options] -s allele_list -1 in1.fq [in1.fq [...]] -2 in2.fq [in2.fq [...]]\n");
  fprintf(stderr, "Input options:\n");
  fprintf(stderr, "     -s FILE   list of reference and alternative alleles [REQUIRED]\n");
  fprintf(stderr, "     -1 FILES  space-separated list of fastq (1st in pair) [REQUIRED]\n");
  fprintf(stderr, "     -2 FILES  space-separated list of fastq (2nd in pair) [REQUIRED]\n");
  fprintf(stderr, "     -o STR    output file prefix [test].\n");
  fprintf(stderr, "     -h        this help.\n");
  fprintf(stderr, "\n");
  return 1;
}


int main_bcall(int argc, char *argv[]) {

  if (argc < 2) return usage();

  FList *fl1 = init_FList(2);
  FList *fl2 = init_FList(2);
  char *sfn=0, *prefix=0;
  int c;
  while ((c = getopt(argc, argv, "s:1:2:o:h")) >= 0) {
    switch (c) {
    case 's': sfn = optarg; break;
    case '1': {
      int index;
      for (index = optind-1;index < argc;++index) {
      	char *next = argv[index];
      	if (next[0] == '-') break;
      	push_FList(fl1, strdup(next));
      }
      optind = index-1;
      break;
    }
    case '2': {
      int index;
      for (index = optind-1;index < argc;++index) {
      	char *next = argv[index];
      	if (next[0] == '-') break;
      	push_FList(fl2, strdup(next));
      }
      optind = index-1;
    } break;
    case 'o': prefix = optarg; break;
    case 'h': return usage();
    default:
      fprintf(stderr, "[%s] Unrecognized command: %c.\n", __func__, c);
      exit(1);
    }
  }

  push_FList(fl1, NULL);
  push_FList(fl2, NULL);

  
  BCnts *bc = load_allele_pairs(sfn);
  bcall(bc, fl1->buffer, fl2->buffer);
  bcnts_format(bc, prefix);

  free_bcnts(bc);

  return 0;
}
