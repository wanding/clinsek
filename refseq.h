#ifndef _WZ_REFSEQ_H_
#define _WZ_REFSEQ_H_

#include "samtools/faidx.h"
#include "wvec.h"
#include "wstring.h"

typedef struct {
  String *name;
  int len;
} Target;

DEFINE_VECTOR(TargetList, Target)

static inline void destroy_TargetList(TargetList *targets) {
  Target *t;
  size_t i;
  for(i=0; i<targets->size; i++) {
    t = ref_TargetList(targets, i);
    free_string(t->name);
  }
  free_TargetList(targets);
}

static inline TargetList*
build_tnames(const char *ref_fn) {
  char *fn = (char*)calloc(strlen(ref_fn) + 5, 1);
  sprintf(fn, "%s.fai", ref_fn);
  FILE *fp = fopen(fn, "r");

  char line[MAX_LINE_LEN];
  char chrm[MAX_LINE_LEN];
  TargetList *targets = init_TargetList(30);
  while (!feof(fp) && fgets(line, MAX_LINE_LEN, fp)) {
    int len;
    if (sscanf(line, "%s%d", chrm, &len)==2) {
      Target *t=next_ref_TargetList(targets);
      t->name = as_string(chrm);
      t->len = len;
    }
  }
  free(fn);
  fclose(fp);
  return targets;
}


static inline uint32_t
index_TargetList(TargetList *tl, char *chrm) {
  
  uint32_t i;
  for (i=0; i<tl->size; ++i) {
    Target *t = ref_TargetList(tl,i);
    if (strcmp(t->name->string, chrm) == 0) return i;
  }
  fprintf(stderr, "[Error] chromosome %s not recognized.\n", chrm);
  exit(EXIT_FAILURE);
  return 0;
}

typedef struct {
  char *rfn;
  faidx_t *fai;
  TargetList *tl;
} RefSeq;

static inline RefSeq *init_RefSeq(char *rfn) {
  RefSeq *rs = malloc(sizeof(RefSeq));
  rs->rfn = rfn;
  rs->fai = fai_load(rfn);
  rs->tl = build_tnames(rfn);
  return rs;
}

static inline void free_RefSeq(RefSeq *rs) {
  destroy_TargetList(rs->tl);
  fai_destroy(rs->fai);
  free(rs);
}

/* beg is 1-based */
static inline char *fetch_RefSeq(RefSeq *rs, uint32_t tid, uint32_t beg, uint32_t len) {
  int l;
  char *seq = faidx_fetch_seq(rs->fai, ref_TargetList(rs->tl, tid)->name->string, beg-1, beg+len-2, &l);
  if ((unsigned) l != len) {
    fprintf(stderr, "[%s:%d] Error, cannot retrieve reference.\n", __func__, __LINE__);
    exit(1);
  }
  return seq;
}

static inline char *fetch2_RefSeq(RefSeq *rs, uint32_t tid, uint32_t beg, uint32_t end) {
  return fetch_RefSeq(rs, tid, beg, end-beg+1);
}

static inline uint32_t chrm2tid(RefSeq *refseq, char *chrm) {
  return index_TargetList(refseq->tl, chrm);
}

static inline char *tid2chrm(RefSeq *refseq, int32_t tid) {
  if (tid < 0) return NULL;
  return ref_TargetList(refseq->tl, tid)->name->string;
}

#endif /* _WZ_REFSEQ_H_ */
