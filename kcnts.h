#ifndef _WZ_KCNTS_H_
#define _WZ_KCNTS_H_

#include "ref.h"
#include "site.h"
#include "refseq.h"
#include "samtools/kstring.h"

typedef struct {
  double contam, error, mu;
  double mu_somatic;
  uint32_t min_mapQ, min_baseQ, min_var;
  int tbaseQ;
  uint8_t realn:1;
  uint8_t mkdup:1;
  uint8_t filter:1;
  double min_var_p, aln_ratio;
  double prior0, prior1, prior2;	/* homozygous reference, heterozygous and homozygous variant priors */
  int verbose;
} CallConf;

#define DEFAULT_CONTAM 0.005
#define DEFAULT_ERROR 0.001

static inline void
conf_init_default(CallConf *conf) {

  conf->contam     = 0.005;
  conf->error      = 0.001;
  conf->mu         = 0.001;
  conf->mu_somatic = 0.001;
  conf->min_mapQ   = 10;
  conf->min_baseQ  = 10;
  conf->tbaseQ     = 10;
  conf->min_var    = 3;
  conf->min_var_p  = 0.005;
  conf->realn      = 0;
  conf->mkdup      = 1;
  conf->aln_ratio  = 1.8;
  conf->filter     = 1;
  conf->prior1     = 0.33333;
  conf->prior2     = 0.33333;
  conf->prior0     = 1.0 - conf->prior1 - conf->prior2;
  conf->verbose = 0;
}

typedef struct {

  RefSeq *rs;
  SiteList *sl;
  SSPool *ssp;
  SeqHash *sh;
  char *sfn;
  CallConf *conf;

} KCnts;

static inline void
free_KCnts(KCnts *kc) {
  destroy_SeqHash(kc->sh, (void (*)(void *)) free_SeqList);
  destroy_SiteList(kc->sl);
  destroy_SSPool(kc->ssp);
  free_RefSeq(kc->rs);
  free(kc);
}


KCnts* load_sites_core(char *sfn, char *rfn, kstring_t *sitestr);
#define load_sites(s, r) load_sites_core(s, r, 0)

int bam_standardize(char *bam_fn, char *bam_tmp_fn);



#endif /* _WZ_KCNTS_H_ */
