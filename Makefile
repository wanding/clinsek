CFLAGS=-W -Wall -finline-functions -fPIC -std=gnu99

# CFLAGS+=-g #-pg

# CFLAGS+=-DDEBUGSITE
# CFLAGS+=-DDEBUGHOMO
# CFLAGS+=-DDEBUGTALN
# CFLAGS+=-DDEBUGTALLY
# CFLAGS+=-DDEBUGTIME
# CFLAGS+=-DDEBUGPLP
# CFLAGS+=-DDEBUGTCALL
# CFLAGS+=-DDEBUGMKDUP

LOBJ = seq.o encode.o site.o tally.o ssw.o tgt_aln.o mate_aln.o bam_std.o pileup.o clinsek_cmd.o tcall.o tally_aln.o homo_scan.o stats.o mark_dup.o aln.o homo_aln.o bam_fmt_aln.o scall.o spileup.o tpileup.o tgt_realn.o bcall.o

EOBJ = samtools/libsam.a bcftools/libbcf.a

INCLUDE = -Isamtools

release : CFLAGS += -O3 # -DDEBUG
release : clinsek

debug : CFLAGS += -g
debug : clinsek

clinsek : $(LOBJ) $(EOBJ)
	gcc -std=c99 $(CFLAGS) -o $@ $(LOBJ) $(EOBJ) -lm -lz -lpthread

.c.o :
	gcc -c $(CFLAGS) $(INCLUDE) $< -o $@

seq.o		: kmer.h
stats.o         : stats.h
encode.o        : encode.h
site.o		: seq.h site.h encode.h ref.h
tally.o		: kcnts.h tally.h ref.h kseq.h
tally_aln.o     : tally.h
ssw.o		: ssw.h
tgt_aln.o	: kcnts.h tally.h site.h
mate_aln.o      :
bam_std.o	: wstring.h wvec.h bam_std.h
pileup.o	: kcnts.h site.h
clinsek_cmd.o	: kcnts.h
tcall.o         : kcnts.h tcall.c
homo_scan.o     : fa_reader.h ssw.h ref.h site.h
mark_dup.o      : wvec.h
aln.o           : site.h aln.h tally.h
homo_aln.o      : site.h aln.h
bam_fmt_aln.o   : 
scall.o		: wstr.h
spileup.o	: pileup.h
tpileup.o	: pileup.h
tgt_realn.o	: realn.h biproc.h read_pool.h
bcall.o         : bcall.h tsv_parser.h rkgen.h

samtools/libsam.a :
	make -C samtools libsam.a

bcftools/libbcf.a :
	make -C bcftools libbcf.a

.PHONY: clean
clean :
	make -C samtools clean
	make -C bcftools clean
	rm -f *.o clinsek a.out gmon.out

homoscan : homo_scan.c
	$(MAKE) -C samtools
	gcc -g -Wall homo_scan.c ssw.c seq.c encode.c $(samtools-objects) -lz -lpthread


plp : plp.c $(LOBJ) $(EOBJ)
	make -C samtools libsam.a
	make -C bcftools libbcf.a
	gcc -g -Wall $(LOBJ)

test.homoscan.cms46 : clinsek
	./clinsek homoscan -r /workspace/zchong/clinsek/test_data/hg19.fa -s /workspace/zchong/clinsek/build_data/library/cms46.site -o /workspace/zchong/clinsek/build_data/library/cms46.site.hscan

test.homoscan.clinvar : clinsek
	./clinsek homoscan -r /workspace/zchong/clinsek/test_data/hg19.fa -s /home/wzhou1/project_clinsek/clinVar/clinvar_20140430.sites.uniq -o /home/wzhou1/project_clinsek/clinVar/clinvar_20140430.sites.uniq.hscan

test.small : clinsek
	./clinsek tcall -s /workspace/zchong/clinsek/build_data/library/cms46.site -r /workspace/zchong/clinsek/test_data/hg19.fa -1 /workspace/zchong/clinsek/test_data/smallsample1.fq -2 /workspace/zchong/clinsek/test_data/Sample_PP2-3_Mix1/s2.fq -o test_small

# globbing expansion is alphabetical
test.tcall.mix : clinsek 
	./clinsek tcall -s /workspace/zchong/clinsek/build_data/library/cms46.site.hscan -r /workspace/zchong/clinsek/test_data/hg19.fa -1 /workspace/zchong/clinsek/test_data/Sample_PP2-3_Mix1/s1.fq -2 /workspace/zchong/clinsek/test_data/Sample_PP2-3_Mix1/s2.fq -o mix

test.tcall.fc24 : clinsek
	./clinsek tcall -s /workspace/zchong/clinsek/build_data/library/cms46.site.hscan -r /workspace/zchong/clinsek/test_data/hg19.fa -1 /projects/flowcell24/data/IPCT-CH-2726-Tumor-538/gz/*R1* -2 /projects/flowcell24/data/IPCT-CH-2726-Tumor-538/gz/*R2* -o test.fc24

test.tcall.fc24.2: clinsek
	./clinsek tcall -s /workspace/zchong/clinsek/build_data/library/cms46.site.hscan -r /workspace/zchong/clinsek/test_data/hg19.fa -1 /projects/flowcell24/data/IPCT-CH-2726-Tumor-538/gz/Cap0189-2726-20_GTGGCC_L008_R1_00{1,2}.fastq.gz -2 /projects/flowcell24/data/IPCT-CH-2726-Tumor-538/gz/Cap0189-2726-20_GTGGCC_L008_R2_00{1,2}.fastq.gz -o test.fc24

test.tcall.555: clinsek
	/home/wzhou1/project_clinsek/clinsek2/clinsek tcall -r /workspace/zchong/clinsek/test_data/hg19.fa -s /home/wzhou1/project_clinsek/data/cms46_sites.hscan -1 /scratch/ipct/wzhou1/cms46_samples/IPCT-CH-1104-Tumor-555/*R1*.gz -2 /scratch/ipct/wzhou1/cms46_samples/IPCT-CH-1104-Tumor-555/*R2*.gz -o test.555

test.tcall.453: clinsek
	/home/wzhou1/project_clinsek/clinsek2/clinsek tcall -r /workspace/zchong/clinsek/test_data/hg19.fa -s /home/wzhou1/project_clinsek/data/cms46_sites.hscan -1 /scratch/ipct/wzhou1/cms46_samples/IPCT-CH-1343-Tumor-453/*R1*.gz -2 /scratch/ipct/wzhou1/cms46_samples/IPCT-CH-1343-Tumor-453/*R2*.gz -o test.453

# sample 5113
test.tcall.5113:
	./clinsek tcall -r /projects/database/reference/hs37d5.fa -s /projects/clinsek_varscan_comparison/clinvar_20140430.sites.uniq.T200.hs37.hscan -1 /projects/clinsek_varscan_comparison/data/IPCT-CH-5113-Normal-1313/gz/*R1*.fastq.gz -2 /projects/clinsek_varscan_comparison/data/IPCT-CH-5113-Normal-1313/gz/*R2*.fastq.gz -o test_data/IPCT-CH-5113

test.tcall.1327:
	./clinsek tcall -r /projects/database/reference/hs37d5.fa -s /projects/clinsek_varscan_comparison/clinvar_20140430.sites.uniq.T200.hs37.hscan -1 /projects/clinsek_varscan_comparison/data/IPCT-CH-4696-Normal-1327/gz/*R1*.fastq.gz -2 /projects/clinsek_varscan_comparison/data/IPCT-CH-4696-Normal-1327/gz/*R2*.fastq.gz -o test_data/IPCT-CH-4696-Normal-1327.hs37


test.tpileup.453: clinsek
	./clinsek tpileup -r /workspace/zchong/clinsek/test_data/hg19.fa -s /home/wzhou1/project_clinsek/data/cms46_sites.hscan -i /scratch/ipct/wzhou1/cms46_result/IPCT-CH-1343-Tumor-453.bam -o test.453

# sample 1107
test.tpileup.1107:
	./clinsek tpileup -r /workspace/zchong/clinsek/test_data/hg19.fa -s /home/wzhou1/project_clinsek/clinsek2/test_data/chr16_70954717 -i /projects/clinsek_varscan_comparison/data/IPCT-CH-5119-Normal-1107-SA/bam/IPCT-CH-5119-Normal-1107-SA.rmdup.bam -o test.1107

test.realn.1107:
	./clinsek realn -i /projects/clinsek_varscan_comparison/data/IPCT-CH-5119-Normal-1107-SA/bam/IPCT-CH-5119-Normal-1107-SA.rmdup.bam -s /home/wzhou1/project_clinsek/clinsek2/test_data/chr16_70954717 -r /workspace/zchong/clinsek/test_data/hg19.fa -o test_data/sample1107.realn.bam

test.mkdup : clinsek
	./clinsek mkdup -i test.bam.tmp.srt


# sample 574
test.homoscan.574: clinsek
	./clinsek homoscan -r /workspace/zchong/clinsek/test_data/hg19.fa -s /home/wzhou1/project_clinsek/clinsek2/test_data/sample574_somatic_sites

# sample 1324
test.scall.1324:
	./clinsek scall -r /projects/database/reference/hs37d5.fa -s /projects/clinsek_varscan_comparison/clinvar_20140430.sites.uniq.T200.hs37.hscan -1 /projects/clinsek_varscan_comparison/data/IPCT-CH-5281-Tumor-1324/gz/*R1*.gz -2 /projects/clinsek_varscan_comparison/data/IPCT-CH-5281-Tumor-1324/gz/*R2*.gz -3 /projects/clinsek_varscan_comparison/data/IPCT-CH-4786-Normal-1324/gz/*R1*.gz -4 /projects/clinsek_varscan_comparison/data/IPCT-CH-4786-Normal-1324/gz/*R2*.gz -o test_data/IPCT-CH-5281-Tumor-1324

test.scall.1296:
	./clinsek scall -r /projects/database/reference/hs37d5.fa -s /projects/clinsek_varscan_comparison/clinvar_20140430.sites.uniq.T200.hs37.hscan -1 /projects/clinsek_varscan_comparison/data/IPCT-CH-5353-Tumor-1296/gz/*R1*.gz -2 /projects/clinsek_varscan_comparison/data/IPCT-CH-5353-Tumor-1296/gz/*R2*.gz -3 /projects/clinsek_varscan_comparison/data/IPCT-CH-4609-Normal-1296/gz/*R1*.gz -4 /projects/clinsek_varscan_comparison/data/IPCT-CH-4609-Normal-1296/gz/*R2*.gz -o test_data/IPCT-CH-5353-Tumor-1296

test.scall.574: clinsek
	./clinsek scall -r /workspace/zchong/clinsek/test_data/hg19.fa -s test_data/sample574_somatic_sites -1 test_data/IPCT-CH-2738-Tumor-574/gz/*R1*.gz -2 test_data/IPCT-CH-2738-Tumor-574/gz/*R2*.gz -3 test_data/IPCT-CH-2936-Normal-574/gz/*R1*.gz -4 test_data/IPCT-CH-2936-Normal-574/gz/*R2*.gz -o test_data/sample574

test.spileup.574: clinsek
	./clinsek spileup -r /workspace/zchong/clinsek/test_data/hg19.fa -s test_data/sample574_somatic_sites -t test_data/IPCT-CH-2738-Tumor-574/bam/IPCT-CH-2738-Tumor-574.rmdup.bam -n test_data/IPCT-CH-2936-Normal-574/bam/IPCT-CH-2936-Normal-574.rmdup.bam -o test_data/sample574.bcf


# sample TCGA 2817 RNASeq bcall
test.bcall.2817:
	./clinsek bcall -s /home/wzhou1/project_clinsek/RNAseq/RNAseq_fusion.dat.curate -1 /home/wzhou1/project_clinsek/RNAseq/ABL1_BCR_samples/TCGA-AB-2817-03A-01T-0736-13_rnaseq/*_1.fastq.gz -2 /home/wzhou1/project_clinsek/RNAseq/ABL1_BCR_samples/TCGA-AB-2817-03A-01T-0736-13_rnaseq/*_2.fastq.gz -o test_data/TCGA_2817

