#ifndef _WZ_SITE_H
#define _WZ_SITE_H
#include <ctype.h>
#include "kmer.h"
#include "wvec.h"
#include "wstring.h"
#include "seq.h"
#include "encode.h"


typedef struct {
  int16_t tpos;			/* position relative to target site */
  uint8_t type;			/* type of difference: mis-match, insertion, deletion */
  char *alt_seq;		/* alternative sequence, \0 padded. */
} SDiff;

DEFINE_VECTOR(DiffList, SDiff)

/* homologous site: h_start and h_end is where the alignable (homologous) region starts and ends from target sequence, relative to the target site */
typedef struct {
  uint8_t chrm;
  size_t pos;
  uint8_t str;
  int32_t h_start;
  int32_t h_end;
  DiffList *diffs;
  size_t rpos;
  char *seq;
  int8_t *swseq;
} HomoSite;

DEFINE_VECTOR(HSiteList, HomoSite);

static inline void destroy_HSiteList(HSiteList* hsl) {
  if (!hsl) return;
  unsigned i, j;
  for (i=0; i<hsl->size; ++i) {
    HomoSite *hs = ref_HSiteList(hsl, i);
    for (j=0; j<hs->diffs->size; ++j) {
      free(ref_DiffList(hs->diffs, j)->alt_seq);
    }
    free_DiffList(hs->diffs);
  }
  free_HSiteList(hsl);
}

typedef struct Site {
  size_t id;
  uint8_t chrm;
  size_t pos;
  KmerList *kl;
  size_t rpos;		 /* site's location on the refseq (zero-based) */
  HSiteList *hsl;	 /* list of homologous sites */
} __attribute__((__packed__)) Site;

DEFINE_NATIVE_VECTOR(SiteList, Site*)

static inline void destroy_SiteList(SiteList *sl) {
  size_t i;
  for (i=0; i<sl->size; ++i) {
    Site *st = get_SiteList(sl, i);
    if (st->hsl) destroy_HSiteList(st->hsl);
    destroy_KmerList(st->kl);
    free(st);
  }
  free_SiteList(sl);
}


static inline int
cmp_sites(const void *p1, const void *p2) {

  const Site *s1 = *((const Site**)p1);
  const Site *s2 = *((const Site**)p2);
  int result;
  result = s1->chrm - s2->chrm;
  if (result) return result;
  result = s1->pos - s2->pos;
  if (result) return result;
  return 0;
}

#define MAX_LINE_LEN 0x10000


/* site filter */
DEFINE_VECTOR(SiteFilter, uint8_t);

static inline SiteFilter*
load_filter_core(char *filter_fn) {
  int c;
  FILE *fh = fopen(filter_fn, "r");
  if (!fh) {
    fprintf(stderr, "[%s] Error, cannot open filter file %s",
	    __func__, filter_fn);
    fflush(stderr);
    exit(1);
  }
  SiteFilter *fter = init_SiteFilter(2);
  while ((c=fgetc(fh)) != EOF) {
    if (isspace(c)) continue;
    if (c > '0') push_SiteFilter(fter, 1);
    else push_SiteFilter(fter, 0);
  }
  return fter;
}

static inline void dump_filter(char *filter_fn, SiteFilter *fter) {
  FILE *fh = fopen(filter_fn, "w");
  if (!fh) {
    fprintf(stderr, "[%s] Error, cannot open filter file for writing %s",
	    __func__, filter_fn);
    fflush(stderr);
    exit(1);
  }

  uint32_t i;
  for (i=0; i<fter->size; ++i) {
    fputc('0'+get_SiteFilter(fter, i), fh);
  }
  fclose(fh);
}


static inline int
count_filter(SiteFilter *fter) {
  unsigned i; int sum = 0;
  for (i=0; i<fter->size; ++i) {
    if (get_SiteFilter(fter, i)) sum++;
  }
  return sum;
}

static inline SiteFilter*
init_SiteFilter_all(uint32_t n_sites) {
  SiteFilter *fter = init_SiteFilter(n_sites+1);
  fter->size = n_sites;
  uint32_t i;
  for (i=0; i<fter->size; ++i) {
    set_SiteFilter(fter, i, 1);
  }
  return fter;
}

/* load filter with the option of filter_fn == 0
   in that case, no siv will be provided 
*/
static inline
SiteFilter* load_filter(char *filter_fn, uint32_t n_sites) {
  SiteFilter *fter;
  if (filter_fn) {
    fter = load_filter_core(filter_fn);
    assert(fter->size == n_sites);
  } else fter = init_SiteFilter_all(n_sites);
  fprintf(stderr, "[%s] load %d valid sites in filter\n",
	  __func__, count_filter(fter));
  return fter;
}


#endif
