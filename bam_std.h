
#ifndef _WZ_BAM_STD_H_
#define _WZ_BAM_STD_H_


#include "samtools/sam.h"
#include "wstring.h"
#include "wvec.h"
#include "kcnts.h"
#include "realn.h"

void bam_sort_ip(char *bam_fn);
void bam_mkdup(char *bam_fn);
void bam_realn(char *bam_fn, KCnts *kc, uint8_t *siv);

#endif /* _WZ_BAM_STD_H_ */
