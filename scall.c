#include <unistd.h>
#include "tally.h"
#include "samtools/bam.h"
#include "tgt_aln.h"
#include "spileup.h"
#include "bam_std.h"
#include "wstr.h"

static int usage() {
  fprintf(stderr, "\n");
  fprintf(stderr, "Usage: clinsek scall [options] -s sitelist -r ref.fa -1 tumor_pair1.fq [tumor_pair1.fq [...]] -2 tumor_pair2.fq [tumor_pair2.fq [...]] -3 normal_pair1.fq [normal_pair1.fq [...]] -4 normal_pair2.fq [normal_pair2.fq [...]]\n");
  fprintf(stderr, "Input options:\n");
  fprintf(stderr, "     -s FILE   site list [required]\n");
  fprintf(stderr, "     -r FILE   reference in fasta format [required]\n");
  fprintf(stderr, "     -1 FILES  fastq file(s) (1st in pair, tumor) [required]\n");
  fprintf(stderr, "     -2 FILES  fastq file(s) (2nd in pair, tumor) [required]\n");
  fprintf(stderr, "     -3 FILES  fastq file(s) (1st in pair, normal) [required]\n");
  fprintf(stderr, "     -4 FILES  fastq file(s) (2nd in pair, normal) [required]\n");
  fprintf(stderr, "     -o STR    output file prefix [test]\n");
  fprintf(stderr, "     -l FILE   site filter file [optional]\n");
  fprintf(stderr, "     -c FLOAT  estimate for contamination rate [0.01]\n");
  fprintf(stderr, "     -e FLOAT  estimate for sequencing error rate [1e-3]\n");
  fprintf(stderr, "     -m FLOAT  estimate for mutation rate [1e-3]\n");
  fprintf(stderr, "     -g FLOAT  estimate of somatic mutation rate [1e-3]\n");
  fprintf(stderr, "     -q INT    minimum mapping quality in variant calling [10]\n");
  fprintf(stderr, "     -b INT    minimum base quality in SNP calling [10]\n");
  fprintf(stderr, "     -d INT    base quality threshold in preliminary call [20]\n");
  fprintf(stderr, "     -v INT    minimum number of variants in tumor sample for prelimnary call [3]\n");
  fprintf(stderr, "     -u FLOAT  minimum variant fraction in tumor sample for preliminary call [0.005]\n");
  fprintf(stderr, "     -a        toggle to turn ON indel realignment\n");
  fprintf(stderr, "     -k        toggle to turn OFF duplicate marking\n");
  fprintf(stderr, "     -h        this help\n");
  fprintf(stderr, "\n");
  return 1;
}


int main_scall(int argc, char *argv[]) {

  CallConf conf;
  conf_init_default(&conf);
  clock_t t = clock();

  if (argc < 2) return usage();

  FList *fl1 = init_FList(2);
  FList *fl2 = init_FList(2);
  FList *fl3 = init_FList(2);
  FList *fl4 = init_FList(2);
  char *sfn=0, *rfn=0, *prefix=0, *filter_fn=0;
  int c;
  while ((c = getopt(argc, argv, "s:r:1:2:3:4:o:l:c:e:m:g:u:q:b:d:v:kah")) >= 0) {
    switch (c) {
    case 's': sfn = optarg; break;
    case 'r': rfn = optarg; break;
    case '1': {
      int index;
      for (index = optind-1;index < argc;++index) {
      	char *next = argv[index];
      	if (next[0] == '-') break;
      	push_FList(fl1, strdup(next));
      }
      optind = index-1;
      break;
    }
    case '2': {
      int index;
      for (index = optind-1;index < argc;++index) {
      	char *next = argv[index];
      	if (next[0] == '-') break;
      	push_FList(fl2, strdup(next));
      }
      optind = index-1;
    } break;
    case '3': {
      int index;
      for (index = optind-1;index < argc;++index) {
      	char *next = argv[index];
      	if (next[0] == '-') break;
      	push_FList(fl3, strdup(next));
      }
      optind = index-1;
      break;
    }
    case '4': {
      int index;
      for (index = optind-1;index < argc;++index) {
      	char *next = argv[index];
      	if (next[0] == '-') break;
      	push_FList(fl4, strdup(next));
      }
      optind = index-1;
    } break;
    case 'o': prefix = optarg; break;
    case 'l': filter_fn = optarg; break;
    case 'c': conf.contam = atof(optarg); break;
    case 'e': conf.error = atof(optarg); break;
    case 'm': conf.mu = atof(optarg); break;
    case 'g': conf.mu_somatic = atof(optarg); break;
    case 'q': conf.min_mapQ = atoi(optarg); break;
    case 'b': conf.min_baseQ = atoi(optarg); break;
    case 'd': conf.tbaseQ = atoi(optarg); break;
    case 'v': conf.min_var = atoi(optarg); break;
    case 'u': conf.min_var_p = atof(optarg); break;
    case 'a': conf.realn = 1; break;
    case 'k': conf.mkdup = 0; break;
    case 'h': return usage();
    default:
      fprintf(stderr, "[%s] Unrecognized command: %c.\n", __func__, c);
      exit(1);
    }
  }

  push_FList(fl1, NULL);
  push_FList(fl2, NULL);
  push_FList(fl3, NULL);
  push_FList(fl4, NULL);
  if (!prefix) prefix = "test";

  KCnts *kc = load_sites(sfn, rfn);
  kc->conf = &conf;

  char *vb_t_fn = wasprintf("%s.t.vardp", prefix);
  VarSIBuffer *vb_t = init_VarSIBuffer(vb_t_fn);
  free(vb_t_fn);

  char *rb_t_fn = wasprintf("%s.t.refdp", prefix);
  RefSIBuffer *rb_t = init_RefSIBuffer(rb_t_fn);
  free(rb_t_fn);

  tally(kc, fl1->buffer, fl2->buffer, vb_t, rb_t);

  char *vb_n_fn = wasprintf("%s.n.vardp", prefix);
  VarSIBuffer *vb_n = init_VarSIBuffer(vb_n_fn);
  free(vb_n_fn);

  char *rb_n_fn = wasprintf("%s.n.refdp", prefix);
  RefSIBuffer *rb_n = init_RefSIBuffer(rb_n_fn);
  free(rb_n_fn);

  tally(kc, fl3->buffer, fl4->buffer, vb_n, rb_n);

  SiteFilter *fter = prelim_call(kc, vb_t, rb_t);
  if (filter_fn) dump_filter(filter_fn, fter);

  char *bam_t_fn = wasprintf("%s.tumor.bam", prefix);
  tgt_aln(kc, fl1->buffer, fl2->buffer, bam_t_fn, vb_t, rb_t, fter->buffer);
  bam_sort_ip(bam_t_fn);
  bam_index_build(bam_t_fn);

  char *bam_n_fn = wasprintf("%s.normal.bam", prefix);
  tgt_aln(kc, fl3->buffer, fl4->buffer, bam_n_fn, vb_n, rb_n, fter->buffer);
  bam_sort_ip(bam_n_fn);
  bam_index_build(bam_n_fn);

  if (conf.realn) {
    bam_realn(bam_t_fn, kc, fter->buffer);
    bam_realn(bam_n_fn, kc, fter->buffer);
  }

  if (conf.mkdup) {
    bam_mkdup(bam_t_fn);
    bam_mkdup(bam_n_fn);
  }

  destroy_VarSIBuffer(vb_t);
  destroy_RefSIBuffer(rb_t);
  destroy_VarSIBuffer(vb_n);
  destroy_RefSIBuffer(rb_n);

  char *bcf_fn = wasprintf("%s.bcf", prefix);
  spileup(bcf_fn, bam_t_fn, bam_n_fn, kc, fter->buffer);

  free_SiteFilter(fter);
  free(bam_t_fn);
  free(bam_n_fn);
  free(bcf_fn);

  destroy_FList(fl1);
  destroy_FList(fl2);
  destroy_FList(fl3);
  destroy_FList(fl4);
  free_KCnts(kc);

  fprintf(stderr, "[%s] All done. Time: %.3f min\n", __func__, (float) (clock() - t) / CLOCKS_PER_SEC / 60.);

  return 0;
}
